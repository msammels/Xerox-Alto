//
//  raf
//  restore_alto_files
//
//  Created by Paul McJones starting on 10/11/2013.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//


#include <algorithm>
#include <cassert>
//#include <cctype>
//#include <cstdint>
//#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <sys/stat.h>
#include <string>
#include <unistd.h>
#include <vector>
#include "Alto_DM_file.h"
#include "archivist.h"
#include "AltoDefs.h"
#include "AltoFileSys.h"
#include "altoweb.h"
#include "Bfs.h"
#include "AltoFileType.h"
#include "bravo2html.h"
#include "press2pdf.h"
#include "CopyDiskProtocol.h"
#include "utility.h"

// Command-line parameters and options (see main)
std::string archive_path; // path to year/tape/record hierarchy
std::string restore_path; // path to server/dirpath/filename!version hierarchy
std::string extras_path; // path to cd{6,8) if present
std::string attributes_path; // path to single file to check for crc32, ascii, and binary
std::string bravo_path; // path to single .bravo file to convert to HTML
std::string press_path; // path to single .press file to convert to PostScript
std::string copydisk_image_path; // path to single or double copydisk-style disk image to extract
std::string aar_image_path; // path to single or double aar.c-style disk image to extract
std::string dm_file_path; // path to single .dm file to unpack
std::string web_path; // path for static web pages
std::set<std::string>   exclude; // years/tapes/records to exclude from consideration
bool verbose = false;
bool tilde = false; // if true, represent version as .~2~ suffix rather than !2 suffix
bool unpack_dm_files = false;
bool unpack_disk_images = false; // e.g., .altodisk, .bfs, and .copydisk
std::string dual_pack_dp0; // e.g., Johnsson-3KRamTest-DP0.altodisk
std::string dual_pack_dp1; // e.g., Johnsson-3KRamTest-DP1.altodisk

// Convert, e.g., (84, 10000, 23) to "84/T10000P/file0023".
std::string path_from_record_id(RecordIdentifier ri) {
    std::ostringstream oss;
    oss.width(2);           // 84
    oss << std::get<0>(ri); // year
    oss.width(0);
    oss << "/T";
    oss.width(5);           // 10000
    oss.fill('0');
    oss << std::get<1>(ri); // tape
    oss.width(0);
    oss << "P/file";
    oss.width(4);           // 0023
    oss << std::get<2>(ri); // record number
    return oss.str();
}

void validate_assumptions() {
//    std::cout << "*** sizeof(unsigned long): " << sizeof(unsigned long) << std::endl;
    static_assert(sizeof(PrefixRep)      == 8, "Unexpected size");
    static_assert(sizeof(DataRep)        == 10, "Unexpected size");
    static_assert(sizeof(DA)             == sizeof(UWord), "Unexpected size");
    static_assert(sizeof(DiskPageHeader) == 2 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(FID)            == 3 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(FileIdentifier) == 3 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(DL)             == 8 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(DiskPageLabel)  == 8 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(DiskPageData)   == 256 * sizeof(UWord), "Unexpected size");
    static_assert(sizeof(LD)             == 256 * sizeof(UWord), "Unexpected size");
}

typedef std::pair<RecordIdentifier, RecordType> RecordMapping;

std::map<RecordIdentifier, RecordType>      record_table;
std::map<RecordIdentifier, DirectoryRecord> directory_table;
std::map<RecordIdentifier, DataRecord>      data_table;

typedef std::string FilePath; // e.g., [Indigo]<DMS>Laurel>6>laurelxref.mcross!1
std::multimap<FilePath, RecordIdentifier> file_path_index; // map path on server to record identifier for data

FilePath file_path_from_directory_record(const DirectoryRecord& dir_rec) {
    return "[" + dir_rec.server + "]<" + dir_rec.dir_path + ">" + append_version(dir_rec.file_name, dir_rec.version);
}

std::vector<FileRecord> file_table;
std::multimap<Crc32, std::vector<FileRecord>::size_type> file_table_crc32_index;

std::map<Extension, int, ci_less> extension_table; // histogram of extensions
typedef std::map<Extension, int, ci_less>::iterator ExtensionTableIterator;

std::map<Author, int, ci_less> author_table; // histogram of authors
typedef std::map<Author, int, ci_less>::iterator AuthorTableIterator;

void add_to_file_table(FileRecord file_rec) {
    // Increment histogram bucket for extension.
    std::string extension = get_extension(file_rec.dir_rec.file_name);
    std::pair<ExtensionTableIterator, bool> p_ext = extension_table.insert(std::make_pair(extension, 1));
    if (!p_ext.second) p_ext.first->second += 1;
    // Some binary files are really press files.
    if (file_rec.data_rec.f_t == binary) {
        if (extension == ".press" || extension == ".PRESS")
            file_rec.data_rec.f_t = press;
    }

    // Increment histogram bucket for author.
    std::pair<ExtensionTableIterator, bool> p_auth = author_table.insert(std::make_pair(file_rec.dir_rec.author, 1));
    if (!p_auth.second) p_auth.first->second += 1;
    
    auto i = file_table.size();
    file_table.push_back(file_rec);
    file_table_crc32_index.insert(std::make_pair(file_rec.data_rec.crc32, i));
    
}

// Convert a string such as "31-Jan-81 19:59:27 PST" to a time_t.
time_t string_to_date(const std::string& date) {
    const long PST_offset = -8*60*60;
    const long PDT_offset = -7*60*60;
    struct tm tm;
    char* time_zone = strptime(date.c_str(), "%d-%b-%y %H:%M:%S ", &tm);
    // Handle time_zone; assume Pacific (since PARC was in Palo Alto).
    if (time_zone != nullptr) {
        if (time_zone[0] == 'P' && time_zone[2] == 'T') {
            if (time_zone[0] == 'S')
                tm.tm_gmtoff = PST_offset;
            else if (time_zone[0] == 'D')
                tm.tm_gmtoff = PDT_offset;
            else tm.tm_gmtoff = PST_offset; // default
        } else tm.tm_gmtoff = PST_offset; // default
    }
    return mktime(&tm);
}

bool get_server(const char* f, const char* l, std::string& value) {
    if (*f != '[') return false;
    ++f;
    if (f == l) return false;
    const char* limit = std::find(f, l, ']');
    if (limit == l) return false;
    value = std::string(f, limit - f);
    return true;
}

/* Example: If the range [f, l) includes the characters, say, "[Indigo]<DMS>Laurel>6>laurelxref.mcross!1((Directory DMS>Laurel>6)(Name-Body laurelxref.mcross)(Version 1)(Creation-Date 19-May-81  9:40:21 PDT)(Write-Date 12-Oct-81 17:57:21 PDT)(Byte-Size 8)(Type Binary)(Size 337858)(Author RWeaver)(Device Primary)(Server-Filename <DMS>Laurel>6>laurelxref.mcross!1))", and prop is "Author", then value will be set to "RWeaver" */
bool get_prop(const char* f, const char* l, const std::string& prop, std::string& value) {
    const auto n = prop.size();
    // Advance past '(' that begins the property list.
    f = std::find(f, l, '(');
    if (f == l) return false;
    ++f;
    // Check each property in turn.
    while (f != l && *f !=')') {
        if (*f != '(') return false; // malformed property
        ++f;
        if (f + n < l && prop.compare(0, n, f, n) == 0) {
            f = f + n; // still f < l
            if (*f != ' ') return false; // malformed property
            ++f;
            const char* limit = std::find(f, l, ')');
            if (limit == l) return false; // malformed property
            value = std::string(f, limit - f);
            return true;
        } else {
            f = std::find(f, l, ')');
            if (f == l) return false; // malformed property
            ++f;
        }
    }
    return false; // end of list with name not found
}

void read_directory(const std::string& full_record_path,
                    const RecordIdentifier& ri,
                    std::ifstream& is, const std::fstream::streamoff& length,
                    const RecordMapping& prev) {
    if (length < sizeof(DirectoryRep)) {
        std::cout << "*** Directory record too short: " << full_record_path << std::endl;
        return;
    }
    if (prev.second == directory) { // prev is redundant
        std::cout << "*** Redundant directory entry: " << path_from_record_id(prev.first)
        << std::endl;
    }
    is.seekg(0, is.beg);
    std::vector<char> buffer(length);
    is.read(&buffer[0], length);
    const Directory dp = (Directory)&buffer[0];
    const char* f = &dp->varData[0];
    const char* l = &*buffer.end();
    DirectoryRecord dr;
    
    // Fill dr from dp.
    // ***** Should iterate through properties, switching on property name
    std::string temp;
    if (!get_server(f, l, dr.server)) {
        std::cout << "*** Missing server name: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Directory", dr.dir_path)) {
        std::cout << "*** Missing directory path: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Name-Body", dr.file_name)) {
        std::cout << "*** Missing file name: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Version", temp)) {
        std::cout << "*** Missing version: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        dr.version = std::stoi(temp);
    
    if (!get_prop(f, l, "Creation-Date", temp)) {
        std::cout << "*** Missing creation date: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        dr.creation_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Write-Date", temp)) {
        std::cout << "*** Missing write date: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        dr.write_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Read-Date", temp)) {
        std::cout << "*** Missing read date: " << path_from_record_id(ri) << std::endl;
        dr.read_date = dr.write_date; /* Not considered fatal */
    } else
        dr.read_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Type", temp)) {
        std::cout << "*** Missing type: " << path_from_record_id(ri) << std::endl;
        dr.text = false; /* Not considered fatal */
    } else {
        dr.text = temp.compare("Text") == 0; // ***** else "Binary" - any other values ?????
        if (!dr.text && temp.compare("Binary") != 0)
            std::cout << "*** Unexpected type (" << temp << "): " << path_from_record_id(ri) << std::endl;
    }
    
    if (!get_prop(f, l, "Size", temp)) {
        std::cout << "*** Missing size: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        dr.size = std::stoi(temp);
    
    if (!get_prop(f, l, "Author", dr.author)) {
        std::cout << "*** Missing author: " << full_record_path << std::endl;
        return;
    }
    
    directory_table.insert(std::make_pair(ri, dr));
    return;
}

void read_data(const std::string& full_record_path, const RecordIdentifier& ri_data,
               std::ifstream& is, const std::fstream::streamoff& length,
               const RecordMapping& prev) {
    DataRecord data_rec;
    if (length < sizeof(DataRep)) {
        std::cout << "*** Data record too short: " << full_record_path << std::endl;
        return;
    }
    RecordIdentifier ri_dir(prev.first);
    if (prev.second != directory) {
        std::cout << "*** Data record not preceded by directory record: " << full_record_path << std::endl;
        std::exit(1);
    }
    auto i_dir = directory_table.find(ri_dir);
    if (i_dir == directory_table.end()) {
        std::cout << "*** Missing directory record: " << full_record_path << std::endl;
        std::exit(1);
    }

    // Create data_table record.
    data_rec.size = length - ((length + 4105) / 4106) * sizeof(DataRep); // 4106 = 4096 + sizeof(DataRep)
    is.seekg(0, is.beg);
    compute_attributes_from_file_stream(is, data_rec.crc32, data_rec.f_t);
    data_rec.rn_dir = std::get<2>(ri_dir);
    DirectoryRecord& dir_rec = (*i_dir).second;
    if (data_rec.size != dir_rec.size) {
        std::cout << "*** File size mismatch between data and directory: " << full_record_path << std::endl;
        std::exit(1);
    }
    data_table.insert(std::make_pair(ri_data, data_rec));
    
    // Create file_path_index record.
    file_path_index.insert(std::make_pair(file_path_from_directory_record(dir_rec), ri_data));
    
    // Create file_table record.
    FileRecord file_rec;
    file_rec.ri = ri_data;
    file_rec.dir_rec = dir_rec;
    file_rec.data_rec = data_rec;
    add_to_file_table(file_rec);
}

void read_record(std::string full_tape_path, std::string year, std::string tape, std::string record,
                 const RecordMapping& prev, RecordMapping& cur) {
    std::string full_record_path = join_paths(full_tape_path, record);
    std::ifstream is(full_record_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** restore_file couldn't open: " << full_record_path << " for reading" << std::endl;
        std::exit(1);
    }
    std::fstream::streamoff n = size(is); // get length of file
    if (n < sizeof(PrefixRep)) {
        std::cout << "*** Record too short to determine type: " + full_record_path + ": "
                  << long(n) << " bytes; skipping" << std::endl;
        return;
    }
    
    PrefixRep prefix;
    
    // Read prefix as a block:
    is.read((char *)&prefix, sizeof(PrefixRep));
    if (!is) {
        std::cout << "*** Only " << is.gcount() << " bytes of record prefix could be read" << std::endl;
        std::exit(1);
    }
    // Example: "84" => 84, "T10000P" => 10000, "file0019" => 19.
    RecordIdentifier ri(std::stoi(year), std::stoi(tape.substr(1, 5)), std::stoi(record.substr(4, 4)));
    cur.first = ri;
    cur.second = prefix.recordType;
    record_table.insert(cur);
    
    if (prefix.recordType == directory) {
        read_directory(full_record_path, ri, is, n, prev);
    } else if (prefix.recordType == data) {
        read_data(full_record_path, ri, is, n, prev);
    } else if (prefix.recordType == status) {
        std::cout << "*** Status record, ignored: " << path_from_record_id(ri) << std::endl;
    } else {
        std::cout <<" *** Unknown record type, ignored: " << path_from_record_id(ri) << std::endl;
    }
}

// Check for redundant archive records, i.e., tape 84/10000.
void check_for_redundant_records() {
    std::cout << std::endl << "Check for redundant archive records" << std::endl;
    auto f = file_path_index.begin();
    auto l = file_path_index.end();
    while (f != l) {
        RecordIdentifier ri = f->second;
        // Look for duplicates
        auto f1 = f;
        ++f1;
        while (f1 != l && f->first == f1->first) {
            ++f1;
        }
        if (std::distance(f, f1) > 1) {
            std::cout << "*** Duplicates of " << f->first << ":" << std::endl;
            for (auto f2 = f; f2 != f1; ++f2) {
                auto i_data = data_table.find(ri);
                std::cout << "    Checksum:  " << std::hex << i_data->second.crc32 << std::dec
                << " ri: "
                << std::get<0>(f2->second) << " "
                << std::get<1>(f2->second) << " "
                << std::get<2>(f2->second) << std::endl;
            }
        }
        ++f;
    }
}

// Read images of the archives tapes, loading record_table, directory_table, and data_table.
void read_tapes(const std::string& path) {
    std::cout << std::endl << "Begin tape reading from path: " << path << std::endl;
    DirMap years;
    DirMap tapes;
    DirMap records;
    read_dirmap(path, years);
    // if (verbose) print_dirmap(path, years);
    for (auto iy = years.cbegin(); iy != years.cend(); ++iy) {
        std::string year = (*iy).first;
        if (exclude.find(year) != exclude.end()) {
            std::cout << "  Excluding year " << year << std::endl;
            continue;
        }
        if (S_ISDIR((*iy).second) && year.size() == 2 && isdigit(year[0]) && isdigit(year[1])) {
            std::string full_year_path = join_paths(path, year);
            tapes.clear();
            read_dirmap(full_year_path, tapes);
            // if (verbose) print_dirmap(full_year_path, tapes);
            for (auto it = tapes.cbegin(); it != tapes.cend(); ++it) {
                std::string tape = (*it).first;
                if (exclude.find(tape) != exclude.end()) {
                    std::cout << "  Excluding tape " << tape << std::endl;
                    continue;
                }
                if (S_ISDIR((*it).second)
                    && tape.size() == 7
                    && tape[0]=='T'
                    && isdigit(tape[1]) && isdigit(tape[2]) && isdigit(tape[3]) && isdigit(tape[4]) && isdigit(tape[5])
                    && tape[6]=='P'){
                    std::string full_tape_path = join_paths(full_year_path, tape);
                    records.clear();
                    read_dirmap(full_tape_path, records);
                    // if (verbose) print_dirmap(full_tape_path, records);
                    RecordMapping prev, cur;
                    prev.first = RecordIdentifier(0, 0, 0);
                    for (auto ir = records.cbegin(); ir != records.cend(); ++ir) {
                        std::string record = (*ir).first;
                        if (exclude.find(record) != exclude.end()) {
                            std::cout << "  Excluding record " << record << std::endl;
                            continue;
                        }
                        if (S_ISREG((*ir).second)
                            && record.size() == 8 & record.compare(0, 4, "file") == 0
                            && isdigit(record[4]) && isdigit(record[5]) && isdigit(record[6]) && isdigit(record[7])) {
                            read_record(full_tape_path, year, tape, record, prev, cur);
                            prev = cur;
                        }
                    }
                }
            }
        }
    }
    check_for_redundant_records();
    std::cout << std::endl << "End tape reading; statistics: " << std::endl
              << record_table.size() << " records, "
              << directory_table.size() << " directory entries, "
              << data_table.size() << " data, "
              << file_table.size() << " file table entries"
              << std::endl;
}

// Restore file from data record file to designated path, skipping data headers.
void restore_file(const std::string& path_from, const std::string& path_to) {
    if (verbose)
        std::cout << "Restore " << path_from << " to " << path_to << std::endl;
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** restore_file couldn't open: " << path_from << " for reading" << std::endl;
        std::exit(1);
    }
    std::ofstream os(path_to);
    if (!os) {
        std::cout << "*** restore_file couldn't open: " << path_to << " for writing" << std::endl;
        std::exit(1);
    }
    auto n = size(is); // length of file
    std::fstream::streamoff i = 0;
    while(i + sizeof(DataRep) < n) {
        i += sizeof(DataRep);
        is.seekg(i, is.beg);
        char buffer[4096];
        std::fstream::streamoff m = n - i;
        if (4096 < m) m = 4096;
        is.read(buffer, m);
        i += m;
        os.write(buffer, m);
    }
    if (i != n) {
        std::cout << "*** restore_file terminated early: " << n - i << " bytes remaining" << std::endl;
        std::exit(1);
    }
}

/* Read the Nova/Alto DM (dump/load) file at path dm_path, extract the constituent files,
   and write them in the directory with path out_dir_path.
   Use dm_file_rec.dir_rec.creation_date for any file lacking a DATE_BLOCK.
   References:
   1. Section 5 of the Executive User's Guide in the Alto Subsystems Manual.
   2. DumpLoad.bcpl in [Indigo]<AltoSource>EXECUTIVE.DM!12
 */
void load_DM_file(const std::string& dm_path,
                  const std::string& out_dir_path,
                  const FileRecord&  dm_file_rec,
                  bool list_only) {
    std::ifstream is(dm_path, std::ifstream::in | std::ifstream::binary);
    std::string   name;
    std::string   output_path;
    time_t        output_date = dm_file_rec.dir_rec.creation_date;
    std::ofstream os;
    bool          output_open = false;
    auto          cleanup = [&] () {
                      if (output_open) {
                          std::fstream::streamoff size = os.tellp();
                          os.close();
                          output_open = false;
                          if (output_date == 0) {
                              std::cout << "*** DATE_BLOCK missing: " << dm_path << std::endl;
                              output_date = dm_file_rec.dir_rec.write_date;
                          }
                          set_file_write_time(output_path, output_date);
                          
                          // Create file_table record
                          FileRecord file_rec = dm_file_rec;
                          file_rec.dir_rec.dir_path += ">"
                                + append_version(file_rec.dir_rec.file_name, file_rec.dir_rec.version)
                                + "_";
                          file_rec.dir_rec.file_name = name;
                          file_rec.dir_rec.version = 0;
                          file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date = output_date;
                          // file_rec.dir_rec.text set below
                          file_rec.dir_rec.size = size;
                          compute_attributes_from_file(output_path,
                                                       file_rec.data_rec.crc32,
                                                       file_rec.data_rec.f_t);
                          file_rec.dir_rec.text = file_rec.data_rec.f_t <= text;
                          file_rec.data_rec.size = size;
                          add_to_file_table(file_rec);
                      }
                  };
    if (!is) return;
    if (verbose) std::cout << "  Loading DM file: " << dm_path << std::endl;
    while (true) {
        unsigned char block_type = is.get();
        if (!is.good()) break;
        switch (block_type) {
            case NAME_BLOCK: {
                if (unpack_dm_files) cleanup();
                // Start of a file
                int attr;
                attr = is.get() << 8;
                attr += is.get(); // always 0 from Alto's Dump
                if (attr != 0) {
                    std::cout << "*** Nonzero attributes in NAME_BLOCK of:" << dm_path << std::endl;
                    // std::exit(1); // ***** Should this be fatal ?????
                }
                
                name = "";
                while (true) {
                    unsigned char c = is.get();
                    if (c == 0) break;
                    name += c;
                };
                if (list_only || verbose) std::cout << "    NAME_BLOCK: " << name << std::endl;
                if (unpack_dm_files) {
                    output_path = join_paths(out_dir_path, name);
                    os.open(output_path, std::ofstream::out | std::ifstream::binary);
                    output_open = true;
                    output_date = dm_file_rec.dir_rec.creation_date;
                }
                break;
            }
            case DATA_BLOCK: {
                // Up to 256 data bytes, preceded by a byte count and a checksum
                uint16_t byte_count;
                uint8_t b0 = is.get();
                uint8_t b1 = is.get();
                byte_count = (b0 << 8) + b1;
                if (byte_count > 256) {
                    std::cout << "*** DATA_BLOCK byte count > 256: " << byte_count << std::endl;
                    std::exit(1);
                }
                uint16_t checksum, expected;
                b0 = is.get();
                b1 = is.get();
                expected = (b0 << 8) + b1;
                checksum = byte_count;
                
                uint8_t buffer[byte_count];
                is.read((char*)&buffer[0], byte_count);
                if (!is.good()) {
                    cleanup();
                    std::cout << "*** DATA_BLOCK shorter than byte count: " << dm_path << std::endl;
                    return;
                }
                
                // Checksum sums 16-bit words, skips final odd byte
                for (std::size_t i = 0; i < byte_count/2; ++i) {
                    uint16_t word;
                    word = buffer[2 * i] << 8;
                    word += buffer[2 * i + 1];
                    checksum += word;
                }
                if (checksum != expected) {
                    std::fstream::streamoff offset = os.tellp();
                    std::cout << "*** Bad checksum in " << name << " within " << dm_path << ": "
                              << std::hex << checksum << " expected: " << expected << " difference: " << (checksum - expected)
                              << std::dec << " around offset " << offset - byte_count << std::endl;
                } else {
                    // if (verbose) std::cout << "*** Good checksum" << std::endl;
                }
                
                //if (verbose) std::cout << "    DATA_BLOCK encountered for offset: " << os.tellp() << std::endl;
                if (unpack_dm_files)
                    os.write((char*)&buffer[0], byte_count);
                break;
            }
            case ERROR_BLOCK:
                // Fatal error
                std::cout << "*** ERROR_BLOCK encountered" << std::endl;
                std::exit(1);
            case END_BLOCK: {
                // End of DM file
                if (verbose) std::cout << "  END DM file" << std::endl;
                cleanup();
                break;
            }
            case DATE_BLOCK: {
                // Last-written date for current file
                unsigned char bytes[4];
                is.read((char*)bytes, 4); // seconds since 1 January 1901
                is.get(); is.get(); // ignore last Alto word
                output_date = alto_TIME_to_time_t(bytes);
                //if (verbose) std::cout << "    DATE_BLOCK: " << date_to_string(output_date) << std::endl;
                break;
            }
            default:
                break;
        }
    }
}

void load_composite_files(const FileRecord* file_rec, const std::string& path_to_dir, const std::string& path_to_file) {
    // path_to_dir is path to directory containing the composite file (path_to_file)
    const bool list_only = false; // ***** Command line option ?????
    auto f = file_rec->dir_rec.file_name.rfind('.');
    if (f != std::string::npos) {
        std::string ext(file_rec->dir_rec.file_name.substr(f, std::string::npos));
        // If file extension is .dm, unpack it
        if (unpack_dm_files) {
            if (ci_equal(ext, ".dm")) {
                std::string path_to_comp_dir(path_to_file + "_");
                mkpath_np(path_to_comp_dir.c_str(), 0777);
                load_DM_file(path_to_file, path_to_comp_dir, *file_rec, list_only);
            }
        }
        // If file extension is ".altodisk", ".bfs", or ".copydisk", unpack it
        if (unpack_disk_images) {
            if (ci_equal(ext, std::string(".altodisk"))
                || ci_equal(ext, std::string(".bfs"))
                || ci_equal(ext, std::string(".copydisk"))) {
                if (file_rec->dir_rec.file_name == dual_pack_dp0) {
                    // We can't proceed until we also have _dp1.
                } else if (file_rec->dir_rec.file_name == dual_pack_dp1) {
                    // Hopefully we already loaded _dp0 and both files have the same version number!
                    std::string dp1_path(join_paths(path_to_dir, append_version(dual_pack_dp1, file_rec->dir_rec.version)));
                    load_dual_disk_image(
                        join_paths(path_to_dir, append_version(dual_pack_dp0, file_rec->dir_rec.version)),
                        dp1_path,
                        dp1_path + "_",
                        file_rec);
                } else {
                    std::string path_to_comp_dir(path_to_file + "_");
                    mkpath_np(path_to_comp_dir.c_str(), 0777);
                    load_disk_image(path_to_file, path_to_comp_dir, file_rec);
                }
            }
        }
    }
}

// Restore images of the original file servers from the archive tape images.
// Precondition: read_tapes has executed.
void restore_tapes() {
    std::cout << std::endl << "Begin restoration to path: " << restore_path << std::endl;
    if (unpack_dm_files)    std::cout << "  Dump files (.dm) will be unpacked\n";
    if (unpack_disk_images) std::cout << "  Disk image files (.altodisk, .bfs, .copydisk) will be unpacked\n";
    if (!dual_pack_dp0.empty()) std::cout << "    Dual pack filesystem " << dual_pack_dp0 << " and " << dual_pack_dp1
                                          << " will be unpacked to " << dual_pack_dp1 + "_" << "\n";
    
    std::vector<FileRecord>::size_type i = 0;
    std::vector<FileRecord>::size_type n = file_table.size();
    // Note load_DM_file will expand file_table, so iterators into it could become invalid.
    while (i < n) {
        FileRecord file_rec = file_table[i];
        
        // Create directory path if necessary
        std::string dir_path = join_paths(file_rec.dir_rec.server, subst(file_rec.dir_rec.dir_path, '>', '/'));
        std::string path_to_dir(join_paths(restore_path, dir_path));
        mkpath_np(path_to_dir.c_str(), 0777);
        
        
        // Restore data to file, skipping data headers
        std::string path_from(join_paths(archive_path, path_from_record_id(file_rec.ri)));
        std::string path_to_file(join_paths(path_to_dir, append_version(file_rec.dir_rec.file_name, file_rec.dir_rec.version)));
        restore_file(path_from, path_to_file);
        set_file_write_time(path_to_file, file_rec.dir_rec.creation_date);
        
        // Handle .dm files and disk image files (.altodisk, .bfs, .copydisk)
        load_composite_files(&file_rec, path_to_dir, path_to_file);
        ++i;
    }
    std::cout << std::endl << "End restoration to path: " << restore_path << std::endl;
    if (unpack_dm_files) std::cout << "  " << (file_table.size() - n) << " new file table entries" << std::endl;
}

// Assuming file_name ends with ".~V~" (independent of --tilde), return V (version number).
uint16_t get_version(const std::string& file_name) {
    auto f = file_name.find('~');
    auto l = file_name.rfind('~');
    if (f != l) {
        ++f; // advance to first digit of version number
        return std::stoi(file_name.substr(f, l - f));
    }
    return 0;
}

// Assuming file_name ends with ".~V~" (independent of --tilde), return file_name with these characters removed.
std::string remove_version(const std::string& file_name) {
    auto f = file_name.find('~');
    auto l = file_name.rfind('~');
    if (f == l || file_name[f - 1] != '.') {
        std::cout << "*** No version to remove" << std::endl;
        return file_name;
    }
    --f; // back up to period preceding version
    return file_name.substr(0, f);
}

// Copy file to designated path.
void copy_file(const std::string& path_from, const std::string& path_to) {
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** copy_file couldn't open: " << path_from << " for reading" << std::endl;
        std::exit(1);
    }
    std::ofstream os(path_to, std::ofstream::trunc | std::ofstream::binary);
    if (!os) {
        std::cout << "*** copy_file couldn't open: " << path_to << " for writing" << std::endl;
        std::exit(1);
    }
    // Our DRAM is (much!) larger than the largest Alto disk drive.
    std::fstream::streamoff length = size(is);
    std::vector<char> buffer(length);
    is.read(&buffer[0], length);
    os.write(&buffer[0], length);
}

void read_extra_file(const std::string& server, // e.g., _cd6_
                     const std::string& root,   // e.g., /Users/.../_CD/cd6
                     const std::string& path,   // e.g., tapeserver/docs
                     const std::string& name) { // e.g., interimmftp.memo.~1~ (independent of --tilde)
    std::string dir_path(join_paths(root, path));
    std::string full_path = join_paths(dir_path, name);
    // Create file_table record.
    FileRecord file_rec;
    file_rec.dir_rec.server = server;
    file_rec.dir_rec.dir_path = subst(path, '/', '>');
    file_rec.dir_rec.file_name = remove_version(name);
    file_rec.dir_rec.version = get_version(name);
    file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date
                                = file_rec.dir_rec.read_date
                                = get_file_write_time(full_path);
    file_rec.dir_rec.size = get_file_size(full_path);
    file_rec.dir_rec.author = "UNKNOWN";
    compute_attributes_from_file(full_path,
                                 file_rec.data_rec.crc32,
                                 file_rec.data_rec.f_t);
    file_rec.dir_rec.text = file_rec.data_rec.f_t <= text;
    file_rec.data_rec.size = file_rec.dir_rec.size;
    add_to_file_table(file_rec);
    
    // Create directory path if necessary.
    std::string restore_dir_path = join_paths(file_rec.dir_rec.server, path);
    std::string path_to_dir(join_paths(restore_path, restore_dir_path));
    mkpath_np(path_to_dir.c_str(), 0777);
    
    // Copy file to restored directory.
    std::string path_to_file(join_paths(path_to_dir,
                                              append_version(file_rec.dir_rec.file_name,
                                                             file_rec.dir_rec.version)));
    if (verbose)
        std::cout << "Copy file " << full_path << " to " << path_to_file << std::endl;
    copy_file(full_path, path_to_file);
    set_file_write_time(path_to_file, file_rec.dir_rec.creation_date);
    
    // Handle .dm files and disk image files (.altodisk, .bfs, .copydisk).
    load_composite_files(&file_rec, path_to_dir, path_to_file);
}

void read_extra_directory(const std::string& server, const std::string& root, const std::string& path) {
    DirMap dirmap;
    std::string dir_path(join_paths(root, path));
    read_dirmap(dir_path, dirmap);
    // if (verbose) print_dirmap(dir_path, dirmap);
    auto f = dirmap.cbegin();
    auto l = dirmap.cend();
    while (f != l) {
        std::string name = f->first;
        if (S_ISDIR(f->second) && name[0] != '.') {
            read_extra_directory(server, root, join_paths(path, name));
        } else if (S_ISREG(f->second) && !ci_equal(name, "YMTRANS.TBL")) {
            read_extra_file(server, root, path, name);
        }
        ++f;
    }
}

// Read and copy the cd6 and cd8 hierarchies, loading file_table and auxiliary indexes and expanding .dm files.
void restore_extra_filesystems() {
    std::cout << std::endl << "Begin reading cd6, cd8 from path: " << extras_path << std::endl;
    if (unpack_dm_files)    std::cout << "  Dump files (.dm) will be unpacked\n";
    if (unpack_disk_images) std::cout << "  Disk image files (.altodisk, .bfs, .copydisk) will be unpacked\n";
    auto file_table_size = file_table.size();
    DirMap top_level;
    read_dirmap(extras_path, top_level);
    // if (verbose) print_dirmap(path, top_level);
    for (auto f = top_level.cbegin(); f != top_level.cend(); ++f) {
        std::string name = f->first;
        if (S_ISDIR(f->second) && name.size() == 3
            && name[0] == 'c' && name[1] == 'd' && (name[2] == '6' || name[2] == '8')) {
            read_extra_directory("_" + name + "_", join_paths(extras_path, name), "");
        }
    }
    std::cout << std::endl << "End reading cd6, cd8; statistics: " << std::endl
              << "  " << file_table.size() - file_table_size << " new file table entries"
              << std::endl;
}

void print_duplicates() {
    auto f = file_table_crc32_index.cbegin();
    auto l = file_table_crc32_index.cend();
    while (f != l) {
        Crc32 crc32 = f->first;
        auto n = file_table_crc32_index.count(crc32);
        if (n > 1) {
            std::string file_name = file_table[f->second].dir_rec.file_name;
            std::cout << std::hex << crc32 << std::dec << " " << file_name << "\n";
            while (n > 0) {
                const RecordIdentifier& ri = file_table[f->second].ri;
                const DirectoryRecord& dir_rec = file_table[f->second].dir_rec;
                std::string fn2("");
                if (!ci_equal(dir_rec.file_name, file_name)) fn2 = "* " + dir_rec.file_name;
                std::string version(std::to_string(dir_rec.version));
                if (dir_rec.version == 0) version = "-";
                std::cout << "    " << fn2
                << " " << dir_rec.server << " " << dir_rec.dir_path << " " << version << " "
                << ": " // end of sort key
                << dir_rec.size << " "
                << date_to_string(dir_rec.creation_date) << " "
                << dir_rec.author << " "
                << std::get<0>(ri) << " " << std::get<1>(ri) << " " << std::get<2>(ri) << "\n";
                ++f;
                --n;
            }
        } else ++f;
        
    }
}

// For each Alto filename, print a list of attributes.
// NOTE: This sorts file_table.
void print_cross_ref() {
//    std::cout << "\nCross reference" << std::endl;
//    std::cout << " " << file_table.size() << " entries including DM and disk image contents" << std::endl;
//    std::sort(file_table.begin(), file_table.end(), less_by_file_name);
//    auto f = file_table.cbegin();
//    auto l = file_table.cend();
//    while (f != l) {
//        std::cout << "  " << f->dir_rec.file_name << std::endl;
//        auto first = f;
//        do {
//            std::string version(std::to_string(f->dir_rec.version));
//            if (f->dir_rec.version == 0) version = "-";
//            std::string type;
//            std::cout << "    "
//                << f->dir_rec.server << " " << f->dir_rec.dir_path << " " << version << " "
//                << ": " // end of sort key
//                << f->dir_rec.size << " "
//                << std::hex << f->data_rec.crc32 << std::dec << " "
//                << date_to_string(f->dir_rec.creation_date) << " "
//                << file_type_to_string(f->data_rec.f_t) << " "
//                << f->dir_rec.author << " "
//                << std::get<0>(f->ri) << " " << std::get<1>(f->ri) << " " << std::get<2>(f->ri)
//                << "\n";
//            ++f;
//        } while (f != l && ci_equal(f->dir_rec.file_name, first->dir_rec.file_name));
//    }
}


typedef std::string Server;
std::map<Server, Directory> serves;

void usage() {
    std::cout << "Usage: restore_alto_files [--exclude=<name> | --in=<path to tape images> | --tilde\n" // DO NOT USE --tilde
                 "       | --out=<path to restore to> | --unpack_dm_files | --unpack_disk_images\n"
                 "       | --dual_pack=<filename>,<filename>,<dirname>"
                 "       | --extras=<path to cd6/8> | --verbose | --attributes=<filename>"
                 "       | --bravo=<filename> | --press=<filename>"
                 "       | --copydisk_image=<filename>[,<filename>]"
                 "       | --aar_image=<filename>[,<filename>]"
                 "       | --dm_file=<filename>"
                 "       | --web"
                 "       | --verbose ]..."
              << std::endl;
    std::exit(1);
}

int main(int argc, const char * argv[]) {

    std::cout << "restore_alto_files of 20 June 2017" << std::endl;
    init_c_c();
    validate_assumptions();
    // Read command line first, then execute in canonical order.
    for (int i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        if (arg.compare(0, 10, "--exclude=") == 0) {
            exclude.insert(arg.substr(10, std::string::npos));
        } else if (arg.compare(0, 5, "--in=") == 0) {
            archive_path = arg.substr(5, std::string::npos);
        } else if (arg.compare(0, 7, "--tilde") == 0) {
            tilde = true;
        } else if (arg.compare(0, 6, "--out=") == 0) {
            restore_path = arg.substr(6, std::string::npos);
        } else if (arg.compare(0, 17, "--unpack_dm_files") == 0) {
            unpack_dm_files = true;
        } else if (arg.compare(0, 20, "--unpack_disk_images") == 0) {
            unpack_disk_images = true;
        } else if (arg.compare(0, 12, "--dual_pack=") == 0) {
            std::string temp = arg.substr(12, std::string::npos);
            auto f = temp.find(',');
            if (f == std::string::npos) {
                std::cout << "Missing commas: " << arg << std::endl;
                usage();
            }
            dual_pack_dp0 = temp.substr(0, f);
            ++f;
            dual_pack_dp1 = temp.substr(f, std::string::npos);
        } else if (arg.compare(0, 9, "--extras=") == 0) {
            extras_path = arg.substr(9, std::string::npos);
        } else if (arg.compare(0, 13, "--attributes=") == 0) {
            attributes_path = arg.substr(13, std::string::npos);
        } else if (arg.compare(0, 8, "--bravo=") == 0) {
            bravo_path = arg.substr(8, std::string::npos);
        } else if (arg.compare(0, 8, "--press=") == 0) {
            press_path = arg.substr(8, std::string::npos);
        } else if (arg.compare(0, 17, "--copydisk_image=") == 0) {
            copydisk_image_path = arg.substr(17, std::string::npos);
        } else if (arg.compare(0, 12, "--aar_image=") == 0) {
            aar_image_path = arg.substr(12, std::string::npos);
        } else if (arg.compare(0, 10, "--dm_file=") == 0) {
            unpack_dm_files = true;
            dm_file_path = arg.substr(10, std::string::npos);
        } else if (arg.compare(0, 6, "--web=") == 0) {
            web_path = arg.substr(6, std::string::npos);
        } else if (arg.compare(0,  9, "--verbose") == 0) {
            verbose = true;
        } else {
            std::cout << "Unknown option: " << arg << std::endl;
            usage();
        }
    }
    
    std::cout << "  Command-line options: ";
    for (int i = 1; i < argc; ++i) {std::cout << argv[i] << " ";}
    std::cout << "\n";
    
    try {
        if (!archive_path.empty()) {
            read_tapes(archive_path);
            if (!restore_path.empty())
                restore_tapes(); // tilde flag controls filename version format
            if (!extras_path.empty())
                restore_extra_filesystems();
            if (!web_path.empty())
                // Must print duplicates before cross_ref, because latter invalidates file_table_crc32_index
                if (verbose) print_duplicates();
                altoweb::gen_pages(web_path, file_table, file_table_crc32_index, extension_table, author_table);
        }
        if (!attributes_path.empty()) {
            uint64_t crc32;
            file_type f_t;
            compute_attributes_from_file(attributes_path, crc32, f_t);
            std::cout << "Attributes of file " << attributes_path
            << ": crc32 = " << std::hex << crc32 << std::dec
            << " f_t: " << f_t << "\n";
        }
        if (!bravo_path.empty()) {
            bravo_to_html(bravo_path, bravo_path, bravo_path + ".html");
        }
        if (!press_path.empty()) {
            bool ps = true;
            press_to_pdf(press_path, press_path, press_path + ".ps", ps);
        }
        if (!copydisk_image_path.empty()) {
            load_copydisk_image(copydisk_image_path);
        }
        if (!aar_image_path.empty()) {
            load_aar_disk_image(aar_image_path);
        }
        if (!dm_file_path.empty()) {
            bool list_only = false;
            FileRecord dm_file_rec;
            dm_file_rec.dir_rec.creation_date = 0;
            dm_file_rec.dir_rec.write_date = 0;
            std::string path_to_comp_dir = dm_file_path + "_";
            mkpath_np(path_to_comp_dir.c_str(), 0777);
            load_DM_file(dm_file_path, path_to_comp_dir, dm_file_rec, list_only);
        }
    } catch (raf_exception p_e) {
        std::cout << "*** " << p_e.what() << std::endl;
        return 1;
    }

    return 0;
}


