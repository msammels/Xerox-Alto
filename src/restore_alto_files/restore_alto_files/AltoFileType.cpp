//
//  AltoFileType.cpp
//  restore_alto_files
//
//  Created by Paul McJones on 1/16/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#include <stdio.h>
#include "AltoFileType.h"

std::string file_type_to_string(file_type f_t) {
    switch (f_t) {
        case ascii: return "Text"; break;
        case smalltalk: return "Smalltalk"; break;
        case bravo: return "Bravo"; break;
        case press: return "Press"; break;
        default: return "Binary"; break;
    }
}
