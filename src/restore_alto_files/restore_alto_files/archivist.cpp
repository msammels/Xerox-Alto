//
//  archivist.cpp
//  restore_alto_files
//
//  Created by Paul McJones on 2/13/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#include <fstream>
#include <zlib.h>   // crc32
#include "archivist.h"
#include "bravo2html.h"
#include "utility.h"

// Compute crc32 and "file type" for given file stream, optionally skipping data headers.
void compute_attributes_from_file_stream(std::ifstream& is, uint64_t& crc, file_type& f_t, bool skip_data_headers) {
    std::fstream::streamoff n = size(is); // length of file
    std::size_t i = 0;
    crc = crc32(0L, Z_NULL, 0);
    file_type_parser f_t_p;
    while (i < n) {
        if (skip_data_headers) {
            if (i + sizeof(DataRep) >= n) {
                throw raf_exception("File to checksum terminated early: " + std::to_string(n - i) + " bytes remaining");
            }
            i += sizeof(DataRep);
            is.seekg(sizeof(DataRep), is.cur);
        }
        unsigned char buffer[4096];
        unsigned m = unsigned(n - i);
        if (m > 4096) m = 4096;
        is.read((char*)buffer, m);
        i += m;
        crc = crc32(crc, buffer, m);
        unsigned char* f = &buffer[0];
        unsigned char* l = &buffer[m];
        while (f != l) {
            f_t_p(*f);
            ++f;
        }
    }
    if (f_t_p.not_not_bravo_so_far && f_t_p.control_Z_seen)
        f_t = bravo;
    // else if (...) f_t = smalltalk;
    else if (f_t_p.ascii_so_far)
        f_t = ascii;
    // else if (...) f_t = press;
    else
        f_t = binary;
}

// Compute crc32 and "file type" for arbitrary file (no headers to skip).
void compute_attributes_from_file(const std::string& path_from, uint64_t& crc, file_type& f_t) {
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (is) compute_attributes_from_file_stream(is, crc, f_t, false);
    else throw raf_exception("Couldn't open: " + path_from + " for reading attributes");
}
