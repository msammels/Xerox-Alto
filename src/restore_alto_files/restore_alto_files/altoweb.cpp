//
//  altoweb.cpp
//  restore_alto_files
//
//  Created by Paul McJones starting 10/30/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include <cassert>
#include <fstream>
#include <memory>
#include <string>
#include <unistd.h>
#include <vector>
#include "altoweb.h"
#include "bfs.h"
#include "bravo2html.h"
#include "press2pdf.h"
#include "pstream.h"
#include "utility.h"
#include "web.h"

namespace altoweb {
    
    typedef std::string Name; // letter, digit, period, ...
    typedef Name ServerName;
    typedef Name DirectoryName;
    typedef std::string DirPath; // DirectoryName (">" DirectoryName)...
    typedef Name FileName;
    typedef int  Version;
    typedef Name Author;
    typedef uint64_t Crc32;
    
    typedef ci_less name_less;
    
    struct Directory;
    
    typedef std::map<DirectoryName, std::shared_ptr<Directory>, name_less> DirectoryDirs;
    
    typedef DirectoryDirs::iterator         DirectoryDirsIterator;
    typedef DirectoryDirs::const_iterator   DirectoryDirsConstIterator;
    
    typedef struct {
        time_t      creation_date;  // e.g., 31-Jan-81 19:59:27 PST
        time_t      write_date;     //       (same format)
        time_t      read_date;      //       (same format)
        file_type   f_t;
        std::fstream::streamoff size;   // in bytes
        Author      author;         // e.g., Murray
        Crc32       crc32;
        RecordIdentifier    ri;
    } File;

    // Weak ordering for DirectoryFiles map
    struct name_version_less {
        name_version_less() {}
        bool operator()(const std::pair<FileName, Version>& a,
                        const std::pair<FileName, Version>& b) const {
            if (name_less()(a.first, b.first)) return true;
            if (name_less()(b.first, a.first)) return false;
            return a.second < b.second;
        }
    };
    
    typedef std::map<std::pair<FileName, Version>, File, name_version_less> DirectoryFiles;
    
    typedef DirectoryFiles::iterator        DirectoryFilesIterator;
    typedef DirectoryFiles::const_iterator  DirectoryFilesConstIterator;
    
    struct Directory {
        DirectoryDirs directories;
        DirectoryFiles files;
    };
    
    std::map<ServerName, Directory, name_less> server; // but no files at top level
    
    typedef std::map<ServerName, Directory, name_less>::iterator        ServerIterator;
    typedef std::map<ServerName, Directory, name_less>::const_iterator  ServerConstIterator;
    
    // Ensure that the directories from sn through dp exist, returning pointer to the last one
    Directory* make_path(ServerName sn, DirPath dp) {
        std::pair<ServerIterator, bool> s_pair = server.insert(std::make_pair(sn, Directory()));
        Directory* dir = &(s_pair.first->second);
        std::string::size_type i = 0;
        std::string::size_type j;
        do {
            j = dp.find(">", i);
            std::string dir_name(dp.substr(i, j == std::string::npos ? j : j - i));
            std::pair<DirectoryDirsIterator, bool> d_pair
                = dir->directories.insert(std::make_pair(dir_name,
                                                         std::shared_ptr<Directory>(new Directory())));
            dir = &(*d_pair.first->second);
            if (j != std::string::npos)
                i = j + 1;
            else
                i = j;
        } while (i != std::string::npos);
        return dir;
    }

    void add_file(const FileRecord& f) {
        Directory* dir = make_path(f.dir_rec.server, f.dir_rec.dir_path);
        std::pair<DirectoryFilesIterator, bool> d_pair =
            dir->files.insert(std::make_pair(std::make_pair(f.dir_rec.file_name, f.dir_rec.version), File()));
        File& file = d_pair.first->second;
        file.creation_date = f.dir_rec.creation_date;
        file.write_date = f.dir_rec.write_date;
        file.read_date = f.dir_rec.read_date;
        file.f_t = f.data_rec.f_t;
        file.size = f.dir_rec.size;
        file.author = f.dir_rec.author;
        file.crc32 = f.data_rec.crc32;
        file.ri = f.ri;
    }
    
    // Weak ordering on file records, for building web model.
    bool less_by_full_path(const FileRecord& a, const FileRecord& b) {
        if (ci_less()(a.dir_rec.server, b.dir_rec.server))          return true;
        if (ci_less()(b.dir_rec.server, a.dir_rec.server))          return false;
        if (ci_less()(a.dir_rec.dir_path, b.dir_rec.dir_path))      return true;
        if (ci_less()(b.dir_rec.dir_path, a.dir_rec.dir_path))      return false;
        if (ci_less()(a.dir_rec.file_name, b.dir_rec.file_name))    return true;
        if (ci_less()(b.dir_rec.file_name, a.dir_rec.file_name))    return false;
        if (a.dir_rec.version < b.dir_rec.version)                  return true;
        if (b.dir_rec.version < a.dir_rec.version)                  return false;
        return false;
    }
    
    void build_model(std::vector<FileRecord>& file_table) {
        std::sort(file_table.begin(), file_table.end(), less_by_full_path);
        auto f = file_table.cbegin();
        auto l = file_table.cend();
        while (f != l) {
            add_file(*f);
            ++f;
        }
    }

    // Generate a page with a (partial) octal dump of the file contents.
    void gen_dump(std::ofstream& os, const std::string& file_path, const std::string& base_relative_file_path) {
        // Consider endianness if displaying more than a byte at a time.
        std::string command_prefix = "od -t oC -N 100000 "; // octal short; maximum of 100000 bytes
        os << "$ " << command_prefix << base_relative_file_path << std::endl;
        std::string command = command_prefix + file_path;
        
        redi::ipstream is(command);
        if (is.fail() || is.eof())
            std::cout << "*** gen_dump: od command failed to produce output\n";
        std::string line;
        while (std::getline(is, line)) {
            os << line << '\n';
        }
    }

    // Generate a page with a view of the file contents based on the file type.
    void gen_file_page(const std::string& dir_path, const std::string& base_relative_path,
                       const FileName& file_name, const File& file) {
        const std::string file_path = join_paths(dir_path, file_name);
        const std::string html_path = join_paths(dir_path, "." + file_name + ".html");
        std::string alto_file_path = base_relative_path;
        alto_file_path.replace(alto_file_path.find('/'), 1, "]&lt;");
        alto_file_path = "[" + alto_style_dir_name(alto_file_path) + file_name;
        const std::string title = "File " + alto_file_path;
        
        auto gen_ascii_contents =
            [&](std::ofstream& os, const std::string& indent) {
                std::ifstream is(file_path, std::ifstream::in | std::ifstream::binary);
                if (!is) throw raf_exception("gen_file_page could not open " + file_path);
                std::fstream::streamoff length = size(is);
                
                std::string s;
                s.resize(length);
                is.read(&s[0], length);
                s = escape_ascii(s);
                os.write(&s[0], s.size());
            };
        
        auto gen_binary_contents =
            [&](std::ofstream& os, const std::string& indent) {
                gen_dump(os, file_path, alto_file_path);
            };
        
        if (file.f_t == ascii)
            gen_preformatted_page(html_path, title, gen_ascii_contents);
//        else if (file.f_t == smalltalk)
//            smalltalk_to_html(file_path, html_path);
        else if (file.f_t == bravo)
            bravo_to_html(alto_file_path, file_path, html_path);
        else if (file.f_t == press) {
            const std::string view_name = "." + file_name + ".pdf";
            const std::string view_path = join_paths(dir_path, view_name);
            try {
                press_to_pdf(alto_file_path, file_path, view_path);
            } catch (raf_exception p_e) {
                std::cout << "*** press_to_pdf(" << file_path << ") " << p_e.what() << std::endl;
            }
        }
        else // binary
            gen_preformatted_page(html_path, title, gen_binary_contents);
    }
    
    void gen_logo(std::ofstream&os) {
        gen_div(os, "", [](std::ofstream& os, const std::string& indent) {
            // *** Should have gen_image; gen_anchor should take a contents function.
            gen_anchor(os, "http://www.computerhistory.org", "<img src=\"http://www.computerhistory.org/_common/img/homev2/CHM-logo.jpg\" width=\"108px\" title=\"Computer History Museum Logo\" alt=\"Computer History Museum Logo\" />");
        });
    }
    
    const bool ordered = true;
    const std::string style_monospaced = "style=\"font: 10pt monospace\"";
    
    // Generate a page with links to each subdirectory and to each file
    void gen_dir_page(const std::string& dir_path, const std::string& base_relative_path,
                      const DirectoryName& dir_name, const Directory& dir) {
        // ***** title should be full path with "breadcrumb" links
        const std::string base_relative_dir_path = join_paths(base_relative_path, dir_name);
        std::string server_and_dir_path = base_relative_dir_path;
        server_and_dir_path.replace(server_and_dir_path.find('/'), 1, "]&lt;");
        const std::string title = "Directory [" + alto_style_dir_name(server_and_dir_path);
        
        auto gen_dir_contents =
        [&](std::ofstream& os, const std::string& indent) {
            gen_logo(os);
            
            gen_heading(os, 1, title);
            
            os << indent; gen_heading(os, 2, "Subdirectories");

            DirectoryDirsConstIterator f_dirs = dir.directories.cbegin();
            DirectoryDirsConstIterator l_dirs = dir.directories.cend();
            std::function<void (DirectoryDirsConstIterator f)> gen_dir_contents = [&os](DirectoryDirsConstIterator f) {
                const DirectoryName subdir_name = f->first;
                gen_anchor(os, join_paths(subdir_name, ".index.html"), alto_style_dir_name(subdir_name));
            };
            gen_list(os, indent, f_dirs, l_dirs, gen_dir_contents, ordered, style_monospaced);
            
            os << indent; gen_heading(os, 2, "Files");
            
            DirectoryFilesConstIterator f_files = dir.files.cbegin();
            DirectoryFilesConstIterator l_files = dir.files.cend();
            std::function<void (DirectoryFilesConstIterator f)> gen_file_contents = [&os](DirectoryFilesConstIterator f) {
                const Version version = f->first.second;
                const FileName file_name = append_version(f->first.first, version);
                const File& file = f->second;
                const std::string view_name = "." + file_name + (file.f_t == press ? ".pdf" : ".html");
                gen_anchor(os, view_name, file_name);
                os << " " << file_type_to_string(file.f_t) << " ";
                gen_anchor(os, file_name, "(raw)");
                os << " ";
                os << file.size
                << " " << date_to_string(file.creation_date)
                << " " << file.author;
            };
            gen_list(os, indent, f_files, l_files, gen_file_contents, ordered, style_monospaced);
        };
        
        // Generate the page representing the directory
        gen_page(join_paths(dir_path, ".index.html"), title, gen_dir_contents);
        
        // For each subdirectory, recurse
        auto f_dirs = dir.directories.cbegin();
        auto l_dirs = dir.directories.cend();
        while (f_dirs != l_dirs) {
            const DirectoryName subdir_name = f_dirs->first;
            std::string subdir_path = join_paths(dir_path, subdir_name);
            const Directory& subdir = *f_dirs->second;
            mkpath_np(subdir_path.c_str(), 0777);
            gen_dir_page(subdir_path, base_relative_dir_path, subdir_name, subdir);
            ++f_dirs;
        }
        
        // For each file, generate a viewable proxy if possible
        auto f_files = dir.files.cbegin();
        auto l_files = dir.files.cend();
        while (f_files != l_files) {
            const Version version = f_files->first.second;
            const FileName file_name = append_version(f_files->first.first, version);
            const File& file = f_files->second;
            gen_file_page(dir_path, base_relative_dir_path, file_name, file);
            ++f_files;
        }
    }
    
    // Generate a page with links to each directory on given server.
    void gen_server_page(const std::string& web_path, ServerConstIterator f_server) {
        const ServerName& server_name = f_server->first;
        const Directory& dir = f_server->second;
        const std::string title = "Server [" + server_name + "]";
        const std::string server_path = join_paths(web_path, server_name);
        mkpath_np(server_path.c_str(), 0777);
        auto f_dirs = dir.directories.cbegin();
        auto l_dirs = dir.directories.cend();
        gen_page(join_paths(server_path, ".index.html"),
                 title,
                 [&, f_dirs, l_dirs](std::ofstream& os, const std::string& indent) {
                     gen_logo(os);
                     gen_heading(os, 1, title);
                     gen_heading(os, 2, "Directories");
                     std::function<void (DirectoryDirsConstIterator f)> gen_server_contents
                        = [&os](DirectoryDirsConstIterator f) {
                            const DirectoryName dir_name = f->first;
                            // These are top-level directories, so begin each name with "<" (escaped for HTML as "&lt;").
                            gen_anchor(os, join_paths(dir_name, ".index.html"), "&lt;" + alto_style_dir_name(dir_name));
                        };
                     gen_list(os, indent, f_dirs, l_dirs, gen_server_contents, ordered, style_monospaced);
                 });
        // For each directory, generate a page with links to each subdirectory and to each file
        while (f_dirs != l_dirs) {
            const DirectoryName& dir_name = f_dirs->first;
            const Directory& dir = *f_dirs->second;
            const std::string dir_path = join_paths(server_path, dir_name);
            mkpath_np(dir_path.c_str(), 0777);
            gen_dir_page(dir_path, server_name, dir_name, dir);
            ++f_dirs;
        }
    }

    std::string extension_frequencies_name = "extension_frequencies.html";
    std::string author_frequencies_name = "author_frequencies.html";
    std::string cross_ref_name = "cross_reference.html";
    std::string conversion_log_name = "conversion_log.html";
    
    template <typename T, typename V, typename R>
    //    requires(Regular(T) && Regular(V) && StrictWeakOrder(R))
    void gen_frequencies_page(const std::string& web_path, const std::map<T, V, R>& table, const std::string& title) {
        auto gen_contents = [&](std::ofstream& os, const std::string& indent){
            gen_logo(os);
            gen_heading(os, 1, title);
            typedef typename std::map<T, V, R>::const_iterator I;
            gen_list<I>(os, indent,
                        table.cbegin(), table.cend(),
                        [&](I f) -> void {os << indent << "  " << f->first << " " << f->second << "\n";}, ordered, style_monospaced);
        };
        gen_page(web_path, title, gen_contents);
    }
    
    // Ordering on file records, for print_cross_ref.
    bool less_by_file_name(const FileRecord& a, const FileRecord& b) {
        if (ci_less()(a.dir_rec.file_name, b.dir_rec.file_name))    return true;
        if (ci_less()(b.dir_rec.file_name, a.dir_rec.file_name))    return false;
        if (ci_less()(a.dir_rec.server, b.dir_rec.server))          return true;
        if (ci_less()(b.dir_rec.server, a.dir_rec.server))          return false;
        if (ci_less()(a.dir_rec.dir_path, b.dir_rec.dir_path))      return true;
        if (ci_less()(b.dir_rec.dir_path, a.dir_rec.dir_path))      return false;
        if (a.dir_rec.version < b.dir_rec.version)                  return true;
        if (b.dir_rec.version < a.dir_rec.version)                  return false;
        return false;
    }
    
    void gen_cross_reference(const std::string& web_path, std::vector<FileRecord>& file_table) {
        const std::string title = "Cross reference by file name";
        auto gen_contents = [&](std::ofstream& os, const std::string& indent){
            gen_logo(os);
            gen_heading(os, 1, title);
            os << indent << "<ul " << style_monospaced << ">\n";
            os << indent << "  <li>file_name\n";
            os << indent << "    <ul><li>full_name length crc32 creation (date time) file_type author archive_id(year tape record)</li></ul>\n";
            os << indent << "  </li>\n";
            os << indent << "</ul>\n";
            auto f = file_table.cbegin();
            auto l = file_table.cend();
            os << indent << "<ul " << style_monospaced << ">\n";
            while (f != l) {
                os << indent << "  <li>";
                os << f->dir_rec.file_name << "\n";
                os << indent << "    <ul>\n";
                auto first = f;
                do {
                    std::string web_dir_path = join_paths(join_paths(f->dir_rec.server,
                                                                     subst(f->dir_rec.dir_path, '>', '/')),
                                                          ".index.html");
                    os  << indent << "      <li>"
                        << "<a href=\"" << web_dir_path << "\">"
                            << "[" << f->dir_rec.server << "]&lt;" << f->dir_rec.dir_path
                        << "&gt;</a>"
                        << append_version(f->dir_rec.file_name, f->dir_rec.version) << " "
                        // end of sort key
                        << f->dir_rec.size << " "
                        << std::hex << f->data_rec.crc32 << std::dec << " "
                        << date_to_string(f->dir_rec.creation_date) << " "
                        << file_type_to_string(f->data_rec.f_t) << " "
                        << f->dir_rec.author << " "
                        << std::get<0>(f->ri) << " " << std::get<1>(f->ri) << " " << std::get<2>(f->ri)
                        << "</li>\n";
                    ++f;
                } while (f != l && ci_equal(f->dir_rec.file_name, first->dir_rec.file_name));
                os << indent << "    </ul>\n";
                os << indent << "  </li>\n";
            }
            os << indent << "</ul>\n";
        };
        
        std::sort(file_table.begin(), file_table.end(), less_by_file_name);
        gen_page(join_paths(web_path, cross_ref_name), title, gen_contents);
    }
    
    // Generate a top-level page with a list of links to servers.
    void gen_top_level_page(const std::string& web_path,
                            std::vector<FileRecord>& file_table,
                            std::multimap<Crc32, std::vector<FileRecord>::size_type>file_table_crc32_index,
                            std::map<Extension, int, ci_less>extension_table,
                            std::map<Author, int, ci_less> author_table) {
        
        const std::string title = "Xerox PARC Alto filesystem archive";
        auto gen_top_level_contents =
        [&](std::ofstream& os, const std::string& indent) {
            gen_logo(os);
            
            gen_heading(os, 1, title);
            
            // *** Should use gen_anchor; gen_paragraph should take a contents function.
            gen_paragraph(os, "", "The <a href=\"http://www.parc.com\">Palo Alto Research Center</a> (a <a href=\"http://www.xerox.com\">Xerox</a> company) has authorized the Computer History Museum to provide public viewing of the software, documents, and other files on this web site, and to provide these same files to private individuals and non-profit institutions with the same rights granted to CHM and subject to the same obligations undertaken by CHM. For more information about these files, see this <a href=\"xerox_alto_file_system_archive.html\">explanatory information</a> and <a href=\"http://www.computerhistory.org/atchm/xerox-alto-source-code/\">@CHM post</a>.");
            
            gen_heading(os, 2, "Servers");
            
            // For each server
            gen_list<ServerConstIterator>(os, indent, server.cbegin(), server.cend(),
                                          [&os](ServerConstIterator f) -> void {
                                              const ServerName& server_name = f->first;
                                              gen_anchor(os, join_paths(server_name, ".index.html"), "[" + server_name + "]");
                                          }, ordered, style_monospaced);
            
            gen_heading(os, 2, "Summaries");
            
            gen_paragraph(os, "", std::to_string(file_table.size()) + " total files; "
                                  + std::to_string(key_count(file_table_crc32_index)) + " unique crc32s");
            gen_anchor(os, extension_frequencies_name, "Extension frequencies", para);
            gen_anchor(os, author_frequencies_name, "Author frequencies", para);
            gen_anchor(os, cross_ref_name, "Cross-reference by name", para);
            gen_anchor(os, conversion_log_name, "Conversion log", para);
        };
        gen_page(join_paths(web_path, "index.html"),
                 title,
                 gen_top_level_contents);
    }
    
    void gen_pages(const std::string& web_path,
                   std::vector<FileRecord>& file_table,
                   std::multimap<Crc32, std::vector<FileRecord>::size_type>file_table_crc32_index,
                   std::map<Extension, int, ci_less>extension_table,
                   std::map<Author, int, ci_less> author_table) {
        std::cout << "\nGenerate web pages." << std::endl;

        mkpath_np(web_path.c_str(), 0777);
        
        build_model(file_table);
        
        // Generate a page with a link to each server.
        gen_top_level_page(web_path, file_table, file_table_crc32_index, extension_table, author_table);
        
        // For each server, generate a page with a link to each directory.
        auto f_server = server.cbegin();
        auto l_server = server.cend();
        while (f_server != l_server) {
            gen_server_page(web_path, f_server);
            ++f_server;
        }
        
        // Generate summary pages.
        std::cout << "\nGenerating frequency tables.\n";
        gen_frequencies_page(join_paths(web_path, extension_frequencies_name), extension_table, "Extension frequencies");
        gen_frequencies_page(join_paths(web_path, author_frequencies_name), author_table, "Author frequencies");
        std::cout << "\nGenerating cross reference.\n"
                  << "  " << file_table.size() << " entries including DM and disk image contents." << std::endl;
        gen_cross_reference(web_path, file_table);
    }
}

