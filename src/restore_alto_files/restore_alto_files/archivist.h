//
//  archivist.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/12/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_archivist_h
#define restore_alto_files_archivist_h

#include "AltoFileType.h"

/*
 These definitions are adapted from those in dumprecord.c,
 which are based on those from the Cedar Mesa program TapeArchiveMedia.mesa
 (as last edited by Tim Diebert: March 20, 1987 10:37:08 am PST).
 */

//typedef enum {
//	volume=0,
//	directory=1,
//	data=2,
//	status=3,
//	anon=255
//} RecordType;

typedef unsigned char RecordType;
const RecordType volume = 0;
const RecordType directory = 1;
const RecordType data = 2;
const RecordType status = 3;
const RecordType invalid = 255;

// Members common to Directory and Data records.
typedef struct {
	unsigned short fileNumber;
	unsigned short blockNumber;
	unsigned short sequenceNumber;
	unsigned char  wastedSpace;
	RecordType     recordType;
} PrefixRep;
typedef PrefixRep *Prefix;

typedef struct {
	PrefixRep      prefix;
	unsigned short fileNameStartLow;
	unsigned short fileNameStartHigh;
	unsigned short fileNameLengthLow;
	unsigned short fileNameLengthHigh;
	unsigned short propertyListStartLow;
	unsigned short propertyListStartHigh;
	unsigned short propertyListLengthLow;
	unsigned short propertyListLengthHigh;
	unsigned short gmtLow;
	unsigned short gmtHigh;
	unsigned short fileSizeLow;
	unsigned short fileSizeHigh;
	unsigned short length;
    char           varData[0];
	/* varData computed */
} DirectoryRep;
typedef DirectoryRep *Directory;

typedef struct {
	PrefixRep      prefix;
	unsigned short length;
    char           data[0];
	/* data computed */
} DataRep;
typedef DataRep *Data;

typedef int Year;           // e.g., 84
typedef int TapeNumber;     // e.g., 10000
typedef int RecordNumber;   // e.g., 19

typedef std::tuple<Year, TapeNumber, RecordNumber> RecordIdentifier;

typedef std::string Extension; // e.g., .mesa
typedef std::string Author; // e.g., Murray

typedef struct {
    std::string server;     // e.g., Ibis
    std::string dir_path;   // e.g., AltoGateway>Drivers
    std::string file_name;  // e.g., AltoSlaDriver.mesa
    int         version;    // e.g., 1
    time_t      creation_date;  // e.g., 31-Jan-81 19:59:27 PST
    time_t      write_date; //       (same format)
    time_t      read_date;  //       (same format)
    bool        text;       // otherwise it's binary
    std::fstream::streamoff     size;   // in bytes
    Author author;          // e.g., Murray
} DirectoryRecord;

typedef uint64_t Crc32;

typedef struct {
    RecordNumber            rn_dir; // same year and tape
    std::fstream::streamoff size;
    Crc32                   crc32;
    file_type               f_t;
} DataRecord;

typedef struct {
    RecordIdentifier    ri;    // e.g., <84, 10000, 20>
    DirectoryRecord     dir_rec;
    DataRecord          data_rec;
} FileRecord;

// Compute crc32 and "file type" for given file stream, optionally skipping data headers.
void compute_attributes_from_file_stream(std::ifstream& is, uint64_t& crc, file_type& f_t,
                                         bool skip_data_headers = true);

// Compute crc32 and "file type" for arbitrary file (no headers to skip).
void compute_attributes_from_file(const std::string& path_from, uint64_t& crc, file_type& f_t);


#endif
