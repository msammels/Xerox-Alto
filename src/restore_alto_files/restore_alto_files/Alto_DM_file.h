//
//  Alto_DM_file.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/14/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_Alto_DM_file_h
#define restore_alto_files_Alto_DM_file_h

const int NAME_BLOCK  = 0377;
const int DATA_BLOCK  = 0376;
const int ERROR_BLOCK = 0375;
const int END_BLOCK   = 0374;
const int DATE_BLOCK  = 0373;

/*
 
 General format of file is sequence of blocks:
 
 <name>[<date>]<data><data>...
 <name>[<date>]<data><data>...
 <end>
 
 An <error> block is fatal, and none are present in the archives donated by Xerox PARC to CHM.
 
 References:
 1. Section 5 of the Executive User's Guide in the Alto Subsystems Manual.
 2. DumpLoad.bcpl in [Indigo]<AltoSource>EXECUTIVE.DM!12
 
 */

#endif
