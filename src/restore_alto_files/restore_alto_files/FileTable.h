//
//  FileTable.h
//  restore_alto_files
//
//  Created by Paul McJones on 2/13/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_FileTable_h
#define restore_alto_files_FileTable_h

#include "archivist.h"

void add_to_file_table(FileRecord file_rec);

#endif
