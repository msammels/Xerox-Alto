//
//  bravo2html.h
//  restore_alto_files
//
//  Created by Paul McJones on 11/13/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.

#ifndef __restore_alto_files__bravo2html__
#define __restore_alto_files__bravo2html__

#include <string>

const int MAX_BRAVO_FILE_SIZE = 65536;

const int MICAS_PER_INCH  = 2540;
const int POINTS_PER_INCH =   72;

typedef struct {
    bool legal_in_ascii;    // 015 + [040, 0200) ***** how about 012 (NL) ?????
    bool legal_in_bravo;
    bool combining;         // overlays next character: hacek, umlaut, grave, acute, tilde, and breve accents
                            // Note: in Unicode, combining characters overlay previous character
    const char* replacement;
} char_class;

extern char_class c_c[256];

void init_c_c();

struct file_type_parser {
    bool ascii_so_far; // no character not legal_in_ascii
    bool not_not_bravo_so_far; // at least one ControlZ and no character not legal_in_bravo
    bool control_Z_seen;
    bool funny_seen; // ***** remove after sorting out purpose for these characters ????
    file_type_parser() : ascii_so_far(true), not_not_bravo_so_far(true), control_Z_seen(false), funny_seen(false) { }
    void operator()(unsigned char c) {
        if (not c_c[c].legal_in_ascii)
            ascii_so_far = false;
        if (not c_c[c].legal_in_bravo)
            not_not_bravo_so_far = false;
        if (c == 000)
            not_not_bravo_so_far = false; // ***** DEBUG
        if (c == 032)
            control_Z_seen = true;
        if (c == 0 || c == 012) funny_seen = true;
    }
};


std::string escape_ascii(const std::string& s);

std::string escape_bravo(const std::string& s);

std::string escape_smalltalk(const std::string& s);

void bravo_to_html(const std::string& alto_file_path, // only for HTML title
                   const std::string& in_path,
                   const std::string& out_path);

#endif /* defined(__restore_alto_files__bravo2html__) */
