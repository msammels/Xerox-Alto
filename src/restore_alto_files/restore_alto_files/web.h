//
//  web.h
//  restore_alto_files
//
//  Created by Paul McJones on 12/7/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef __restore_alto_files__web__
#define __restore_alto_files__web__

#include <fstream>

void gen_heading(std::ofstream& os, unsigned int level, const std::string& heading);

template <typename I>
// requires(Iterator(I)
void gen_list(std::ofstream& os, const std::string& indent, I f, I l,
              std::function<void (I f)> gen_contents,
              bool ordered = false, const std::string attributes = "") {
    std::string tag = ordered ? "ol" : "ul";
    os << indent << "<" << tag;
    if (attributes.size() != 0) os << " " << attributes;
    os  << ">\n";
    while (f != l) {
        os << indent << "  <li>";
            gen_contents(f);
        os << "</li>\n";
        ++f;
    }
    os << indent << "</" << tag << ">\n";
}

void gen_div(std::ofstream& os,
             const std::string& style,
             std::function<void (std::ofstream& os, const std::string& indent)> gen_contents);

void gen_span(std::ofstream& os, const std::string& style, const std::string& contents);

void gen_paragraph(std::ofstream& os, const std::string& style, const std::string& contents);

const bool para = true;

void gen_anchor(std::ofstream& os, const std::string& href, const std::string& contents, bool paragraph = false);

void gen_page(const std::string& path,
              const std::string& title,
              std::function<void (std::ofstream& os, const std::string& indent)> gen_contents);

void gen_preformatted_page(const std::string& path,
                           const std::string& title,
                           std::function<void (std::ofstream& os, const std::string& indent)> gen_contents);

#endif /* defined(__restore_alto_files__web__) */
