//
//  bravo2html.cpp
//  restore_alto_files
//
//  Created by Paul McJones starting 11/13/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.

#include <algorithm>
#include <cassert>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "bravo2html.h"
#include "utility.h"
#include "web.h"

extern bool verbose;

static std::string the_in_path;


// Encoding for standard text fonts

/* References:
    [1] Lyle Ramshaw. "Ensuing font cataclysm". Memorandum, Xerox PARC CSL, April 1, 1980.
        Originally stored as [Ivy]<Fonts>Memos>EnsuingCataclysm.bravo, .press;
        now available from http://bitsavers.org/pdf/xerox/alto/printing/AltoFontMemos.pdf .
    [2] Lyle Ramshaw. "Special Characters over the years". Memorandum, Xerox PARC CSL, July 19, 1980.
        Originally stored as [Ivy]<Fonts>Memos>SpecialChars.bravo, .press;
        now available from http://bitsavers.org/pdf/xerox/alto/printing/AltoFontMemos.pdf .
 */

/* Terminology (from [1]):
    1st generation font: produced before October 1978; "their origin is shrouded in antiquity".
    2nd generation font: released in November 1978 by Ron Pellar.
    3rd generation font: released in January 1980 by Ron Pellar.
 */

char_class c_c[256];

bool c_c_inited = false;

void init_c_c() {
    if (c_c_inited) return;
    for (int i = 0; i < 256; ++i)    c_c[i] = {false, false, nullptr};
    for (int i = 040; i < 0200; ++i) c_c[i] = { true,  true, nullptr};
    //  code      legal_in_ascii
    //  |         |      legal_in_bravo
    //  |         |      |     combining
    //  |         |      |     |      replacement      Ascii use  Xerox use
    //  +---------+------+-----+------+----------------+----------+
    c_c[000]  = { true,  true, false, ""};          // Control @ (nul): It's in _cd8_/altodocs/bravomanual.bravo!2 (by mistake?)
    c_c[001]  = { true,  true,  true, "&#780;"};    // Control A: hacek accent (combining)
    c_c[002]  = { true,  true, false, "&iquest;"};  // Control B: inverted question mark
    c_c[003]  = { true,  true, false, "c&#807;"};   // Control C: c with cedilla
    c_c[004]  = { true,  true,  true, "&#776;"};    // Control D: umlaut accent (combining)
    c_c[005]  = { true,  true,  true, "&#768;"};    // Control E: grave accent (combining)
    c_c[006]  = { true,  true, false, "&#64256;"};  // Control F: ff ligature
    c_c[007]  = { true,  true, false, "&lsquo;"};   // Control G: left single quote
    c_c[010]  = { true,  true, false, "&iexcl;"};   // Control H: inverted exclamation point
    c_c[011]  = { true,  true, false, nullptr};     // Control I (tab)
    c_c[012]  = { true,  true, false, nullptr};     // Control J (nl): ***** newline ?????
            // *** Newline seen in Indigo/AltoSource/BRIEFINGBLURB.DM!2_/BriefingBlurb.glossary
    c_c[013]  = { true,  true,  true, "&#769;"};    // Control K: acute accent (combining)
                                                    // ***** Circumflex represented as (Control K, Control E, char)
    c_c[014]  = { true,  true, false, nullptr};     // Control L (ff): ***** page break ?????
    c_c[015]  = { true,  true, false, "<br>"};      // Control M: cr
    c_c[016]  = { true,  true,  true, "&#772;"};    // Control N: minus sign (1st gen); macron accent (combining) (3rd gen)
    c_c[017]  = { true,  true, false, "&emsp;"};    // Control O: em quad
    c_c[020]  = { true,  true,  true, "&#771;"};    // Control P: tilde accent (combining)
    c_c[021]  = { true,  true, false, "&#64259;"};  // Control Q: ffi ligature
    c_c[022]  = { true,  true, false, "&#64260;"};  // Control R: ffl ligature
    c_c[023]  = { true,  true, false, "&mdash;"};   // Control S: em dash
    c_c[024]  = { true,  true, false, "&#64257;"};  // Control T: fi ligature
    c_c[025]  = { true,  true, false, "&#64258;"};  // Control U: fl ligature
    c_c[026]  = { true,  true, false, "&ndash;"};   // Control V: en dash
    c_c[027]  = { true,  true,  true, "&#774;"};    // Control W: breve accent (combining)
    c_c[030]  = { true,  true, false, "&minus;"};   // Control X: underline (1st gen); minus sign (2nd gen)
    c_c[031]  = { true,  true, false, "&#8199"};    // Control Y: figure space: &#8199 with Unicode
    c_c[032]  = {false,  true, false, nullptr};     // Control Z: start of looks in Bravo file
    c_c[033]  = { true,  true, false, nullptr};     // Control [ (esc): (used in Bravo macros)
    c_c[034]  = { true,  true, false, "&ensp;"};    // Control \: en quad
    c_c[035]  = { true,  true,  true, "xxx"};       // Control ]: low dot accent (combining)
    c_c[036]  = { true,  true,  true, "&#771;"};    // Control ^: tilde accent (combining)
    c_c[037]  = { true,  true,  true, "&#778;"};    // Control _: circle [ring] accent (combining)
    c_c[047]  = { true,  true, false, "&rsquo;"};   // '
    c_c[074]  = { true,  true, false, /*"&#12296;"*/ "&lt;"};  // <: left angle bracket [Times Roman/Helvetica only]
    c_c[076]  = { true,  true, false, /*"&#x3009;"*/ "&gt;"};  // >: right angle bracket [Times Roman/Helvetica only]
    c_c[0136] = { true,  true, false, "&uarr;"};    // ^: up arrow
    c_c[0137] = { true,  true, false, "&larr;"};    // _: left arrow
 // c_c[0140] = { true,  true, false, nullptr};     // ` (opening single quote)
    // + codes in eight-bit 3rd-generation fonts -- see [2].
    c_c_inited = true;
}

typedef std::vector<char>::const_iterator CharIterator;

typedef struct {
    int font = 0;
    int offset = 0;
    int tab_or_color = -1;
    bool underlined = false, bold = false, italic = false,
    graphic = false, visible = false,
    overstrike = false, vanished = false;
} char_looks;

void append_style(std::string& style, const std::string& s) {
    if (style.length() == 0) style = s;
    else {
        style += "; ";
        style += s;
    }
}

/* These colors from [_cd8_]<altodocs>bravo-file-format.press!2, page 4,
   do not agree with the usage in [_cd8_]<altodocs>bravo76.bravo!1, which we follow.
 t0 black
 t1 cyan
 t2 green
 t3 magenta
 t4 red
 t5 violet
 t6 yellow
 */

std::string color_name_from_number(int number) {
    // Colors from [_cd8_]<altodocs>bravo76.bravo!1, page 1 (25-Sep-1982):
    switch (number) {
        case 1: return "aqua";
        case 2: return "cyan";
        case 3: return "brown"; // should be "dark brown"
        case 4: return "green";
        case 5: return "lime";
        case 6: return "magenta";
        case 7: return "orange";
        case 8: return "pink";
        case 9: return "red";
        case 10: return "#BBC9CA"; // should be "smoke"; we use http://www.benjaminmoore.com/en-us/paint-color/smoke: 187 201 202
        case 11: return "turquoise";
        case 12: return "#9C00FF"; // should be "ultraviolet"; we made this up
        case 13: return "violet";
        case 14: return "yellow";
        default: return "black";
    }
}

/* From Bravo Manual, page 40 (description of standard non-programmer fonts):
 0 Times Roman, 10 pt. This is the standard font.
 1 Times Roman, 8 pt.
 2 XEROX logo (only the capital letters E O R and X)
 3 Math, 10 pt. A large set of mathematical symbols. No bold or italics on hardcopy.
 4 Greek, 10 pt. No bold or italics on hardcopy.
 5 Times Roman, 12 pt.
 6 Helvetica, 10 pt.
 7 Helvetica, 8 pt.
 8 Gacha, 10 pt. This is a fixed-pitch font.
 */

std::string assemble_span_style(const char_looks& c_l) {
    std::string style, name;
    int size;
    switch (c_l.font) {
        case 0: name = "serif"; size = 10; break;       // want Times Roman
        case 1: name = "serif"; size =  8; break;       // want Times Roman
        case 2: name = "monospace"; size = 18; break;   // really XEROX logo (just E O R X)
        case 3: name = "math"; size = 10; break;
        case 4: name = "greek"; size = 10; break;
        case 5: name = "serif"; size = 12; break;       // want Times Roman
        case 6: name = "sans-serif"; size = 10; break;  // want Helvetica
        case 7: name = "sans-serif"; size =  8; break;  // want Helvetica
        case 8: name = "monospace"; size = 10; break;   // want Gacha
        default: name = "serif"; size = 10; break;      // treat like 0
    }
    style = "font: ";
    if (c_l.bold) style += "bold ";
    if (c_l.italic) style += "italic ";
    style += std::to_string(size) + "pt " + name;
    if (c_l.offset != 0) append_style(style, "vertical-align: " + std::to_string(c_l.offset) + "pt");
    if (1 <= c_l.tab_or_color && c_l.tab_or_color <= 14) {
        if (verbose)
            std::cout << "*** bravo_to_html(" << the_in_path << "): colored "
                      << (c_l.underlined ? "background: " : "text: ") << c_l.tab_or_color << "\n";
        append_style(style, (c_l.underlined ? "background-color: " : "color: ") + color_name_from_number(c_l.tab_or_color));
    } else if (c_l.underlined) append_style(style, "text-decoration: underline");
    // if (c_l.graphic) ????? // display control character as special glyph -- see Bravo Manual, page 57
    // if (c_l.visible) ????? // substitute visible glyph for space, tab, carriage return
    // if (c_l.overstrike) ?????
    // if (c_l.vanished) ?????
    return style;
}

// Tabs are set at the paragraph level, but carry over to following paragraphs.
const int N_NAMED_TABS = 15;

typedef struct {
    int tab_interval; // micas
    int named_tabs[N_NAMED_TABS]; // micas
} tab_settings;

typedef struct {
    int right_margin = 527 * MICAS_PER_INCH / POINTS_PER_INCH; // micas
    int left_margin = 85 * MICAS_PER_INCH / POINTS_PER_INCH; // micas
    int line_1_margin = -1; // micas; default: same as left_margin
    int vertical_tab = -1; // points; default: none
    int line_leading = 1; // points;
    int par_leading = 0; // points
    bool doc_profile = false;
    bool justified = false;
    bool centered = false;
    bool hardcopy = false;
    int keep = 0; // points
} par_looks;

std::string assemble_div_style(const tab_settings& tabs, const par_looks& p_l) {
    std::string style;
    append_style(style, "width: " + std::to_string((p_l.right_margin - p_l.left_margin) * POINTS_PER_INCH / MICAS_PER_INCH) + "pt");
    append_style(style, "margin-left: " + std::to_string(p_l.left_margin * POINTS_PER_INCH / MICAS_PER_INCH) + "pt");
    if (p_l.line_1_margin != -1)
        append_style(style, "text-indent: " + std::to_string((p_l.line_1_margin - p_l.left_margin) * POINTS_PER_INCH / MICAS_PER_INCH) + "pt");
    // ***** p_l.vertical_tab
    // ***** p_l.line_leading needs to insert _extra_ leading
    // append_style(style, "line-height: " + std::to_string(p_l.line_leading) + "pt");
    append_style(style, "margin-top: " + std::to_string(p_l.par_leading) + "pt");
    // ***** p_l.doc_profile
    append_style(style, "text-align: " + std::string(p_l.justified ? "justify" : (p_l.centered ? "center" : "left")));
    // ***** p_l.hardcopy
    // ***** p_l.keep
    // ***** tabs.tab_interval
    // ***** tabs..named_tabs
    return style;
}

int get_param(CharIterator& f, CharIterator l) {
    int param = 0;
    while (f != l && '0' <= *f && *f <= '9') {
        param = param * 10 + (*f - '0');
        ++f;
    }
    // Space separates parameter from following run-length
    if (f != l && *f == ' ') ++f;
    return param;
}

CharIterator parse_par_looks(CharIterator f_z, CharIterator f_bs, tab_settings& tabs, par_looks& p_l) {
    CharIterator g = f_z;
    ++g;
    while (g != f_bs) {
        char c = *g;
        ++g;
        int param = 0;
        switch (c) {
            case 'z':
                p_l.right_margin = get_param(g, f_bs);
                break;
            case 'l':
                p_l.left_margin = get_param(g, f_bs);
                break;
            case 'd':
                p_l.line_1_margin = get_param(g, f_bs);
                break;
            case 'y':
                p_l.vertical_tab = get_param(g, f_bs);
                break;
            case 'x':
                p_l.line_leading = get_param(g, f_bs);
                break;
            case 'e':
                p_l.par_leading = get_param(g, f_bs);
                break;
            case 'q':
                p_l.doc_profile = true;
                break;
            case 'j':
                p_l.justified = true;
                break;
            case 'c':
                p_l.centered = true;
                break;
            case 'h':
                p_l.hardcopy = true;
                break;
            case 'k':
                p_l.keep = get_param(g, f_bs);
                break;
            case '(': {
                int n = get_param(g, f_bs); // tab interval or name
                assert(g != f_bs);
                if (*g == ')')
                    tabs.tab_interval = n;
                else if (*g == ',') {
                    ++g;
                    int val = get_param(g, f_bs);
                    if (val == 65535) val = -1;
                    assert(n < N_NAMED_TABS);
                    tabs.named_tabs[n] = val;
                    assert(g != f_bs && *g == ')');
                }
                ++g;
                break;
            }
            case 'p':
                param = get_param(g, f_bs); // ***** TODO: figure out what this is supposed to do.
                std::cout << "*** Unexpected .bravo par look \"p\" in file " << the_in_path << std::endl;
                break;
            default:
                assert(!(c == c));
                // ***** Print error message ?????
                break;
        }
    }
    return g;
}

CharIterator parse_char_looks(CharIterator g, CharIterator f_m, char_looks& c_l) {
    do {
        char c = *g;
        ++g;
        switch (c) {
            case 'f':
                c_l.font = get_param(g, f_m);
                break;
            case 'o':
                c_l.offset = get_param(g, f_m);
                // 0-127 is positive; 128-255 is negative
                if (c_l.offset >= 128) c_l.offset = c_l.offset - 256;
                break;
            case 't':
                c_l.tab_or_color = get_param(g, f_m);
                break;
            case 'u': case 'U': c_l.underlined = islower(c); break;
            case 'b': case 'B': c_l.bold = islower(c); break;
            case 'i': case 'I': c_l.italic = islower(c); break;
            case 'g': case 'G': c_l.graphic = islower(c); break;
            case 'v': case 'V': c_l.visible = islower(c); break;
            case 's': case 'S': c_l.overstrike = islower(c); break;
            case 'n': case 'N': c_l.vanished = islower(c); break;
            default:
                assert(false);
                break; // ***** Report error ?????
        }
    } while (g != f_m && (*g < '0' || *g > '9'));
    return g;
}

std::string escape_ascii(const std::string& s) {
    std::stringstream ss;
    auto n = s.length();
    for (decltype(n) i = 0; i < n; ++i) {
        unsigned char c = s[i];
        // ***** Map Control A and Control [ for User.cm ?????
        if (c == '^')
            ss << "&uarr;";
        else if (c == '_')
            ss << "&larr;";
        else if (c == '<')
            ss << "&lt;";
        else if (c == '>')
            ss << "&gt;";
        else if (c == '&')
            ss << "&amp;";
        else
            ss << c;
    };
    return ss.str();
}

std::string escape_bravo(const std::string& s) {
    std::stringstream ss;
    auto n = s.length();
    for (decltype(n) i = 0; i < n; ++i) {
        const char* r = c_c[s[i]].replacement;
        if (c_c[s[i]].combining) {
            // ***** Look for Control K followed by Control E, and convert to circumflex
            ++i;
            if (i < n) ss << s[i];
            ss << r;
        } else {
            if (r != nullptr)
                ss << r;
            else
                ss << s[i];
        }
    };
    return ss.str();
}

/* From Indigo/BasicDisks/Smalltalk14.bfs!1_/st80sources.v00:
 
 "1003" LADScanner$'Initialization'
 [classInit | |
 "LADScanner classInit."
 typeTable _ Vector new: 256.
 typeTable ・ (1 to: 256) all_ #binary "default".
 typeTable ・ #(9 10 12 13 32 ) all_ #xDelimiter "tab lf ff cr space".
 typeTable ・ #(1 6 14 18 ) all_ #xAbbreviation " ^A ^F ^N ^R ".
 typeTable ・ 15 _ #xLitQuote.
 typeTable ・ 26 _ #xControlZ.
 typeTable ・ 30 _ #doIt.
 typeTable ・ 34 _ #xDoubleQuote.
 typeTable ・ 39 _ #xSingleQuote.
 typeTable ・ 40 _ #leftParenthesis.
 typeTable ・ 41 _ #rightParenthesis.
 typeTable ・ 44 _ #comma.
 typeTable ・ 46 _ #period.
 typeTable ・ (48 to: 57) all_ #xDigit.
 typeTable ・ #(3 58 ) all_ #colon.
 typeTable ・ 59 _ #semicolon.
 typeTable ・ #(60 61 62 126 ) all_ #xRelational " < = > ~ ".
 typeTable ・ ((65 to: 90) concat: (97 to: 122)) all_ #xLetter.
 typeTable ・ 91 _ #leftBracket.
 typeTable ・ 93 _ #rightBracket.
 typeTable ・ 94 _ #upArrow.
 typeTable ・ 95 _ #leftArrow.
 typeTable ・ 123 _ #leftBrace.
 typeTable ・ 124 _ #verticalBar.
 typeTable ・ 125 _ #rightBrace].
 */
std::string escape_smalltalk(const std::string& s) {
    std::stringstream ss;
    auto n = s.length();
    for (decltype(n) i = 0; i < n; ++i) {
        unsigned char c = s[i];
        switch (c) {
            case 001: ss << "&x2264;"; break;   // Control A: less-than or equal to
            case 003: ss << "&x2982;"; break;   // Control C: remote evaluation (hollow colon)∶
            case 006: ss << "&x2261;"; break;   // Control F: same object (@ or equivalence sign)
            case 007: ss << "&x30FB;"; break;   // Control G: subscript (centered dot)
            case 016: ss << "&x2260;"; break;   // Control N: not equal
            case 017: ss << "&x27A6;"; break;   // Control O: literal quote (curved upwards and righwards arrow)
            case 021: ss << "&x21D1;"; break;   // Control Q: return (upwards double arrow)
            case 022: ss << "&x2265;"; break;   // Control R: greater-than or equal to
            case 023: ss << "&x02BC;&x1D494;"; break;   // Control S: in instance context ('s)
            case 025: ss << "&x00AF;"; break;   // Control U: negative literal (macron or high minus)
            case 027: ss << "&x2299;"; break;   // Control W: point from coords (circled dot operator)
            case 030: ss << "_"; break;         // Control X: ***** underscore ?????
            case 033: ss << "&x21D2;"; break;   // Control [: then (rightwards double arrow)
            case 036: ss << "&x231F;"; break;   // Control ^: end of stream (bottom right corner) or doIt
            case '_': ss << "&x2190;"; break;   // underscore: leftwards arrow
            case '^': ss << "&x21D1;"; break;   // caret: upwards double arrow ***** same as Control Q ?????
        }
    };
    return ss.str();
}

static const int PX_PER_INCH = 96;

int micas_to_px(int micas) {
    return micas * PX_PER_INCH / MICAS_PER_INCH;
}

// Return true if one or more tabs encountered.
bool gen_run_with_tabs(std::ofstream& os, const tab_settings& tabs, const char_looks& cl, const std::string& contents) {
    bool tab_seen = false;
    assert(!contents.empty()); // ***** DEBUG
    std::string::size_type f = 0;
    std::string::size_type f1;
    // For each tab, output <span class=CLASS></span>, where CLASS is "tab" or position in px
    while (true) {
        f1 = contents.find('\t', f);
        if (f1 == std::string::npos) {
            if (f != contents.size()) gen_span(os, assemble_span_style(cl), contents.substr(f, f1));
            break;
        }
        if (f1 > f) gen_span(os, assemble_span_style(cl), contents.substr(f, f1 - f));
        tab_seen = true;
        int n = tabs.tab_interval;
        if (cl.tab_or_color != -1 && cl.tab_or_color != 0) { // ***** Is this the correct treatment of named tab 0 ?????
            assert(cl.tab_or_color <= N_NAMED_TABS);
            n = tabs.named_tabs[cl.tab_or_color - 1];
            if (n == -1) {
                std::cout << "*** bravo_to_html(" << the_in_path << "): undefined named tab: " << cl.tab_or_color << "\n";
                n = tabs.tab_interval;
            }
        }
        os << "<span class=\"tab\" val=\"" << std::to_string(micas_to_px(n)) << "\"></span>";
        ++f1;
        f = f1;
    };
    return tab_seen;
}

void gen_tab_script(std::ofstream& os) {
    os <<
    "<script>"
    "function do_tabs() { // unit = px\n"
    "  var tabs = document.getElementsByClassName(\"tab\");\n"
    "  for (var i = 0; i < tabs.length; ++i) {\n"
    "    var span = tabs[i];\n"
    "    var val = span.getAttribute(\"val\");\n"
    "    if (val == null) console.log(\"Couldn't get val\");\n"
    "    var rect = span.getBoundingClientRect();\n"
    "    var tabstop;\n"
    "    if (val > 0) tabstop = Math.ceil(rect.left / val) * val;\n"
    "    else tabstop = -val;\n"
    "    console.log(\"do_tabs\", val, tabstop)\n" // ***** DEBUG *****
    "    if (tabstop > 0) {\n"
    "      var width = tabstop - rect.left;\n"
    "      span.style.display = \"inline-block\";\n"
    "      span.style.width = width + \"px\";\n"
    "    }\n"
    "  }\n"
    "}\n"
    "do_tabs();\n"
    "</script>\n";
}

void render_paragraph(CharIterator begin, CharIterator f, CharIterator f_z, CharIterator f_bs, CharIterator f_m,
                      tab_settings& tabs, std::ofstream& os) {
    // For each paragraph look entry in [f_z + 1, f_bs), apply it to the entire range [f, f_z)
    // ***** TODO: look profile affects the entire document
    bool tab_seen = false;
    par_looks p_l;
    std::size_t i_f = std::distance(begin, f);
    std::size_t i_l = std::distance(begin, f_m);
    CharIterator g = parse_par_looks(f_z, f_bs, tabs, p_l);
    std::string style = assemble_div_style(tabs, p_l);
    auto recover = [&]() {
        std::cout << "*** Inconsistent run length in .bravo file " << the_in_path << " in range [" << i_f << ", " << i_l << ")" << std::endl;
        std::size_t n = std::distance(f, f_z); // rest of paragraph text
        char_looks c_l; // default looks
        tab_seen |= gen_run_with_tabs(os, tabs, c_l, escape_bravo(std::string(&(*f), n)));
    };
    auto gen_contents = [&](std::ofstream& os, const std::string& indent) {
        char_looks c_l;
        std::size_t n; // run length
        if (f_bs != f_m) {
            // For each character look entry in [f_bs + 1, f_m), apply it to the corresponding subrange of [f, f_z)
            assert(g == f_bs);
            ++g;
            if ('0' <= *g && *g <= '9') {
                // Run-length without property change descriptor
                n = get_param(g, f_m);
                // Output the next n characters with default looks
                if (f + n > f_z) {
                    recover();
                    return;
                }
                tab_seen |= gen_run_with_tabs(os, tabs, c_l, escape_bravo(std::string(&(*f), n)));
                f += n;
            }
            while (g != f_m) {
                // Parse property change descriptor
                g = parse_char_looks(g, f_m, c_l);
                // Parse run length
                if (g != f_m) {
                    n = get_param(g, f_m);
                    if (f + n > f_z) {
                        recover();
                        return;
                    }
                } else {
                    // last run length is implicit
                    n = std::distance(f, f_z);
                }
                if (n > 0) {
                    // Output the next n characters with the specified looks
                    tab_seen |= gen_run_with_tabs(os, tabs, c_l, escape_bravo(std::string(&(*f), n)));
                    f += n;
                    if (f > f_z) {
                        os << std::endl; // flushes buffer
                        std::cout << std::endl; // flushes buffer
                        assert(false);
                    }
                }
            }
        } else {
            // Character looks defaulted
            n = std::distance(f, f_z);
            if (n > 0) {
                tab_seen |= gen_run_with_tabs(os, tabs, c_l, escape_bravo(std::string(&(*f), n)));
                f += n;
            }
        }
    };
    gen_div(os, style, gen_contents);
}

// Unless otherwise specified, we follow: [_cd8_]<altodocs>bravo-file-format.press!2

void bravo_to_html(const std::string& alto_file_path, const std::string& in_path, const std::string& out_path) {
    the_in_path = in_path;
    std::ifstream is(in_path, std::ifstream::in | std::ifstream::binary);
    if (!is) throw raf_exception("bravo_to_html could not open " + in_path);
    std::ofstream os(out_path, std::ofstream::out);
    if (!os) throw raf_exception("bravo_to_html could not open " + out_path);
    
    auto gen_contents = [&](std::ofstream& os, const std::string& indent) {
        
        std::fstream::streamoff length = size(is);
        if (length > MAX_BRAVO_FILE_SIZE) {
            std::cout << "*** File is too large to be in Bravo format: " << in_path << std::endl;
            std::exit(1);
        }
        
        std::vector<char> buffer(length);
        is.read(&buffer[0], length);
        
        CharIterator f_0 = buffer.cbegin();
        CharIterator f = f_0;
        CharIterator l = buffer.cend();
        tab_settings tabs;
        tabs.tab_interval = 60 * MICAS_PER_INCH / POINTS_PER_INCH;
        for (int i = 0; i < N_NAMED_TABS; ++i) tabs.named_tabs[i] = -1;
        // ***** Initialize tabs from user.cm

        // For each paragraph, read characters and looks
        while (f != l) {
            CharIterator f_z = std::find(f, l, '\32');      // Control-Z begins paragraph trailer
            CharIterator f_m = std::find(f_z, l, '\15');    // Control-M ends paragraph trailer
            CharIterator f_bs = std::find(f_z, f_m, '\\');  // backslash separates paragraph looks from character looks
            // f_z == l => "vanilla" document
            // f_m == l => missing final carriage return -- is this an error?
            // f_bs == f_m => all character looks defaulted
            if (f_z != l) {
                if (f_m == l) {
                    std::cout << "*** Missing CR in Bravo format file: " << in_path << std::endl;
                    std::exit(1);
                }
                render_paragraph(f_0, f, f_z, f_bs, f_m, tabs, os);
                f = f_m;
                ++f;
            } else {
                // Output [f, l) with default looks
                auto n = std::distance(f, l);
                char_looks c_l;
                assert(n != 0); // *****
                gen_span(os, assemble_span_style(c_l), escape_bravo(std::string(&(*f), n))); // ***** What if tabs ?????
                f = l;
            }
        }
        
        // Generate the script that processes tabs.
        gen_tab_script(os);
    };
    
    gen_page(out_path, alto_file_path, gen_contents);

}
