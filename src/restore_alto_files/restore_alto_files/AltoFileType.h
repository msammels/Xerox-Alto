//
//  AltoFileType.h
//  restore_alto_files
//
//  Created by Paul McJones on 1/16/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_AltoFileType_h
#define restore_alto_files_AltoFileType_h

#include <string>

// Discriminate based on view for altoweb.h
// ***** TODO: various font formats, BCPL executables, Mesa .bcd, .symbol, .image, ...
enum file_type {
    ascii = 1, // includes .cm, .asm, .bcpl, .d, .mesa, .config
    smalltalk = 2,
    bravo = 3,
    text = bravo, // highest value that is text
    press = 4,
    binary = 5
};

std::string file_type_to_string(file_type f_t);

#endif
