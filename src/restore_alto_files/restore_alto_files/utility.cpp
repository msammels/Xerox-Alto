//
//  utility.cpp
//  restore_alto_files
//
//  Created by Paul McJones on 11/18/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include "utility.h"
#include <dirent.h>
#include <sys/stat.h>
#include <utime.h>

// Strings

std::string subst(std::string s, char old_char, char new_char) {
    std::replace(s.begin(), s.end(), old_char, new_char);
    return s;
}

bool ci_equal(const std::string& a, const std::string& b) {
    std::size_t n = a.size();
    if (n != b.size()) return false;
    std::size_t i = 0;
    while (i < n) {
        if (tolower(a[i]) != tolower(b[i])) return false;
        ++i;
    }
    return true;
}

// Directories

void read_dirmap(std::string path, DirMap& dirmap) {
    DIR *dp;
    struct dirent *dirp;
    struct stat filestat;
    dp = opendir(path.c_str());
    if (dp == nullptr) {
        std::cout << "*** Error(" << errno << ") opening " << path << std::endl;
        return;
    }
    while ((dirp = readdir(dp))) {
        std::string filepath = join_paths(path, dirp->d_name);
        
        // If the file is in some way invalid or the name begins with a "." we skip it.
        if (dirp->d_name[0] == '.') continue;
        if (stat( filepath.c_str(), &filestat )) continue;
        dirmap.insert(std::make_pair(std::string(dirp->d_name), filestat.st_mode));
    }
    closedir(dp);
}

void print_dirmap(const std::string& path, const DirMap& dirmap) {
    std::cout << path + ": " << std::endl;
    for (auto ir = dirmap.cbegin(); ir != dirmap.cend(); ++ir)
        std::cout << " [" << (*ir).first << ':' << std::hex << (*ir).second << ']' << std::endl;
}

// Convert, for example, ifs/sources to ifs>sources>, or altotape/ddtape-rev-a.dm!1_ to altotape>ddtape-rev-a.dm!1>
std::string alto_style_dir_name(const std::string& dir_name) {
    auto n = dir_name.size();
    if (dir_name[n - 1] == '_') n = n - 1;
    return subst(dir_name.substr(0, n), '/', '>') + ">";
}

// Files

std::string join_paths(const std::string& p1, const std::string& p2) {
    if (!p1.empty()) {
        if (!p2.empty()) return p1 + "/" + p2;
        else return p1;
    } else return p2;
}


off_t get_file_size(const std::string& path) {
    struct stat st;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        std::exit(1);
    }
    return st.st_size; /* seconds since the epoch */
}

time_t get_file_write_time(const std::string& path) {
    struct stat st;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        std::exit(1);
    }
    return st.st_mtime; /* seconds since the epoch */
}

void set_file_write_time(const std::string& path, time_t write_time) {
    struct stat st;
    struct utimbuf new_times;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        return;
    }
    new_times.modtime = write_time;
    new_times.actime = st.st_atime;
    if (utime(path.c_str(), &new_times) < 0) {
        std::cout << "**** Error setting write time of file: " << path << std::endl;
        return;
    }

}

std::string date_to_string(std::time_t t) {
    std::tm* ptm = std::localtime(&t);
    char buffer[32];
    // Format: Mo, 15.06.2009 20:20:00
    std::strftime(buffer, 32, "%d-%b-%Y %H:%M:%S %Z", ptm);
    return std::string(buffer);
}

// File streams

std::fstream::streamoff size(std::ifstream& is) {
    std::fstream::streamoff cur = is.tellg();   // save
    is.seekg(0, is.end);
    std::fstream::streamoff end = is.tellg();
    is.seekg(cur, is.beg);                      // restore
    return end;
}