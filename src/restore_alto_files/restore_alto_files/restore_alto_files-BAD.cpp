//
//  raf
//  restore_alto_files
//
//  Created by Paul McJones starting on 10/11/2013.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <ctime>
#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <utime.h>
#include <vector>
#include <zlib.h>
#include "Alto_DM_file.h"
#include "archivist.h"
#include "AltoDefs.h"
#include "AltoFileSys.h"
#include "Bfs.h"
#include "CopyDiskProtocol.h"

// Command-line parameters and options (see main)
std::string archive_path; // path to year/tape/record hierarchy
std::string restore_path; // path to server/dirpath/filename!version hierarchy
std::string extras_path; // path to cd{6,8) if present
std::set<std::string>   exclude; // years/tapes/records to exclude from consideration
bool verbose = false;
bool tilde = false; // represent version as .~2~ suffix rather than !2 suffix
bool unpack_dm_files = false;
bool unpack_disk_images = false; // e.g., .altodisk, .bfs, and .copydisk
bool xref = false;

std::string subst(std::string s, char old_char, char new_char) {
    std::replace(s.begin(), s.end(), old_char, new_char);
    return s;
}

std::string join_paths(const std::string& p1, const std::string& p2) {
    if (!p1.empty()) {
        if (!p2.empty()) return p1 + "/" + p2;
        else return p1;
    } else return p2;
}

typedef std::map<std::string, mode_t> DirMap;

// Add entries of directory at path to dirmap
void read_dirmap(std::string path, DirMap& dirmap) {
    DIR *dp;
    struct dirent *dirp;
    struct stat filestat;
    dp = opendir(path.c_str());
    if (dp == NULL) {
        std::cout << "*** Error(" << errno << ") opening " << path << std::endl;
        return;
    }
    while ((dirp = readdir(dp))) {
        std::string filepath = join_paths(path, dirp->d_name);
        
        // If the file is in some way invalid or the name begins with a "." we skip it.
        if (dirp->d_name[0] == '.') continue;
        if (stat( filepath.c_str(), &filestat )) continue;
        dirmap.insert(std::make_pair(std::string(dirp->d_name), filestat.st_mode));
    }
    closedir(dp);
}

void print_dirmap(const std::string& path, const DirMap& dirmap) {
    std::cout << path + ": " << std::endl;
    for (auto ir = dirmap.cbegin(); ir != dirmap.cend(); ++ir)
        std::cout << " [" << (*ir).first << ':' << std::hex << (*ir).second << ']' << std::endl;
}

// Read a big-endian 16-bit unsigned from a stream
UWord read_UWord(std::istream& is) {
    UByte b0 = is.get();
    UByte b1 = is.get();
    return (b0 << 8) | b1;
}

// Read a sequence of big-endian 16-bit unsigneds from a stream
void read_UWords(std::istream& is, UWord* pWord, std::size_t nWords) {
    while (nWords != 0) {
        *pWord = read_UWord(is);
        ++pWord;
        --nWords;
    }
}

typedef int Year;           // e.g., 84
typedef int TapeNumber;     // e.g., 10000
typedef int RecordNumber;   // e.g., 19
//typedef enum {
//    INVALID,
//    DIRECTORY,
//    DATA} RecordType;

typedef std::tuple<Year, TapeNumber, RecordNumber> RecordIdentifier;

// Convert, e.g., (84, 10000, 23) to "84/T10000P/file0023".
std::string path_from_record_id(RecordIdentifier ri) {
    std::ostringstream oss;
    oss.width(2);           // 84
    oss << std::get<0>(ri); // year
    oss.width(0);
    oss << "/T";
    oss.width(5);           // 10000
    oss.fill('0');
    oss << std::get<1>(ri); // tape
    oss.width(0);
    oss << "P/file";
    oss.width(4);           // 0023
    oss << std::get<2>(ri); // record number
    return oss.str();
}

typedef struct {
    RecordIdentifier
                ri;         // e.g., <84, 10000, 20>
    std::string server;     // e.g., Ibis
    std::string dir_path;   // e.g., AltoGateway>Drivers
    std::string file_name;  // e.g., AltoSlaDriver.mesa
    int         version;    // e.g., 1
    time_t      creation_date;  // e.g., 31-Jan-81 19:59:27 PST
    time_t      write_date; //       (same format)
    time_t      read_date;  //       (same format)
    bool        text;       // otherwise it's binary
    std::fstream::streamoff     size;   // in bytes
    std::string author;     // e.g., Murray
} DirectoryRecord;

typedef struct {
    RecordIdentifier        ri;    // e.g., <84, 10000, 20>
    std::fstream::streamoff size;
    uint64_t                crc32;
} DataRecord;

void validate_assumptions() {
//    std::cout << "*** sizeof(unsigned long): " << sizeof(unsigned long) << std::endl;
    assert(sizeof(PrefixRep)      == 8);
    assert(sizeof(DataRep)        == 10);
    assert(sizeof(DA)             == sizeof(UWord));
    assert(sizeof(DiskPageHeader) == 2 * sizeof(UWord));
    assert(sizeof(FID)            == 3 * sizeof(UWord));
    assert(sizeof(FileIdentifier) == 3 * sizeof(UWord));
    assert(sizeof(DL)             == 8 * sizeof(UWord));
    assert(sizeof(DiskPageLabel)  == 8 * sizeof(UWord));
    assert(sizeof(DiskPageData)   == 256 * sizeof(UWord));
    assert(sizeof(LD)             == 256 * sizeof(UWord));
}

std::size_t n_records = 0;
std::size_t n_dir_records = 0;
std::size_t n_data_records = 0;

typedef std::string FilePath; // e.g., [Indigo]<DMS>Laurel>6>laurelxref.mcross!1
std::multimap<FilePath, RecordIdentifier> file_path_index; // map path on server to record identifier for data

std::string append_version(const std::string& file_name, int version) {
    std::string ver(std::to_string(version));
    if (tilde) return file_name + ".~" + ver + "~";
    return file_name + "!" + ver;
}

FilePath file_path_from_directory_record(const DirectoryRecord& dir_rec) {
    return "[" + dir_rec.server + "]<" + dir_rec.dir_path + ">" + append_version(dir_rec.file_name, dir_rec.version);
}

typedef struct {
    DirectoryRecord     dir_rec;
    DataRecord          data_rec;
} FileRecord;

// Case-independent string equality
bool ci_equal(const std::string& a, const std::string& b) {
    std::size_t n = a.size();
    if (n != b.size()) return false;
    std::size_t i = 0;
    while (i < n) {
        if (tolower(a[i]) != tolower(b[i])) return false;
        ++i;
    }
    return true;
}

// Case-independent string ordering
struct ci_less {
    ci_less() {}
    bool operator()(const std::string& a, const std::string& b) const {
        std::size_t n = std::min(a.size(), b.size());
        std::size_t i = 0;
        while (i < n) {
            if (tolower(a[i]) < tolower(b[i])) return true;
            if (tolower(b[i]) < tolower(a[i])) return false;
            ++i;
        }
        return i < b.size();
    }
};

// Ordering on file records, based on Alto/IFS filename semantics
bool less(const FileRecord& a, const FileRecord& b) {
    if (ci_less()(a.dir_rec.file_name, b.dir_rec.file_name)) return true;
    if (ci_less()(b.dir_rec.file_name, a.dir_rec.file_name)) return false;
    if (ci_less()(a.dir_rec.server, b.dir_rec.server)) return true;
    if (ci_less()(b.dir_rec.server, a.dir_rec.server)) return false;
    if (ci_less()(a.dir_rec.dir_path, b.dir_rec.dir_path)) return true;
    if (ci_less()(b.dir_rec.dir_path, a.dir_rec.dir_path)) return false;
    if (a.dir_rec.version < b.dir_rec.version) return true;
    if (b.dir_rec.version < a.dir_rec.version) return false;
    return false;
}

// Count the number of non-equivalent keys in a multimap
template <typename K, typename V>
size_t key_count(const std::multimap<K, V> &m) {
    if (m.empty()) {
        return 0;
    }
    size_t n = 1;
    auto f = m.cbegin();
    auto l = m.cend();
    auto key_less = m.key_comp();
    K lastkey = f->first;
    ++f;
    while (f != l) {
        if (key_less(lastkey, f->first)) {
            ++n;
            lastkey = f->first;
        }
        ++f;
    }
    return n;
}

typedef uint64_t Crc32;

std::vector<FileRecord> file_table;
std::multimap<Crc32, std::vector<FileRecord>::size_type> file_table_crc32_index;

typedef std::string Extension;
std::map<Extension, int, ci_less> extension_table; // histogram of extensions
typedef std::map<Extension, int, ci_less>::iterator ExtensionTableIterator;

typedef std::string Author;
std::map<Author, int, ci_less> author_table; // histogram of authors
typedef std::map<Author, int, ci_less>::iterator AuthorTableIterator;

void add_to_file_table(const FileRecord file_rec) {
    auto i = file_table.size();
    file_table.push_back(file_rec);
    file_table_crc32_index.insert(std::make_pair(file_rec.data_rec.crc32, i));
    
    // Increment histogram bucket for extension
    auto f = file_rec.dir_rec.file_name.rfind('.');
    if (f != std::string::npos) {
        std::pair<ExtensionTableIterator, bool> p = extension_table.insert(std::make_pair(file_rec.dir_rec.file_name.substr(f, std::string::npos), 1));
        if (!p.second) p.first->second += 1;
    }

    // Increment histogram bucket for author
    std::pair<ExtensionTableIterator, bool> p = author_table.insert(std::make_pair(file_rec.dir_rec.author, 1));
    if (!p.second) p.first->second += 1;
}

// Alto TIME (seconds since 1 January 1901) to time_t
time_t alto_TIME_to_time_t(unsigned char bytes[4]) {
    // Leap years between 1901 and 1970: 04 08 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68
    const unsigned int UnixCorrection = ((1970 - 1901)*365+17)*24*60*60; // seconds from Alto epoch to Unix epoch
    //const unsigned int UnixCorrection = 2177452800; // seconds from 1901/01/01 to 1970/01/01
    unsigned int s = (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + bytes[3];
    return s - UnixCorrection;
}

// Convert a string such as "31-Jan-81 19:59:27 PST" to a time_t.
time_t string_to_date(const std::string& date) {
    const long PST_offset = -8*60*60;
    const long PDT_offset = -7*60*60;
    struct tm tm;
    char* time_zone = strptime(date.c_str(), "%d-%b-%y %H:%M:%S ", &tm);
    // Handle time_zone; assume Pacific (since PARC was in Palo Alto).
    if (time_zone != NULL) {
        if (time_zone[0] == 'P' && time_zone[2] == 'T') {
            if (time_zone[0] == 'S')
                tm.tm_gmtoff = PST_offset;
            else if (time_zone[0] == 'D')
                tm.tm_gmtoff = PDT_offset;
            else tm.tm_gmtoff = PST_offset; // default
        } else tm.tm_gmtoff = PST_offset; // default
    }
    return mktime(&tm);
}

// Convert a time_t to an Alto-format date string, e.g.,  "31-Jan-81 19:59:27 PST"
std::string date_to_string(std::time_t t) {
    std::tm* ptm = std::localtime(&t);
    char buffer[32];
    // Format: Mo, 15.06.2009 20:20:00
    std::strftime(buffer, 32, "%d-%b-%Y %H:%M:%S %Z", ptm);
    return std::string(buffer);
}

bool get_server(const char* f, const char* l, std::string& value) {
    if (*f != '[') return false;
    ++f;
    const char* limit = std::find(f, l, ']');
    if (f == l) return false;
    value = std::string(f, limit - f);
    return true;
}

/* Example: If the range [f, l) includes the characters, say, "[Indigo]<DMS>Laurel>6>laurelxref.mcross!1((Directory DMS>Laurel>6)(Name-Body laurelxref.mcross)(Version 1)(Creation-Date 19-May-81  9:40:21 PDT)(Write-Date 12-Oct-81 17:57:21 PDT)(Byte-Size 8)(Type Binary)(Size 337858)(Author RWeaver)(Device Primary)(Server-Filename <DMS>Laurel>6>laurelxref.mcross!1))", and prop is "Author", then value will be set to "RWeaver" */
bool get_prop(const char* f, const char* l, const std::string prop, std::string& value) {
    auto n = prop.size();
    // Advance past '(' that begins the property list.
    if (f != l) {
        f = std::find(f, l, '(');
        if (f == l) return false;
        ++f;
    }
    // Check each property in turn.
    while (f != l) {
        if (*f != '(') return false; // end of list with name not found
        ++f;
        if (f + n <= l && prop.compare(0, n, f, n) == 0) {
            f = f + n;
            if (*f != ' ') return false;
            ++f;
            const char* limit = std::find(f, l, ')');
            if (limit == l) return false;
            value = std::string(f, limit - f);
            return true;
        } else {
            f = std::find(f, l, ')');
            if (f == l) return false;
            ++f;
        }
    }
    return false;
}

// Return the length of a file stream, preserving its current position
std::fstream::streamoff size(std::ifstream& is) {
    std::fstream::streamoff cur = is.tellg();   // save
    is.seekg(0, is.end);
    std::fstream::streamoff end = is.tellg();
    is.seekg(cur, is.beg);                      // restore
    return end;
}

void read_directory(const std::string& full_record_path,
                    const RecordIdentifier& ri,
                    std::ifstream& is, const std::fstream::streamoff& length,
                    FileRecord& file_rec) {
    if (length < sizeof(DirectoryRep)) {
        std::cout << "*** Directory record too short: " << full_record_path << std::endl;
        return;
    }
    is.seekg(0, is.beg);
    std::vector<char> buffer(length);
    is.read(&(buffer[0]), length);
    const Directory dp = (Directory)&(buffer[0]);
    const char* f = &dp->varData[0];
    const char* l = &(*buffer.end());
    
    // Fill file_rec.dir_rec from dp
    file_rec.dir_rec.ri = ri;
    std::string temp;
    if (!get_server(f, l, file_rec.dir_rec.server)) {
        std::cout << "*** Missing server name: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Directory", file_rec.dir_rec.dir_path)) {
        std::cout << "*** Missing directory path: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Name-Body", file_rec.dir_rec.file_name)) {
        std::cout << "*** Missing file name: " << path_from_record_id(ri) << std::endl;
        return;
    }
    
    if (!get_prop(f, l, "Version", temp)) {
        std::cout << "*** Missing version: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        file_rec.dir_rec.version = std::stoi(temp);
    
    if (!get_prop(f, l, "Creation-Date", temp)) {
        std::cout << "*** Missing creation date: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        file_rec.dir_rec.creation_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Write-Date", temp)) {
        std::cout << "*** Missing write date: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        file_rec.dir_rec.write_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Read-Date", temp)) {
        std::cout << "*** Missing read date: " << path_from_record_id(ri) << std::endl;
        file_rec.dir_rec.read_date = file_rec.dir_rec.write_date; /* Not considered fatal */
    } else
        file_rec.dir_rec.read_date = string_to_date(temp);
    
    if (!get_prop(f, l, "Type", temp)) {
        std::cout << "*** Missing type: " << path_from_record_id(ri) << std::endl;
        file_rec.dir_rec.text = false; /* Not considered fatal */
    } else {
        file_rec.dir_rec.text = temp.compare("Text") == 0; // ***** else "Binary" - any other values ?????
        if (!file_rec.dir_rec.text && !temp.compare("Binary") == 0)
            std::cout << "*** Unexpected type (" << temp << "): " << path_from_record_id(ri) << std::endl;
    }
    
    if (!get_prop(f, l, "Size", temp)) {
        std::cout << "*** Missing size: " << path_from_record_id(ri) << std::endl;
        return;
    } else
        file_rec.dir_rec.size = std::stoi(temp);
    
    if (!get_prop(f, l, "Author", file_rec.dir_rec.author)) {
        std::cout << "*** Missing author: " << full_record_path << std::endl;
        return;
    }
    
    ++n_dir_records;
    
    return;
}

// Checksum given file, optionally skipping data headers.
uint64_t checksum_file_stream(std::ifstream& is, bool skip_data_headers = true) {
    std::fstream::streamoff n = size(is); // length of file
    std::size_t i = 0;
    uint64_t crc = crc32(0L, Z_NULL, 0);
    while (i < n) {
        if (skip_data_headers) {
            if (i + sizeof(DataRep) >= n) {
                std::cout << "*** File to checksum terminated early: " << n - i << " bytes remaining" << std::endl;
                std::exit(1);
            }
            i += sizeof(DataRep);
            is.seekg(sizeof(DataRep), is.cur);
        }
        unsigned char buffer[4096];
        unsigned m = unsigned(n - i);
        if (m > 4096) m = 4096;
        is.read((char*)buffer, m);
        i += m;
        crc = crc32(crc, buffer, m);
    }
    return crc;
}

// Checksum arbitrary file (no headers to skip).
uint64_t checksum_file(const std::string& path_from) {
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (is) {
        return checksum_file_stream(is, false);
    } else {
        std::cout << "*** Couldn't open: " << path_from << " for reading" << std::endl;
        std::exit(1);
    }
}

void read_data(const std::string& full_record_path, const RecordIdentifier& ri,
               std::ifstream& is, const std::fstream::streamoff& length,
               FileRecord& file_rec) {
    if (length < sizeof(DataRep)) {
        std::cout << "*** Data record too short: " << full_record_path << std::endl;
        return;
    }

    // Create data_table record
    file_rec.data_rec.ri = ri;
    file_rec.data_rec.size = length - ((length + 4105) / 4106) * sizeof(DataRep); // 4106 = 4096 + sizeof(DataRep)
    is.seekg(0, is.beg);
    file_rec.data_rec.crc32 = checksum_file_stream(is);
    if (file_rec.data_rec.size != file_rec.dir_rec.size) {
        std::cout << "*** File size mismatch between data and directory: " << full_record_path << std::endl;
        std::exit(1);
    }
    
    // Create file_path_index record
    file_path_index.insert(std::make_pair(file_path_from_directory_record(file_rec.dir_rec), ri));
    
    // Create file_table record
    add_to_file_table(file_rec);
    
    ++n_data_records;
}

void read_record(std::string full_tape_path, std::string year, std::string tape, std::string record,
                 RecordType& prev_type, FileRecord& file_rec) {
    std::string full_record_path = join_paths(full_tape_path, record);
    std::ifstream is(full_record_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** restore_file couldn't open: " << full_record_path << " for reading" << std::endl;
        std::exit(1);
    }
    std::fstream::streamoff n = size(is); // get length of file
    if (n < sizeof(PrefixRep)) {
        std::cout << "*** Record too short to determine type: " + full_record_path + ": "
                  << long(n) << " bytes; skipping" << std::endl;
        return;
    }
    
    PrefixRep prefix;
    
    // Read prefix as a block:
    is.read((char *)&prefix, sizeof(PrefixRep));
    if (!is) {
        std::cout << "*** Only " << is.gcount() << " bytes of record prefix could be read" << std::endl;
        std::exit(1);
    }
    // Example: "84" => 84, "T10000P" => 10000, "file0019" => 19
    RecordIdentifier ri(std::stoi(year), std::stoi(tape.substr(1, 5)), std::stoi(record.substr(4, 4)));
    
    if (prefix.recordType == directory) {
        if (prev_type == directory) {
            std::cout << "*** Unexpected directory record following directory record; second was: " << path_from_record_id(ri) << std::endl;
        }
        read_directory(full_record_path, ri, is, n, file_rec);
    } else if (prefix.recordType == data) {
        if (prev_type != directory) {
            std::cout << "*** Unexpected data record not following directory record: "
                      << path_from_record_id(ri) << std::endl;
        } else {
            read_data(full_record_path, ri, is, n, file_rec);
        }
    } else if (prefix.recordType == status) {
        std::cout << "*** Status record, ignored: " << path_from_record_id(ri) << std::endl;
    } else {
        std::cout <<" *** Unknown record type, ignored: " << path_from_record_id(ri) << std::endl;
        prefix.recordType = invalid;
    }
    
    prev_type = prefix.recordType;
    ++n_records;
}

decltype(file_table.cbegin()) file_rec_from_ri(RecordIdentifier ri) {
    auto f = file_table.cbegin();
    auto l = file_table.cend();
    while (f != l && f->data_rec.ri != ri) ++f;
    assert(f != l);
    return f;
}

// Check for redundant archive records, i.e., tape 84/10000.
void check_for_redundant_records() {
    std::cout << std::endl << "Check for redundant archive records" << std::endl;
    auto f = file_path_index.begin();
    auto l = file_path_index.end();
    while (f != l) {
        RecordIdentifier ri = f->second;
        // Look for duplicates
        auto f1 = f;
        ++f1;
        while (f1 != l && f->first == f1->first) ++f1;
        if (std::distance(f, f1) > 1) {
            std::cout << "*** Duplicates of " << f->first << ":" << std::endl;
            for (auto f2 = f; f2 != f1; ++f2) {
                auto p_file_rec = file_rec_from_ri(f2->second);
                std::cout << "    Checksum:  " << std::hex << p_file_rec->data_rec.crc32 << std::dec
                          << " ri: " << path_from_record_id(f2->second) << std::endl;
            }
        }
        ++f;
    }
}

// Read images of the archives tapes, loading record_table, directory_table, and data_table.
void read_tapes(const std::string& path) {
    std::cout << std::endl << "Begin tape reading from path: " << path << std::endl;
    DirMap years;
    DirMap tapes;
    DirMap records;
    read_dirmap(path, years);
    // if (verbose) print_dirmap(path, years);
    for (auto iy = years.cbegin(); iy != years.cend(); ++iy) {
        std::string year = (*iy).first;
        if (exclude.find(year) != exclude.end()) {
            std::cout << "  Excluding year " << year << std::endl;
            continue;
        }
        if (S_ISDIR((*iy).second) && year.size() == 2 && isdigit(year[0]) && isdigit(year[1])) {
            std::string full_year_path = join_paths(path, year);
            tapes.clear();
            read_dirmap(full_year_path, tapes);
            // if (verbose) print_dirmap(full_year_path, tapes);
            for (auto it = tapes.cbegin(); it != tapes.cend(); ++it) {
                std::string tape = (*it).first;
                if (exclude.find(tape) != exclude.end()) {
                    std::cout << "  Excluding tape " << tape << std::endl;
                    continue;
                }
                if (S_ISDIR((*it).second)
                    && tape.size() == 7
                    && tape[0]=='T'
                    && isdigit(tape[1]) && isdigit(tape[2]) && isdigit(tape[3]) && isdigit(tape[4]) && isdigit(tape[5])
                    && tape[6]=='P'){
                    std::string full_tape_path = join_paths(full_year_path, tape);
                    records.clear();
                    read_dirmap(full_tape_path, records);
                    // if (verbose) print_dirmap(full_tape_path, records);
                    RecordType prev_type = invalid;
                    FileRecord file_rec;
                    for (auto ir = records.cbegin(); ir != records.cend(); ++ir) {
                        std::string record = (*ir).first;
                        if (exclude.find(record) != exclude.end()) {
                            std::cout << "  Excluding record " << record << std::endl;
                            continue;
                        }
                        if (S_ISREG((*ir).second)
                            && record.size() == 8 & record.compare(0, 4, "file") == 0
                            && isdigit(record[4]) && isdigit(record[5]) && isdigit(record[6]) && isdigit(record[7])) {
                            read_record(full_tape_path, year, tape, record, prev_type, file_rec);
                        }
                    }
                }
            }
        }
    }
    check_for_redundant_records();
    std::cout << std::endl << "End tape reading; statistics: " << std::endl
              << n_records << " records, "
              << n_dir_records << " directory entries, "
              << n_data_records << " data, "
              << file_table.size() << " file table entries"
              << std::endl;
}

off_t get_file_size(const std::string& path) {
    struct stat st;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        std::exit(1);
    }
    return st.st_size; /* seconds since the epoch */
}

time_t get_file_write_time(const std::string& path) {
    struct stat st;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        std::exit(1);
    }
    return st.st_mtime; /* seconds since the epoch */
}

void set_file_write_time(const std::string& path, time_t write_time) {
    struct stat st;
    struct utimbuf new_times;
    if (stat(path.c_str(), &st) < 0) {
        std::cout << "**** Error getting status of file: " << path << std::endl;
        return;
    }
    new_times.modtime = write_time;
    new_times.actime = st.st_atime;
    if (utime(path.c_str(), &new_times) < 0) {
        std::cout << "**** Error setting write time of file: " << path << std::endl;
        return;
    }
    
}

// Restore file from data record file to designated path, skipping data headers
void restore_file(const std::string& path_from, const std::string& path_to) {
    if (verbose)
        std::cout << "Restore " << path_from << " to " << path_to << std::endl;
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** restore_file couldn't open: " << path_from << " for reading" << std::endl;
        std::exit(1);
    }
    std::ofstream os(path_to);
    if (!os) {
        std::cout << "*** restore_file couldn't open: " << path_to << " for writing" << std::endl;
        std::exit(1);
    }
    auto n = size(is); // length of file
    std::fstream::streamoff i = 0;
    while(i + sizeof(DataRep) < n) {
        i += sizeof(DataRep);
        is.seekg(i, is.beg);
        char buffer[4096];
        std::fstream::streamoff m = n - i;
        if (4096 < m) m = 4096;
        is.read(buffer, m);
        i += m;
        os.write(buffer, m);
    }
    if (i != n) {
        std::cout << "*** restore_file terminated early: " << n - i << " bytes remaining" << std::endl;
        std::exit(1);
    }
}

/* Read the Nova/Alto DM (dump/load) file at path dm_path, extract the constituent files,
   and write them in the directory with path out_dir_path. Use dm_file_rec.dir_rec.creation_date for any file lacking a DATE_BLOCK.
   References:
   1. Section 5 of the Executive User's Guide in the Alto Subsystems Manual.
   2. DumpLoad.bcpl in [Indigo]<AltoSource>EXECUTIVE.DM!12
 */
void load_DM_file(const std::string& dm_path,
                  const std::string& out_dir_path,
                  const FileRecord&  dm_file_rec,
                  bool list_only) {
    std::ifstream is(dm_path, std::ifstream::in | std::ifstream::binary);
    std::string   name;
    std::string   output_path;
    time_t        output_date = dm_file_rec.dir_rec.creation_date;
    std::ofstream os;
    bool          output_open = false;
    auto          cleanup = [&] () {
                      if (output_open) {
                          std::fstream::streamoff size = os.tellp();
                          os.close();
                          output_open = false;
                          if (output_date != 0) set_file_write_time(output_path, output_date);
                          
                          // Create file_table record
                          FileRecord file_rec = dm_file_rec;
                          file_rec.dir_rec.dir_path += ">"
                                + append_version(file_rec.dir_rec.file_name, file_rec.dir_rec.version)
                                + "_";
                          file_rec.dir_rec.file_name = name;
                          file_rec.dir_rec.version = 0;
                          file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date = output_date;
                          file_rec.dir_rec.size = size;
                          file_rec.data_rec.crc32 = checksum_file(output_path);
                          file_rec.data_rec.size = size;
                          add_to_file_table(file_rec);
                      }
                  };
    if (!is) return;
    if (verbose) std::cout << "  Loading DM file: " << dm_path << std::endl;
    while (true) {
        unsigned char block_type = is.get();
        if (!is.good()) break;
        switch (block_type) {
            case NAME_BLOCK: {
                if (unpack_dm_files) cleanup();
                // Start of a file
                int attr;
                attr = is.get() << 8;
                attr += is.get(); // always 0 from Alto's Dump
                if (attr != 0) {
                    std::cout << "*** Nonzero attributes in NAME_BLOCK of:" << dm_path << std::endl;
                    // std::exit(1); // ***** Should this be fatal ?????
                }
                
                name = "";
                while (true) {
                    unsigned char c = is.get();
                    if (c == 0) break;
                    name += c;
                };
                if (list_only || verbose) std::cout << "    NAME_BLOCK: " << name << std::endl;
                if (unpack_dm_files) {
                    output_path = join_paths(out_dir_path, name);
                    os.open(output_path, std::ofstream::out | std::ifstream::binary);
                    output_open = true;
                    output_date = dm_file_rec.dir_rec.creation_date;
                }
                break;
            }
            case DATA_BLOCK: {
                // Up to 256 data bytes, preceded by a byte count and a checksum
                uint16_t byte_count;
                uint8_t b0 = is.get();
                uint8_t b1 = is.get();
                byte_count = (b0 << 8) + b1;
                if (byte_count > 256) {
                    std::cout << "*** DATA_BLOCK byte count > 256: " << byte_count << std::endl;
                    std::exit(1);
                }
                uint16_t checksum, expected;
                b0 = is.get();
                b1 = is.get();
                expected = (b0 << 8) + b1;
                checksum = byte_count;
                
                uint8_t buffer[byte_count];
                is.read((char*)&(buffer[0]), byte_count);
                if (!is.good()) {
                    std::cout << "*** DATA_BLOCK shorter than byte count: " << dm_path << std::endl;
                    return;
                }
                
                // Checksum sums 16-bit words, skips final odd byte
                for (std::size_t i = 0; i < byte_count/2; ++i) {
                    uint16_t word;
                    word = buffer[2 * i] << 8;
                    word += buffer[2 * i + 1];
                    checksum += word;
                }
                if (checksum != expected) {
                    std::fstream::streamoff offset = os.tellp();
                    std::cout << "*** Bad checksum in " << name << " within " << dm_path << ": "
                              << std::hex << checksum << " expected: " << expected << " difference: " << (checksum - expected)
                              << std::dec << " around offset " << offset - byte_count << std::endl;
                } else {
                    // if (verbose) std::cout << "*** Good checksum" << std::endl;
                }
                
                //if (verbose) std::cout << "    DATA_BLOCK encountered for offset: " << os.tellp() << std::endl;
                if (unpack_dm_files)
                    os.write((char*)&(buffer[0]), byte_count);
                break;
            }
            case ERROR_BLOCK:
                // Fatal error
                std::cout << "*** ERROR_BLOCK encountered" << std::endl;
                std::exit(1);
            case END_BLOCK: {
                // End of DM file
                if (verbose) std::cout << "  END DM file" << std::endl;
                cleanup();
                break;
            }
            case DATE_BLOCK: {
                // Last-written date for current file
                unsigned char bytes[4];
                is.read((char*)bytes, 4); // seconds since 1 January 1901
                is.get(); is.get(); // ignore last Alto word
                output_date = alto_TIME_to_time_t(bytes);
                //if (verbose) std::cout << "    DATE_BLOCK: " << date_to_string(output_date) << std::endl;
                break;
            }
            default:
                break;
        }
    }
}

typedef struct {
    DiskPageLabel l;
    std::vector<UByte> d; // size is zero for free page or 512 bytes
} LabelAndData;

std::string da_to_string(DA da) {
    return std::to_string(da.restore)
    + "/" + std::to_string(da.disk)
    + "/" + std::to_string(da.head)
    + "/" + std::to_string(da.track)
    + "/" + std::to_string(da.sector);
}

typedef std::vector<LabelAndData> DiskImage;

/* Read the CopyDisk image file at image_path.
   References:
   1. _cd8_/pup/copydisk.{bravo,press}.~1~
   2. Indigo/IFS/IfsCopyDisk.dm.~2~
 */
void read_disk_image(std::ifstream& is, DiskParams& dp, DiskImage& di) {
    auto n = size(is);
    decltype(n) start = 0;
    is.seekg(start, is.beg);
    UWord length;
    UWord block_type;
    while (is.good()) {
        length = read_UWord(is);
        block_type = read_UWord(is);
        switch (block_type) {
            case hereAreDiskParams:
                if (length < 2 + 5 || length > 2 + 6) {
                    std::cout << "*** Unexpected length for [HereAreDiskParams] block: " << length << std::endl;
                    std::exit(1);
                }
                dp.disk_type = read_UWord(is);
                dp.tracks = read_UWord(is);
                dp.heads = read_UWord(is);
                dp.sectors = read_UWord(is);
                if (dp.disk_type == diablo31) dp.n_disks = read_UWord(is);
                /*if (verbose)*/ {
                    std::cout << "[HereAreDiskParams]\n"
                    " disk_type: " << dp.disk_type
                    << " cylinders: " << dp.tracks
                    << " heads: " << dp.heads
                    << " sectors: " << dp.sectors;
                    if (dp.disk_type == diablo31) std::cout << " disks: " << dp.n_disks;
                    std::cout << std::endl;
                }
                break;
            case hereIsDiskPage:
                if (dp.disk_type == diablo31) {
                    if (length < 12) {
                        std::cout << "*** Unexpected [HereIsDiskPage] block length: " << length << std::endl;
                        std::exit(1);
                    }
                    DiskPageHeader  h;
                    read_UWords(is, h.word, 2);
                    if (h.dh.packId != 0) {
                        std::cout << "*** Unexpected [HereIsDiskPage] nonzero pack id: " << h.dh.packId << std::endl;
                        std::exit(1);
                    }
                    VirtualDA vda = BFSVirtualDA(dp, h.dh.diskAddress);
                    if (vda >= di.size()) di.resize(vda + 1);
                    LabelAndData& ld = di[vda];
                    read_UWords(is, ld.l.word, 8);
                    if (ld.l.dl.next.disk == 1) {
                        std::cout << "*** Unexpected DA with disk=1" << std::endl;
                    }
                    /*if (verbose)*/ {
                        std::cout << "[HereIsDiskPage]\n";
                        std::cout << "  DA: " << da_to_string(h.dh.diskAddress) << " virtualDA: " << vda << "\n";
                        std::cout << "  Label:\n  next: " << da_to_string(ld.l.dl.next)
                        << " prev: " << da_to_string(ld.l.dl.previous)
                        << " blank: " << ld.l.dl.blank
                        << " nChars: " << ld.l.dl.num_chars
                        << " pageNum: " << ld.l.dl.page_number
                        << " FID (ver/SN(dir/rand/nolog/part1/part2): "
                        << ld.l.dl.file_id.version << " "
                        << (int)ld.l.dl.file_id.serial_number.directory << " "
                        << (int)ld.l.dl.file_id.serial_number.random << " "
                        << (int)ld.l.dl.file_id.serial_number.nolog << " "
                        << (int)ld.l.dl.file_id.serial_number.part1 << " "
                        << (int)ld.l.dl.file_id.serial_number.part2 << "\n";
                    }
                    if (length == 12) {
                        if (verbose) std::cout << "  Data absent -- free page" << std::endl;
                    } else {
                        ld.d.resize(512);
                        is.read((char*)&ld.d[0], 512);
                    }
                } else {
                    std::cout << "*** TODO: Implement [HereAreDiskParams] for Trident" << std::endl;
                    std::exit(1);
                }
                break;
            case endOfTransfer:
                if (length == 2) { // length, block_type
                    if (verbose) std::cout << "[EndOfTransfer]" << std::endl;
                    return;
                } else {
                    std::cout << "*** Unexpected [EndOfTransfer] block length: " << length << std::endl;
                    std::exit(1);
                }
            default:
                std::cout << "*** TODO: Unexpected CopyDisk block_type: " << block_type << std::endl;
                break;
        }
    }
}

void extract_file(const DiskParams& dp, const DiskImage& di, VirtualDA leaderDA,
                  const std::string& out_dir_path, const std::string& name, const FileRecord& image_file_rec, time_t created) {
    std::string path_to(join_paths(out_dir_path, name));
    std::ofstream os(path_to, std::ifstream::binary);
    auto n = di.size();
    decltype(n) i = leaderDA;
    DA da = di[i].l.dl.next;
    if (os) {
        std::fstream::streamoff size = 0;
        while (da != zeroDA) {
            decltype(i) i_prev = i;
            i = BFSVirtualDA(dp, da);
            if (i >= n) {
                std::cout << "*** Bad virtual disk address extracting from image file: " << out_dir_path << std::endl;
                std::exit(1);
            }
            UWord num_chars = di[i].l.dl.num_chars;
            os.write((const char*)&di[i].d[0], num_chars);
            size = size + num_chars;
            da = di[i].l.dl.next;
        }
        os.close();
        set_file_write_time(path_to, created);
        
        // Create file_table record
        FileRecord file_rec = image_file_rec;
        file_rec.dir_rec.dir_path += ">"
            + append_version(file_rec.dir_rec.file_name, file_rec.dir_rec.version)
            + "_";
        file_rec.dir_rec.file_name = name;
        file_rec.dir_rec.version = 0;
        file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date = created;
        file_rec.dir_rec.size = size;
        file_rec.data_rec.crc32 = checksum_file(path_to);
        file_rec.data_rec.size = size;
        add_to_file_table(file_rec);
    }
}

typedef UByte const* UBytePtr;

time_t get_TIME(UBytePtr& f) {
    // Precondition: [f, f+3) is a readable range
    unsigned char   time[4];
    std::copy_n(f, sizeof(time), time);
    f = f + sizeof(time);
    return alto_TIME_to_time_t(time);
}

void extract_files(const DiskParams& dp, const DiskImage& di, const std::string& out_dir_path,
                   const FileRecord& image_file_rec) {
    // outpath should be a directory; create it if necessary
    mkpath_np(out_dir_path.c_str(), 0777);
    // For each entry in SysDir, read leader page and copy data pages to a new file
    auto n = di.size();
    decltype(n) i;
    for (i = 1; i < n; ++i) { // skip virtual page zero
        if (di[i].l.dl.page_number == 0 && di[i].d.size() == 512) { // leader page of a file
            // ***** Skip SysDir (and other directories) and DiskDescriptor *****
            UBytePtr f = &di[i].d[0];
            time_t  created = get_TIME(f);
            /*time_t  written =*/ get_TIME(f);
            /*time_t  read = */ get_TIME(f);
            unsigned char buf[2 * maxLengthFnInWords];
            std::copy_n(f, sizeof(buf), buf);
            f = f + sizeof(buf);
            std::string name((char*)&buf[1], buf[0] - 1);
            if (!ci_equal(name, "SysDir") && !ci_equal(name, "DiskDescriptor")) {
                std::string created_date = date_to_string(created);
                /*if (verbose)*/ std::cout << " Extracting " << name << " " << created_date << std::endl;
                extract_file(dp, di, i, out_dir_path, name, image_file_rec, created);
            }
        }
    }
}

/* Extract the constituent files from a disk image and write them in the directory with path out_dir_path.
   Use image_file_rec.dir_rec as defaults.
 */
void load_disk_image(const std::string& image_path,
                  const std::string& out_dir_path,
                  const FileRecord&  image_file_rec,
                  bool list_only) {
    std::ifstream is(image_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** Could not open image file " << image_path << std::endl;
        std::exit(1);
    }
    if (verbose) std::cout << "Loading image file " << image_path << std::endl;
    DiskParams dp;
    DiskImage di;
    read_disk_image(is, dp, di);
    extract_files(dp, di, out_dir_path, image_file_rec);
}

void load_composite_files(const FileRecord& file_rec, const std::string& path_to) {
    const bool list_only = false; // ***** Command line option ?????
    auto f = file_rec.dir_rec.file_name.rfind('.');
    if (f != std::string::npos) {
        std::string ext(file_rec.dir_rec.file_name.substr(f, std::string::npos));
        // If file extension is .dm, unpack it
        if (unpack_dm_files) {
            if (ci_equal(ext, ".dm")) {
                std::string path_to_dir(path_to + "_");
                mkpath_np(path_to_dir.c_str(), 0777);
                load_DM_file(path_to, path_to_dir, file_rec, list_only);
            }
        }
        // If file extension is ".altodisk", ".bfs", or ".copydisk, unpack it
        if (unpack_disk_images) {
            if (ci_equal(ext, std::string(".altodisk"))
                || ci_equal(ext, std::string(".bfs"))
                || ci_equal(ext, std::string(".copydisk"))) {
                std::string path_to_dir(path_to + "_");
                mkpath_np(path_to_dir.c_str(), 0777);
                load_disk_image(path_to, path_to_dir, file_rec, list_only);
            }
        }
    }
}

// Restore images of the original file servers from the archive tape images.
// Precondition: read_tapes has executed.
void restore_tapes() {
    std::cout << std::endl << "Begin restoration to path: " << restore_path << std::endl;
    if (unpack_dm_files)    std::cout << "  DM files will be unpacked\n";
    if (unpack_disk_images) std::cout << "  Disk image files (.altodisk, .bfs, .copydisk) will be unpacked\n";
    
    std::vector<FileRecord>::size_type i = 0;
    std::vector<FileRecord>::size_type n = file_table.size();
    // Note load_DM_file will expand file_table, so iterators into it could become invalid.
    while (i < n) {
        FileRecord file_rec = file_table[i];
        
        // Create directory path if necessary
        std::string dir_path = join_paths(file_rec.dir_rec.server, subst(file_rec.dir_rec.dir_path, '>', '/'));
        mkpath_np(dir_path.c_str(), 0777);
        
        // Restore data to file, skipping data headers
        std::string path_from(join_paths(archive_path, path_from_record_id(file_rec.data_rec.ri)));
        std::string path_to(join_paths(restore_path,
                                       join_paths(dir_path,
                                                  append_version(file_rec.dir_rec.file_name,
                                                                 file_rec.dir_rec.version))));
        restore_file(path_from, path_to);
        set_file_write_time(path_to, file_rec.dir_rec.creation_date);
        
        // Handle .dm files and disk image files (.altodisk, .bfs, .copydisk)
        load_composite_files(file_rec, path_to);
        ++i;
    }
    std::cout << std::endl << "End restoration to path: " << restore_path << std::endl;
    if (unpack_dm_files) std::cout << "  " << (file_table.size() - n) << " new file table entries" << std::endl;
}

// Assuming file_name ends with ".~V~", return V (version number)
uint16_t get_version(const std::string& file_name) {
    auto f = file_name.find('~');
    auto l = file_name.rfind('~');
    if (f != l) {
        ++f; // advance to first digit of version number
        return std::stoi(file_name.substr(f, l - f));
    }
    return 0;
}

// Assuming file_name ends with ".~V~", return file_name with these characters removed
std::string remove_version(const std::string& file_name) {
    auto f = file_name.find('~');
    auto l = file_name.rfind('~');
    if (f == l || file_name[f - 1] != '.') {
        std::cout << "*** No version to remove" << std::endl;
        return file_name;
    }
    --f; // back up to period preceding version
    return file_name.substr(0, f);
}

// Copy file to designated path
void copy_file(const std::string& path_from, const std::string& path_to) {
    if (verbose)
        std::cout << "Copy " << path_from << " to " << path_to << std::endl;
    std::ifstream is(path_from, std::ifstream::in | std::ifstream::binary);
    if (is) {
        std::ofstream os(path_to);
        if (os) {
            // Our DRAM is (much) larger than the largest Alto disk drive
            std::fstream::streamoff length = size(is);
            std::vector<char> buffer(length);
            is.read(&(buffer[0]), length);
            os.write(&buffer[0], length);
        } else {
            std::cout << "*** copy_file couldn't open: " << path_to << " for writing" << std::endl;
            std::exit(1);
        }
    } else {
        std::cout << "*** copy_file couldn't open: " << path_from << " for reading" << std::endl;
        std::exit(1);
    }
}

void read_extra_file(const std::string& server, // e.g., {cd6}
                     const std::string& root, // e.g., /Users/.../_CD/cd6
                     const std::string& path, // e.g., tapeserver/docs
                     const std::string& name) { // e.g., interimmftp.memo.~1~
    std::string dir_path(join_paths(root, path));
    std::string full_path = join_paths(dir_path, name);
    // Create file_table record
    FileRecord file_rec;
    file_rec.dir_rec.server = server;
    file_rec.dir_rec.dir_path = subst(path, '/', '>');
    file_rec.dir_rec.file_name = remove_version(name);
    file_rec.dir_rec.version = get_version(name);
    file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date
                                = file_rec.dir_rec.read_date
                                = get_file_write_time(full_path);
    file_rec.dir_rec.size = get_file_size(full_path);
    file_rec.dir_rec.author = "UNKNOWN";
    file_rec.dir_rec.text = false; // *****
    file_rec.data_rec.crc32 = checksum_file(full_path);
    file_rec.data_rec.size = file_rec.dir_rec.size;
    add_to_file_table(file_rec);
    
    // Create directory path if necessary
    std::string restore_dir_path = join_paths(file_rec.dir_rec.server, path);
    mkpath_np(restore_dir_path.c_str(), 0777);
    
    // Copy file to restored directory
    std::string path_to(join_paths(restore_path,
                                   join_paths(restore_dir_path,
                                              append_version(file_rec.dir_rec.file_name,
                                                             file_rec.dir_rec.version))));
    copy_file(full_path, path_to);
    set_file_write_time(path_to, file_rec.dir_rec.creation_date);
    
    // Handle .dm files and disk image files (.altodisk, .bfs, .copydisk)
    load_composite_files(file_rec, path_to);
}

void read_extra_directory(const std::string& server, const std::string& root, const std::string& path) {
    DirMap dirmap;
    std::string dir_path(join_paths(root, path));
    read_dirmap(dir_path, dirmap);
    // if (verbose) print_dirmap(dir_path, dirmap);
    auto f = dirmap.cbegin();
    auto l = dirmap.cend();
    while (f != l) {
        std::string name = f->first;
        if (S_ISDIR(f->second) && name[0] != '.') {
            read_extra_directory(server, root, join_paths(path, name));
        } else if (S_ISREG(f->second) && !ci_equal(name, "YMTRANS.TBL")) {
            read_extra_file(server, root, path, name);
        }
        ++f;
    }
}

// Read and copy the cd6 and cd8 hierarchies, loading file_table and auxiliary indexes and expanding .dm files
void restore_extra_filesystems() {
    std::cout << std::endl << "Begin reading cd6, cd8 from path: " << extras_path << std::endl;
    if (unpack_dm_files) std::cout << "  DM files will be unpacked\n";
    if (unpack_disk_images) std::cout << "  Disk image files (.altodisk, .bfs, .copydisk) will be unpacked\n";
    auto file_table_size = file_table.size();
    DirMap top_level;
    read_dirmap(extras_path, top_level);
    // if (verbose) print_dirmap(path, top_level);
    for (auto f = top_level.cbegin(); f != top_level.cend(); ++f) {
        std::string name = f->first;
        if (S_ISDIR(f->second) && name.size() == 3
            && name[0] == 'c' && name[1] == 'd' && (name[2] == '6' || name[2] == '8')) {
            read_extra_directory("_" + name + "_", join_paths(extras_path, name), "");
        }
    }
    std::cout << std::endl << "End reading cd6, cd8; statistics: " << std::endl
              << "  " << file_table.size() - file_table_size << " new file table entries"
              << std::endl;
}

template <typename T, typename V, typename R>
//    requires(Regular(T) && Regular(V) && StrictWeakOrder(R))
void print_histogram(const std::map<T, V, R>& table, const std::string& name) {
    std::cout << "\n" << name << std::endl;
    auto f = table.cbegin();
    auto l = table.cend();
    while (f != l) {
        std::cout << " " << f->first << " " << f->second << "\n";
        ++f;
    }
    std::cout.flush();
}

void print_duplicates() {
    std::cout << "\nDuplicate files (same crc32)\n "
              << key_count(file_table_crc32_index)
              << " unique crc32s "
              << file_table.size() << " total files\n";
    auto f = file_table_crc32_index.cbegin();
    auto l = file_table_crc32_index.cend();
    while (f != l) {
        Crc32 crc32 = f->first;
        auto n = file_table_crc32_index.count(crc32);
        if (n > 1) {
            std::string file_name = file_table[f->second].dir_rec.file_name;
            std::cout << std::hex << crc32 << std::dec << " " << file_name << "\n";
            while (n > 0) {
                const RecordIdentifier& ri = file_table[f->second].data_rec.ri;
                const DirectoryRecord& dir_rec = file_table[f->second].dir_rec;
                std::string fn2("");
                if (!ci_equal(dir_rec.file_name, file_name)) fn2 = "* " + dir_rec.file_name;
                std::string version(std::to_string(dir_rec.version));
                if (dir_rec.version == 0) version = "-";
                std::cout << "    " << fn2
                << " " << dir_rec.server << " " << dir_rec.dir_path << " " << version << " "
                << ": " // end of sort key
                << dir_rec.size << " "
                << date_to_string(dir_rec.creation_date) << " "
                << dir_rec.author << " "
                << std::get<0>(ri) << " " << std::get<1>(ri) << " " << std::get<2>(ri) << "\n";
                ++f;
                --n;
            }
        } else ++f;
        
    }
}

// For each Alto filename, print a list of attributes.
void print_cross_ref() {
    std::cout << "\nCross reference" << std::endl;
    std::cout << " " << file_table.size() << " entries including DM and disk image contents" << std::endl;
    std::sort(file_table.begin(), file_table.end(), less);
    auto f = file_table.cbegin();
    auto l = file_table.cend();
    while (f != l) {
        std::cout << "  " << f->dir_rec.file_name << std::endl;
        auto first = f;
        do {
            std::string version(std::to_string(f->dir_rec.version));
            if (f->dir_rec.version == 0) version = "-";
            std::cout << "    "
                << f->dir_rec.server << " " << f->dir_rec.dir_path << " " << version << " "
                << ": " // end of sort key
                << f->dir_rec.size << " "
                << std::hex << f->data_rec.crc32 << std::dec << " "
                << date_to_string(f->dir_rec.creation_date) << " "
                << f->dir_rec.author << " "
                << std::get<0>(f->data_rec.ri) << " " << std::get<1>(f->data_rec.ri) << " " << std::get<2>(f->data_rec.ri)
                << "\n";
            ++f;
        } while (f != l && ci_equal(f->dir_rec.file_name, first->dir_rec.file_name));
    }
}

void usage() {
    std::cout << "Usage: restore_alto_files [--exclude=<name> | --in=<path to tape images> | --tilde\n"
                 "       | --out=<path to restore to> | --unpack_dm_files | --unpack_disk_images\n"
                 "       | --extras=<path to cd6/8> | --xref | --verbose]..." << std::endl;
    std::cout << "  --in is required; the others are optional" << std::endl;
    std::exit(1);
}

int main(int argc, const char * argv[]) {

    std::cout << "restore_alto_files of 8 Nov 2013" << std::endl;
    validate_assumptions();
    int i = 1;
    // Read command line first, then execute in canonical order.
    while (i < argc) {
        std::string arg(argv[i]);
        if (arg.compare(0, 10, "--exclude=") == 0) {
            exclude.insert(arg.substr(10, std::string::npos));
        } else if (arg.compare(0, 5, "--in=") == 0) {
            archive_path = arg.substr(5, std::string::npos);
        } else if (arg.compare(0, 7, "--tilde") == 0) {
            tilde = true;
        } else if (arg.compare(0, 6, "--out=") == 0) {
            restore_path = arg.substr(6, std::string::npos);
        } else if (arg.compare(0, 17, "--unpack_dm_files") == 0) {
            unpack_dm_files = true;
        } else if (arg.compare(0, 20, "--unpack_disk_images") == 0) {
            unpack_disk_images = true;
        } else if (arg.compare(0, 9, "--extras=") == 0) {
            extras_path = arg.substr(9, std::string::npos);
        } else if (arg.compare(0,  6, "--xref") == 0) {
            xref = true;
        } else if (arg.compare(0,  9, "--verbose") == 0) {
            verbose = true;
        } else {
            std::cout << "Unknown option: " << arg << std::endl;
            usage();
        }
        i = i + 1;
    }
    if (archive_path.empty()) {
        std::cout << "--in not specified" << std::endl;
        usage();
    }
    read_tapes(archive_path);
    if (!restore_path.empty())
        restore_tapes(); // tilde flag controls filename version format
    if (!extras_path.empty())
        restore_extra_filesystems();
    if (xref) {
        // Must print duplicates before cross_ref, because latter invalidates file_table_crc32_index
        print_duplicates();
        print_cross_ref();
        print_histogram(extension_table, "Extensions");
        print_histogram(author_table, "Authors");
    }

    return 0;
}

