//
//  web.cpp
//  restore_alto_files
//
//  Created by Paul McJones on 12/7/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include "web.h"
#include <cassert>

void gen_heading(std::ofstream& os, unsigned int level, const std::string& heading) {
    assert(level <= 6);
    os <<
    "<h" << level << ">" << heading << "</h" << level << ">\n";
}

void gen_div(std::ofstream& os,
             const std::string& style,
             std::function<void (std::ofstream& os, const std::string& indent)> gen_contents) {
    os <<
    "<div style=\"" << style << "\">\n";
      gen_contents(os, "      ");
    os <<
    "</div>\n";
}

void gen_span(std::ofstream& os, const std::string& style, const std::string& contents) {
    os <<
    "<span style=\"" << style << "\">" << contents << "</span>";
}

void gen_paragraph(std::ofstream& os, const std::string& style, const std::string& contents) {
    os <<
    "<p style=\"" << style << "\">" << contents << "</p>";
}

void gen_anchor(std::ofstream& os, const std::string& href, const std::string& contents, bool paragraph) {
    if (paragraph) os << "<p>";
        os <<
        "<a href=\"" << href << "\">" << contents << "</a>";
    if (paragraph) os << "</p>";
}

void gen_page(const std::string& path,
              const std::string& title,
              std::function<void (std::ofstream& os, const std::string& indent)> gen_contents) {
    std::ofstream os(path, std::fstream::trunc);
    
    os <<
    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">\n" // ***** or 4.01 or STRICT or TRANSITIONAL or XHTML ?????
    "<html>\n"
    "  <head>\n"
    "    <meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\">\n"
    "    <title>" << title << "</title>\n"
    "  </head>\n"
    "  <body>\n"
    "    ";
    gen_contents(os, "    ");
    os <<
    "  </body>\n"
    "</html>" << std::endl;
}

void gen_preformatted_page(const std::string& path,
                           const std::string& title,
                           std::function<void (std::ofstream& os, const std::string& indent)> gen_contents) {
    std::function<void (std::ofstream& os, const std::string& indent)> my_gen_contents =
    [&](std::ofstream& os, const std::string& indent) {
        os <<
        "<pre>\n";
          gen_contents(os, indent);
          os <<
        "</pre>\n";
    };
    gen_page(path, title, my_gen_contents);
}
