//
//  altoweb.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/30/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef __restore_alto_files__altoweb__
#define __restore_alto_files__altoweb__

#include "AltoFileType.h"
#include "archivist.h"
#include "utility.h"

namespace altoweb {

    void gen_pages(const std::string& web_path,
                   std::vector<FileRecord>& file_table,
                   std::multimap<Crc32, std::vector<FileRecord>::size_type>file_table_crc32_index,
                   std::map<Extension, int, ci_less>extension_table,
                   std::map<Author, int, ci_less> author_table);
    /* TODO
         Use relative URLs (and specify BASE in <HEAD>?)
         Create literal copies of text files?
         Link each file entry to corresponding file
         Add link to Xerox PARC/CHM agreement
         Add breadcrumbs on each page for navigating up hierarchy
         Make URL base a parameter of deployment target
     */
    
}

#endif /* defined(__restore_alto_files__genweb__) */

