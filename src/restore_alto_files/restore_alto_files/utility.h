//
//  utility.h
//  restore_alto_files
//
//  Created by Paul McJones on 11/18/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef __restore_alto_files__utility__
#define __restore_alto_files__utility__

#include "AltoDefs.h"
#include <fstream>
#include <iostream>
#include <map>
#include <string>


// Exception

struct raf_exception : public std::runtime_error
{
    raf_exception(const char* message) : runtime_error(message) { }
    raf_exception(const std::string& message) : runtime_error(message) { }
};


// Strings

// Substitute old_char for new_char in a copy of s.
std::string subst(std::string s, char old_char, char new_char);

// Case-independent string equality.
bool ci_equal(const std::string& a, const std::string& b);

// Case-independent string ordering.
struct ci_less {
    ci_less() {}
    bool operator()(const std::string& a, const std::string& b) const {
        std::size_t n = std::min(a.size(), b.size());
        std::size_t i = 0;
        while (i < n) {
            if (tolower(a[i]) < tolower(b[i])) return true;
            if (tolower(b[i]) < tolower(a[i])) return false;
            ++i;
        }
        return i < b.size();
    }
};

// Multimaps

// Count the number of non-equivalent keys in a multimap.
template <typename K, typename V>
size_t key_count(const std::multimap<K, V> &m) {
    if (m.empty()) {
        return 0;
    }
    size_t n = 1;
    auto f = m.cbegin();
    auto l = m.cend();
    auto key_less = m.key_comp();
    K lastkey = f->first;
    ++f;
    while (f != l) {
        if (key_less(lastkey, f->first)) {
            ++n;
            lastkey = f->first;
        }
        ++f;
    }
    return n;
}


// Directories

typedef std::map<std::string, mode_t> DirMap;

// Add entries of directory at path to dirmap.
void read_dirmap(std::string path, DirMap& dirmap);

void print_dirmap(const std::string& path, const DirMap& dirmap);

// Convert, for example, ifs/sources to ifs>sources>, or altotape/ddtape-rev-a.dm!1_ to altotape>ddtape-rev-a.dm!1>
std::string alto_style_dir_name(const std::string& dir_name);


// Files

inline
std::string get_extension(const std::string file_name) {
    auto f = file_name.rfind('.');
    return f == std::string::npos ? std::string("[Empty]") : file_name.substr(f, std::string::npos);
}

// Return a file pathname combining p1 and p2.
std::string join_paths(const std::string& p1, const std::string& p2);

off_t get_file_size(const std::string& path);

time_t get_file_write_time(const std::string& path);

void set_file_write_time(const std::string& path, time_t write_time);

// Convert a time_t to an Alto-format date string, e.g.,  "31-Jan-81 19:59:27 PST".
std::string date_to_string(std::time_t t);


// File streams

// Return the length of a file stream, preserving its current position.
std::fstream::streamoff size(std::ifstream& is);

#endif /* defined(__restore_alto_files__utility__) */
