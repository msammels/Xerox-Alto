//
//  Bfs.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/25/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_bfs_h
#define restore_alto_files_bfs_h

#include "AltoFileSys.h"
#include "archivist.h"

typedef struct {
    UWord   disk_type;  // 12B for Diablo 31, 2 for Trident
    UWord   tracks;     // 203
    UWord   heads;      //   2
    UWord   sectors;    //  12
    UWord   n_disks;    //   1 or 2 -- only if Diablo 31
} DiskParams;

typedef enum {
    trident     =   2,
    diablo31    = 012,
} DiskType;


// Disk Address (real)
typedef struct DA {
    UByte   restore:1;
    UByte   disk:1;
    UByte   head:1;
    UWord   track:9; // misnamed: really cylinder
    UByte   sector:4;
    friend bool operator==(DA x, DA y) {
        return x.sector == y.sector
            && x.track == y.track
            && x.head == y.head
            && x.disk == y.disk
            && x.restore == y.restore;
    }
    friend bool operator!=(DA x, DA y) {
        return !(x == y);
    }
} DA; // (real) DA in Bfs.d

inline
DA construct_DA(UByte sector, UWord track, UByte head, UByte disk, UByte restore) {
    DA da;
    da.sector = sector;
    da.track = track;
    da.head = head;
    da.disk = disk;
    da.restore = restore;
    return da;
}

const DA zeroDA = construct_DA(0, 0, 0, 0, 0);

// Convert real disk address to virtual
inline
VirtualDA BFSVirtualDA(const DiskParams& dp, DA da) {
    if (da == zeroDA) return eofDA;
    return (((((da.disk   * dp.tracks)
              + da.track) * dp.heads)
              + da.head)  * dp.sectors)
              + da.sector;
}

// Convert virtual disk address to real
inline
DA BFSRealDA(const DiskParams& dp, VirtualDA vda) {
    DA da;
    if (vda == eofDA) return zeroDA;
    da.sector = vda / dp.sectors; vda = vda % dp.sectors;
    da.head = vda / dp.heads; vda = vda % dp.heads;
    da.track = vda / dp.tracks;
    da.disk = vda % dp.tracks;
    da.restore = 0;
    return da;
}

//typedef union {
//    UWord   word;
//    DA      da;
//} DiskAddress;

// Disk Header
typedef struct {
    UWord   packId; // must be 0
    DA      diskAddress;
} DH;

typedef union {
    UWord   word[2];
    DH      dh;
} DiskPageHeader;


// FID usage:
// All three words are 01fffff for free page
typedef struct {
    UWord   version;
    SN      serial_number;
} FID; // File Identifier (used only on a disk label)

typedef union {
    UWord word[3];
    FID fid;
} FileIdentifier;

typedef struct {
    DA      next; // disk address of next file page, or eofDA
    DA      previous; // disk address of previous file page, or eofDA
    UWord   blank;
    UWord   num_chars; // between 0 and charsPerPage inclusive
                     // (ne charsPerPage only on last page)
    UWord   page_number; // * leader is page 0, first data page is page 1
    FID     file_id; // *
} DL; // Disk Label *=set by DoDiskCommand

typedef union {
    UWord   word[8];
    DL      dl;
} DiskPageLabel;

typedef union {
    UByte   byte[512];
    UWord   word[256];
} DiskPageData;

typedef struct {
    DiskPageHeader  h;
    DiskPageLabel   l;
    DiskPageData    d;
} DP;

typedef union {
    UWord word[sizeof(DP) / sizeof(UWord)];
    DP dp;
} DiskPage;

std::string append_version(const std::string& file_name, int version);

void load_disk_image(const std::string& image_path,
                     const std::string& out_dir_path,
                     const FileRecord* image_file_rec);

void load_dual_disk_image(const std::string& image_path_dp0,
                          const std::string& image_path_dp1,
                          const std::string& out_dir_path,
                          const FileRecord* image_file_rec);

/* Extract the constituent files from a copydisk-style image.
 Output directory name is name of (first) input image, with "_" appended.
 */
void load_copydisk_image(const std::string& image_path);

/* Extract the constituent files from an aar.c-style disk image.
 Output directory name is name of (first) input image, with "_" appended.
 */
void load_aar_disk_image(const std::string& image_path);

#endif
