//
//  AltoDefs.cpp
//  restore_alto_files
//
//  Created by Paul McJones on 1/15/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#include <cassert>
#include "AltoDefs.h"
#include "utility.h"

UWord read_UWord(std::istream& is, bool big_endian) {
    UByte b0 = is.get();
    UByte b1 = is.get();
    if (is.eof())
        throw raf_exception("read_UWord: eof on input");
    if (is.bad())
        throw raf_exception("read_UWord: bad on input");
    if (is.fail())
        throw raf_exception("read_UWord: fail on input");
    assert(is);
    return big_endian ? (b0 << 8) | b1 : (b1 << 8) | b0;
}

UDWord read_UDWord(std::istream& is, bool big_endian) {
    UWord w0 = read_UWord(is, big_endian);
    UWord w1 = read_UWord(is, big_endian);
    return big_endian ? (w0 << 16) | w1 : (w1 << 16) | w0;
}

// Read a sequence of big-endian 16-bit unsigneds from a stream
void read_UWords(std::istream& is, UWord* pWord, std::size_t nWords, bool big_endian) {
    while (nWords != 0) {
        *pWord = read_UWord(is, big_endian);
        ++pWord;
        --nWords;
    }
}

// Convert Alto TIME (seconds since 1 January 1901) to time_t.
time_t alto_TIME_to_time_t(unsigned char bytes[4]) {
    // Leap years between 1901 and 1970: 04 08 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68
    const unsigned int UnixCorrection = ((1970 - 1901)*365+17)*24*60*60; // seconds from Alto epoch to Unix epoch
    //const unsigned int UnixCorrection = 2177452800; // seconds from 1901/01/01 to 1970/01/01
    unsigned int s = (bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + bytes[3];
    return s - UnixCorrection;
}
