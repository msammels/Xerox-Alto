//
//  Bfs.cpp
//  restore_alto_files
//
//  Created by Paul McJones starting 10/25/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include <cassert>
#include <fstream>
#include <unistd.h>
#include <vector>
#include "Bfs.h"
#include "CopyDiskProtocol.h"
#include "FileTable.h"
#include "utility.h"

extern bool verbose;
extern bool tilde;

void verify_assumptions() {
}

typedef struct {
    DiskPageLabel l;
    std::vector<UByte> d; // size is zero for free page or 512 bytes
} LabelAndData;

std::string da_to_string(DA da) {
    return std::to_string(da.restore)
    + "/" + std::to_string(da.disk)
    + "/" + std::to_string(da.head)
    + "/" + std::to_string(da.track)
    + "/" + std::to_string(da.sector);
}

void print_label(DA da, VirtualDA vda, const DiskPageLabel& l) {
    std::cout << "  DA: " << da_to_string(da) << " virtualDA: " << vda << "\n";
    std::cout << "  Label:\n    next: " << da_to_string(l.dl.next)
    << " prev: " << da_to_string(l.dl.previous)
    << " blank: " << l.dl.blank
    << " nChars: " << l.dl.num_chars
    << " pageNum: " << l.dl.page_number
    << " FID (ver/SN(dir/rand/nolog/part1/part2): "
    << l.dl.file_id.version << " "
    << (int)l.dl.file_id.serial_number.directory << " "
    << (int)l.dl.file_id.serial_number.random << " "
    << (int)l.dl.file_id.serial_number.nolog << " "
    << (int)l.dl.file_id.serial_number.part1 << " "
    << (int)l.dl.file_id.serial_number.part2 << "\n";
}

std::string append_version(const std::string& file_name, int version) {
    std::string ver(std::to_string(version));
    if (version == 0) return file_name;
    if (tilde) return file_name + ".~" + ver + "~";
    return file_name + "!" + ver;
}

typedef std::vector<LabelAndData> DiskImage;

/* Read the CopyDisk image file at image_path.
 References:
 1. Hint from David Boggs: disk format is (subset of) wire protocol.
 2. _cd8_/pup/copydisk.{bravo,press}!1
 3. Indigo/IFS/IfsCopyDisk.dm!1
 */
void read_copydisk_image(const std::string& image_path, DiskParams& dp, DiskImage& di) {
    std::ifstream is(image_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {std::cout << "*** Could not open copydisk-style image file " + image_path << std::endl; std::exit(1);}
    is.seekg(0, is.beg);
    UWord length;
    UWord block_type;
    while (is.good()) {
        length = read_UWord(is);
        block_type = read_UWord(is);
        switch (block_type) {
            case hereAreDiskParams:
                if (length < 2 + 5 || length > 2 + 6) {
                    std::cout << "*** Unexpected length for [HereAreDiskParams] block: " << length << std::endl;
                    std::exit(1);
                }
                dp.disk_type = read_UWord(is);
                dp.tracks = read_UWord(is);
                dp.heads = read_UWord(is);
                dp.sectors = read_UWord(is);
                if (dp.disk_type == diablo31) dp.n_disks = read_UWord(is);
                if (verbose) {
                    std::cout << "[HereAreDiskParams]\n"
                    " disk_type: " << dp.disk_type
                    << " cylinders: " << dp.tracks
                    << " heads: " << dp.heads
                    << " sectors: " << dp.sectors;
                    if (dp.disk_type == diablo31) std::cout << " disks: " << dp.n_disks;
                    std::cout << std::endl;
                }
                break;
            case hereIsDiskPage:
                if (dp.disk_type == diablo31) {
                    if (length < 12) {
                        std::cout << "*** Unexpected [HereIsDiskPage] block length: " << length << std::endl;
                        std::exit(1);
                    }
                    DiskPageHeader  h;
                    read_UWords(is, h.word, sizeof(DiskPageHeader) / sizeof(UWord));
                    if (h.dh.packId != 0) {
                        std::cout << "*** Unexpected [HereIsDiskPage] nonzero pack id: " << h.dh.packId << std::endl;
                        std::exit(1);
                    }
                    VirtualDA vda = BFSVirtualDA(dp, h.dh.diskAddress);
                    if (vda >= di.size()) di.resize(vda + 1);
                    LabelAndData& ld = di[vda];
                    read_UWords(is, ld.l.word, sizeof(DiskPageLabel) / sizeof(UWord));
                    if (verbose) {
                        std::cout << "[HereIsDiskPage]\n";
                        print_label(h.dh.diskAddress, vda, ld.l);
                    }
                    if (length == 12) {
                        if (verbose) std::cout << "  Data absent -- free page" << std::endl;
                    } else {
                        ld.d.resize(512);
                        is.read((char*)&ld.d[0], 512);
                    }
                } else {
                    std::cout << "*** TODO: Implement [HereAreDiskParams] for Trident" << std::endl;
                    std::exit(1);
                }
                break;
            case endOfTransfer:
                if (length == 2) { // length, block_type
                    if (verbose) std::cout << "[EndOfTransfer]" << std::endl;
                    return;
                } else {
                    std::cout << "*** Unexpected [EndOfTransfer] block length: " << length << std::endl;
                    std::exit(1);
                }
            default:
                std::cout << "*** TODO: Unexpected CopyDisk block_type: " << block_type << std::endl;
                break;
        }
    }
}

// Image is only pack of a single-disk filesystem, or first or second pack of a two-disk filesystem.
void read_aar_disk_image(const std::string& image_path, const DiskParams& dp, DiskImage& di) {
    std::ifstream is(image_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** Could not open aar.c-style image file " << image_path << std::endl;
        std::exit(1);
    }
    // An aar.c-style disk image contains 203*2*12 records, each consisting of:
    //   page number (2 bytes, ignored here)
    //   disk header (4 bytes)
    //   disk label (16 bytes)
    //   disk data (512 bytes)
    const int PAGES_PER_DISK = dp.sectors * dp.heads * dp.tracks; // Diablo 31: cylinders * heads * sectors
    const int RECORD_SIZE = 2 + 4 + 16 + 512; // page number + header + label + data
    auto n = size(is);
    if (n != PAGES_PER_DISK * RECORD_SIZE) {
        std::cout << "*** read_aar_disk_image: unexpected image size: " << n << std::endl;
        std::exit(1);
    }
    
    // It maybe stored in big-endian or little-endian order so we must detect that.
    is.seekg(RECORD_SIZE, is.beg);
    bool big_endian = false;
    UWord page_num = read_UWord(is, big_endian);
    if (page_num == 256)
        big_endian = true;
    else {
        assert(page_num == 1);
    }
    
    di.resize(di.size() + PAGES_PER_DISK);
    decltype(n) i = 0;
    is.seekg(i, is.beg);
    while (i < n) {
        /* page_num = */ read_UWord(is, big_endian);
        DiskPageHeader  h;
        read_UWords(is, h.word, sizeof(DiskPageHeader) / sizeof(UWord), big_endian);
        if (h.dh.packId != 0) {
            std::cout << "*** read_aar_disk_image: unexpected nonzero pack id: " << h.dh.packId << " at "
            << da_to_string(h.dh.diskAddress) << "; ignoring\n";
        }
        VirtualDA vda = BFSVirtualDA(dp, h.dh.diskAddress);
        if (vda < di.size()) {
            LabelAndData& ld = di[vda];
            read_UWords(is, ld.l.word, sizeof(DiskPageLabel) / sizeof(UWord), big_endian);
            if (verbose) {
                print_label(h.dh.diskAddress, vda, ld.l);
            }
            ld.d.resize(512);
            if (big_endian) {
                is.read((char*)&ld.d[0], 512);
            } else {
                char data[512];
                is.read(data, 512);
                swab(data, &ld.d[0], 512);
            }
        } else {
            std::cout << "*** Illegal disk address: " << vda << " (" << da_to_string(h.dh.diskAddress)
            << ") (disk image needs scavenging?)" << std::endl;
            // Not fatal; presumably disk image really needed scavenging
            is.seekg(sizeof(DiskPageLabel) + 512, is.cur);
        }
        i += RECORD_SIZE;
    }
}

// If image_file_rec != 0, add the file to the file_table, using image_file_rec for defaults.
void extract_file_from_disk_image(const DiskParams& dp, const DiskImage& di, VirtualDA leaderDA,
                                  const std::string& out_dir_path, const std::string& name, const FileRecord* image_file_rec, time_t created) {
    std::string path_to(join_paths(out_dir_path, name));
    std::ofstream os(path_to, std::ifstream::binary);
    auto n = di.size();
    decltype(n) i = leaderDA, prev_i;
    DA da = di[i].l.dl.next;
    if (os) {
        std::fstream::streamoff size = 0;
        while (da != zeroDA) {
            prev_i = i;
            i = BFSVirtualDA(dp, da);
            if (i >= n) {
                std::cout << "*** Illegal disk address: " << i << " (" << da_to_string(da) << "); truncating file: " << path_to << " (disk image needs scavenging?)" << std::endl;
                // Not fatal; presumably disk image really needed scavenging
                break;
            }
            UWord num_chars = di[i].l.dl.num_chars;
            os.write((const char*)&di[i].d[0], num_chars);
            size = size + num_chars;
            da = di[i].l.dl.next;
        }
        os.close();
        set_file_write_time(path_to, created);
        
        // Create file_table record
        if (image_file_rec != 0) {
            FileRecord file_rec = *image_file_rec;
            file_rec.dir_rec.dir_path += ">"
            + append_version(file_rec.dir_rec.file_name, file_rec.dir_rec.version)
            + "_";
            file_rec.dir_rec.file_name = name;
            file_rec.dir_rec.version = 0;
            file_rec.dir_rec.write_date = file_rec.dir_rec.creation_date = created;
            file_rec.dir_rec.size = size;
            compute_attributes_from_file(path_to,
                                         file_rec.data_rec.crc32,
                                         file_rec.data_rec.f_t);
            file_rec.dir_rec.text = file_rec.data_rec.f_t <= text;
            file_rec.data_rec.size = size;
            add_to_file_table(file_rec);
        }
    }
}

typedef UByte const* UBytePtr;

time_t get_TIME(UBytePtr& f) {
    // Precondition: [f, f+3) is a readable range
    unsigned char   time[4];
    std::copy_n(f, sizeof(time), time);
    f += sizeof(time);
    return alto_TIME_to_time_t(time);
}

void extract_files(const DiskParams& dp, const DiskImage& di, const std::string& out_dir_path,
                   const FileRecord* image_file_rec) {
    // out_dir_path should be a directory; create it if necessary.
    mkpath_np(out_dir_path.c_str(), 0777);
    // For each entry in SysDir, read leader page and copy data pages to a new file.
    auto n = di.size();
    decltype(n) i;
    for (i = 1; i < n; ++i) { // skip virtual page zero
        const LabelAndData& ld = di[i];
        if (ld.l.dl.page_number == 0 && ld.d.size() == 512 &&
            (ld.l.dl.file_id.version != 0177777
             || ld.l.word[6] != 0177777
             || ld.l.word[7] != 0177777)) { // leader page of a file
                // ***** Skip SysDir (and other directories) and DiskDescriptor *****
                UBytePtr f = &ld.d[0];
                time_t  created = get_TIME(f);
                /*time_t  written =*/ get_TIME(f);
                /*time_t  read = */ get_TIME(f);
                unsigned char buf[2 * maxLengthFnInWords];
                std::copy_n(f, sizeof(buf), buf);
                if (buf[0] != 0) {
                    std::string name((char*)&buf[1], buf[0] - 1);
                    if (!ci_equal(name, "SysDir") && !ci_equal(name, "DiskDescriptor")) {
                        std::string created_date = date_to_string(created);
                        if (verbose) std::cout << " Extracting " << name << " " << created_date << std::endl;
                        extract_file_from_disk_image(dp, di, i, out_dir_path, name, image_file_rec, created);
                    }
                } else {
                    std::cout << "*** extract_files: skipping zero-length filename\n";
                }
                f = f + sizeof(buf);
            }
    }
}

/* Extract the constituent files from a disk image and write them in the directory with path out_dir_path.
   Use image_file_rec.dir_rec as defaults.
 */
void load_disk_image(const std::string& image_path,
                     const std::string& out_dir_path,
                     const FileRecord* image_file_rec) {
    /*if (verbose)*/
        std::cout << "Loading image file " << image_path << std::endl;
    DiskParams dp;
    DiskImage di;
    read_copydisk_image(image_path, dp, di);
    extract_files(dp, di, out_dir_path, image_file_rec);
}

void load_dual_disk_image(const std::string& image_path_dp0,
                          const std::string& image_path_dp1,
                     const std::string& out_dir_path,
                     const FileRecord* image_file_rec) {
    /*if (verbose)*/
    std::cout << "Loading dual pack image files " << image_path_dp0 << " and " << image_path_dp1 << std::endl;
    DiskParams dp_1, dp_2;
    DiskImage di;
    read_copydisk_image(image_path_dp0, dp_1, di);
    read_copydisk_image(image_path_dp1, dp_2, di);
    assert (dp_1.disk_type == dp_2.disk_type && dp_1.tracks == dp_2.tracks && dp_1.heads == dp_2.heads
            && dp_1.sectors == dp_2.sectors);
    dp_1.n_disks = 2;
    extract_files(dp_1, di, out_dir_path, image_file_rec);
}


/* Extract the constituent files from a copydisk-style image.
   Output directory name is name of (first) input image, with "_" appended.
 */
void load_copydisk_image(const std::string& image_path) {
    if (verbose) std::cout << "Loading copydisk-style image file(s) " << image_path << std::endl;
    // Assumed disk parameters:
    auto f = image_path.rfind(',');
    std::string image_path_1(image_path);
    std::string image_path_2;
    if (f != std::string::npos) {
        image_path_1 = image_path.substr(0, f);
        ++f;
        image_path_2 = image_path.substr(f, std::string::npos);
    }
    DiskParams dp_1, dp_2;
    std::string out_dir_path(image_path_1 + "_");
    DiskImage di;
    read_copydisk_image(image_path_1, dp_1, di);
    if (!image_path_2.empty()) {
        read_copydisk_image(image_path_2, dp_2, di);
        assert (dp_1.disk_type == dp_2.disk_type && dp_1.tracks == dp_2.tracks && dp_1.heads == dp_2.heads
                && dp_1.sectors == dp_2.sectors);
        dp_1.n_disks = 2;
    }
    extract_files(dp_1, di, out_dir_path, nullptr);
}

/* Extract the constituent files from an aar.c-style disk image.
   Output directory name is name of (first) input image, with "_" appended.
 */
void load_aar_disk_image(const std::string& image_path) {
    if (verbose) std::cout << "Loading aar.c-style image file(s) " << image_path << std::endl;
    // Assumed disk parameters:
    DiskParams dp = {012 /* Diablo 31 */, 203, 2, 12, 1}; // n_disks is 1 or 2
    auto f = image_path.rfind(',');
    std::string image_path_1(image_path);
    std::string image_path_2;
    if (f != std::string::npos) {
        image_path_1 = image_path.substr(0, f);
        ++f;
        image_path_2 = image_path.substr(f, std::string::npos);
        dp.n_disks = 2;
    }
    std::string out_dir_path(image_path_1 + "_");
    DiskImage di;
    read_aar_disk_image(image_path_1, dp, di);
    if (dp.n_disks == 2)
        read_aar_disk_image(image_path_2, dp, di);
    extract_files(dp, di, out_dir_path, nullptr);
}
