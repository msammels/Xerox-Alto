//
//  AltoDefs.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/28/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_Header_h
#define restore_alto_files_Header_h

#include <istream>

typedef uint8_t     UByte;
typedef uint16_t    UWord;
typedef uint32_t    UDWord;

// Read a 16-bit unsigned from a stream -- big-endian by default
UWord read_UWord(std::istream& is, bool big_endian = true);

// Read a 32-bit unsigned from a stream -- big-endian by default
UDWord read_UDWord(std::istream& is, bool big_endian = true);

// Read a sequence of 16-bit unsigneds from a stream -- big-endian by default
void read_UWords(std::istream& is, UWord* pWord, std::size_t nWords, bool big_endian = true);

// Convert Alto TIME (seconds since 1 January 1901) to time_t.
time_t alto_TIME_to_time_t(unsigned char bytes[4]);

#endif
