//
//  CopyDiskProtocol.h
//  restore_alto_files
//
//  Created by Paul McJones on 11/5/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_CopyDiskProtocol_h
#define restore_alto_files_CopyDiskProtocol_h


/* CopyDisk protocol (from CopyDisk.bravo)
 The format of the disk parameters in a [HereAreDiskParams] response is:
 
 words 0-1: standard header
 word 2: 12B (registered disk type)
 word 3: # of cylinders
 word 4: # of heads
 word 5: # of sectors
 word 6: # of disks
 
 The format of the transfer parameters in [RetrieveDisk] and [StoreDisk] commands is:
 
 words 0-1: standard header
 words 2: first real disk address
 words 3: last real disk address
 
 The format of a [HereIsDiskPage] command is:
 
 words 0-1: standard header
 words 2-3: header record
 words 4-11: label record
 words 12-267: data record
 */

typedef enum {
    version = 1,
    sendDiskParamsR = 2,
    hereAreDiskParams = 3,
    storeDisk = 4,
    retrieveDisk = 5,
    hereIsDiskPage = 6,
    endOfTransfer = 7,
    sendErrors = 010,
    hereAreErrors = 011,
    no = 012,
    yes = 013,
    comment = 014,
    login = 015,
    sendDiskParamsW = 016
} BlockType;

#endif
