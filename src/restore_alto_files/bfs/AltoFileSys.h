//
//  AltoFileSys.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/28/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//
//  Based on:
//
//  AltoFileSys.D -- Definitions for things kept on a disk.
//  Copyright Xerox Corporation 1979
//  Last modified November 17, 1980  4:32 PM by Boggs


#ifndef restore_alto_files_AltoFileSys_h
#define restore_alto_files_AltoFileSys_h

#include "AltoDefs.h"

template <int n>
struct STRING {
    UByte   length;
    UByte   ch[n]; // the Alto Bcpl version subscript range is [1..255]
};

const int lSTRING = sizeof(STRING<255>) / sizeof(UWord); // (maximum) number of words occupied by a STRING

// Time -- seconds since epoch?
typedef struct {
    UWord h;
    UWord l;
} TIME;

const int lTIME = sizeof(TIME) / sizeof(UWord); // number of words occupied by a TIME

//  F I L E    D E S C R I P T O R S

// Virtual Disk Address
typedef UWord VirtualDA;
const VirtualDA eofDA = 0;

// File Serial Number
typedef struct {
    struct {
        UWord part1:13;
        UByte nolog:1;
        UByte random:1;
        UByte directory:1;
    };
    UWord part2;
} SN;

// File Pointer -- describes a particular file by label & leader page position
typedef struct {
    SN          serialNumber;
    UWord       version;
    UWord       blank;
    VirtualDA   leaderVirtualDa;
} FP;

const int lFP = sizeof(FP) / sizeof(UWord); // number of words occupied by an FP


// File Address -- describes a particular spot in some file, but
//		does not contain enough info to read it (SN, vn missing)
typedef struct {
    UWord   da;
    UWord   pageNumber;   // 0, 1, ...
    UWord   charPos;      // byte position within a page
} FA;

const int lFA = sizeof(FA) / sizeof(UWord); // number of words occupied by an FA

// Complete File Address -- complete information needed to address
//		any byte in the file structure
typedef struct {
    FP  fp;
    FA  fa;
} CFA;


//     D I S K    D E S C R I P T O R

// Disk Descriptor Header
typedef struct {
    UWord   nDisks;     // number of disks
    UWord   nTracks;    // number of tracks
    UWord   nHeads;     // number of heads
    UWord   nSectors;   // number of sectors
    SN      lastSn;     // last serial number used on disk
    UWord   blank;      // formerly bitTableChanged (OS17 and before)
    UWord   diskBTsize; // number of valid words in the disk bit table
    UWord   defaultVersionsKept;    // default number of file versions kept
                                    // 0 means no multiple versions enabled
    UWord   freePages;
    UWord   blank1[6];
} KDH;


//      L E A D E R   P A G E

// Leader Page -- note that the definition is independent of page size
//		(assumes at least 256 words can be stored)

// Maximum length of a filename
const int maxLengthFn = 39;                         // for all OS routines
const int maxLengthFnInWords = (maxLengthFn+2)/2;   // enough for final "."


typedef struct {
    TIME    created;
    TIME    written;
    TIME    read;
    UWord   name[maxLengthFnInWords];
    UWord   leaderProps[210];   // properties stored in leader page
    UWord   spare[256-3*lTIME-maxLengthFnInWords-210-2-lFP-lFA];
    
    UByte   propertyLength;
    UByte   propertyBegin;
    
    UByte   changeSerial:8;
    UByte   blank1:7;
    UByte   consecutive:1;
    
    FP      dirFP;          // hint for directory
    FA      hintLastPageFa; // hint for end of file
} LD;

// File properties (partly in leader)
// 0 word (type and length both zero) terminates the list
typedef struct {
    UByte   length:8;
    UByte   type:8;
} FPROP;

// FPROP types 0-177b are reserved for the system.
const int fpropTypeDShape = 1;	// contains a DSHAPE (in leader page of SysDir)
const int fpropTypePartitionName = 2;  // contains a STRING which is the name of this partition (in leader page of SysDir)
// FRPOP types 200b-277b are for users.
// type 200b used by Bravo X
// type 201b used by E. Schmidt

typedef struct {
    UWord   nDisks;     // number of disks
    UWord   nTracks;    // number of tracks
    UWord   nHeads;     // number of heads
    UWord   nSectors;   // number of sectors
} DSHAPE;

const int lDSHAPE = sizeof(DSHAPE) / sizeof(UWord);


//      D I R E C T O R I E S

// Directory entry value
typedef struct {
    UWord   length:10;
    UByte   type:6;
    FP      fp;
    STRING<maxLengthFn+1>   name;
} DV;

typedef struct {
    UWord   type_and_length;
    // File Pointer -- describes a particular file by label & leader page position
        // File Serial Number
        UWord       dir_rand_nolog_part1;
        UWord       part2;
    UWord       version;
    UWord       blank;
    VirtualDA   leaderVirtualDa;
    STRING<maxLengthFn+1>   name;
} DV_swap;

const int maxDVPerPage = 256 / (1 + lFP + 1); // 1 word is shortest STRING

const int lDV = sizeof(DV) / sizeof(UWord);

// DV types
const int dvTypeFree = 0;
const int dvTypeFile = 1;

#endif
