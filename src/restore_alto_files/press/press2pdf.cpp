//
//  press2pdf.cpp
//
//  Created by Paul McJones on 10/21/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//

#include <assert.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
#include "AltoDefs.h"
#include "Press.h"
#include "press2pdf.h"
#include "pstream.h"
#include "utility.h"

bool Debug = false;

void read_doc_dir(std::istream& is, DocumentDirectory& doc_dir) {
    // Get length of file.
    is.seekg(0, is.end);
    std::fstream::streamoff length = is.tellg();
    if (length % 512 != 0) throw raf_exception("*** File length must be multiple of 512 bytes");
    // Read document directory
    is.seekg(-sizeof(DocumentDirectory), is.end);
    doc_dir.password = read_UWord(is);
    if (doc_dir.password != Presspassword)
        throw raf_exception(("Wrong document directory password: " + std::to_string(doc_dir.password)).c_str());
    doc_dir.rec_count = read_UWord(is);
    doc_dir.part_count = read_UWord(is);
    doc_dir.pd_first = read_UWord(is);
    doc_dir.pd_count = read_UWord(is);
    doc_dir.back_pointer = read_UWord(is);
    doc_dir.date = read_UDWord(is);
    doc_dir.first_copy = read_UWord(is);
    doc_dir.last_copy = read_UWord(is);
    is.seekg(236, is.cur); // ***** Express in terms of offset differences
    is.read((char*)&doc_dir.file_name, sizeof(doc_dir.file_name));
    is.read((char*)&doc_dir.creator_name, sizeof(doc_dir.creator_name));
    is.read((char*)&doc_dir.creation_date, sizeof(doc_dir.creation_date));
    is.seekg(132, is.cur); // ***** Express in terms of offset differences
    if (doc_dir.rec_count * WordsPerRecord * sizeof(UWord) != length) {
        throw raf_exception("Document directory record count does not match file size");
    }
    if (is.tellg() != length) {
        throw raf_exception("Error reading document directory -- check code and recompile");
    }
}

typedef std::vector<PartDirectoryEntry> PartDirectory;

void read_part_dir(std::istream& is, const DocumentDirectory& doc_dir, PartDirectory& part_dir) {
    if (doc_dir.pd_count * sizeof(PartDirectoryEntry) > records_to_bytes(doc_dir.pd_count)) {
        throw raf_exception("Part count too large for part directory record count");
    }
    is.seekg(records_to_bytes(doc_dir.pd_first), is.beg);
    part_dir.reserve(records_to_bytes(doc_dir.part_count));
    UWord i = 0;
    while (i < doc_dir.part_count) {
        PartDirectoryEntry part_dir_ent;
        part_dir_ent.part_type = read_UWord(is); // actually SWord
        part_dir_ent.part_first = read_UWord(is);
        part_dir_ent.part_count = read_UWord(is);
        part_dir_ent.EL_padding_size = read_UWord(is);
        part_dir.push_back(part_dir_ent);
        ++i;
    }
}

typedef std::vector<FontDirectoryEntry> FontDirectory;

void read_font_dir(std::istream& is, const PartDirectory& part_dir, FontDirectory& font_dir) {
    auto f = part_dir.cbegin();
    auto l = part_dir.cend();
    while (f != l && f->part_type != PartTypeFontDir) ++f;
    if (f == l) {
        throw raf_exception("Document lacks font directory");
    }
    is.seekg(records_to_bytes(f->part_first), is.beg);
    std::size_t offset = 0;
    std::size_t size = records_to_bytes(f->part_count);
    
    while (offset < size) {
        FontDirectoryEntry font_dir_ent;
        font_dir_ent.length = read_UWord(is);
        if (font_dir_ent.length == 0) break;
        font_dir_ent.font_set = is.get();
        font_dir_ent.font = is.get();
        font_dir_ent.m = is.get();
        font_dir_ent.n = is.get();
        if (font_dir_ent.n != 0377) {
            // Normal font
            if (font_dir_ent.length * sizeof(UWord) != sizeof(FontDirectoryEntry)) {
                throw raf_exception("Unexpected font directory entry size");
            }
            is.read((char*)&font_dir_ent.family, sizeof(font_dir_ent.family));
            font_dir_ent.face = is.get();
            font_dir_ent.source = is.get();
            font_dir_ent.size = read_UWord(is); // actually SWord
            font_dir_ent.rotation = read_UWord(is);
            font_dir.push_back(font_dir_ent);
        } else {
            // Alternate object-based font format
            throw raf_exception("Object-based font format not yet supported");
        }
        offset = offset + font_dir_ent.length;
    }
}

void print_font_dir(const FontDirectory& font_dir) {
    std::cout << "font_set font m-n family face source size rotation" << std::endl;
    auto f = font_dir.cbegin();
    auto l = font_dir.cend();
    while (f != l) {
        std::cout << (int)f->font_set << " "
                  << (int)f->font << " " << (int)f-> m << "-" << (int)f->n << " "
                  << std::string((char*)&f->family[1], f->family[0]) << " "
                  << (int)f->face << " "
                  << (int)f->source << " "
                  << (int)f->size << " "
                  << (int)f->rotation << std::endl;
        ++f;
    }
}

static FontDirectoryEntry mock_font;

static std::string the_in_path;

const FontDirectoryEntry* get_font(const FontDirectory font_dir, UByte font_set, UByte font) {
    auto f = font_dir.cbegin();
    auto l = font_dir.cend();
    while (f != l && (f->font_set != font_set || f->font != font)) ++f;
    if (f != l) return &(*f);
    f = font_dir.cbegin();
    if (f != l) {
        std::cout << "*** press_to_pdf(" << the_in_path << "): Couldn't find font_set " << (int)font_set << " / font " << (int)font << "; using first font" << std::endl;
        return &(*font_dir.cbegin()); // ******
    }
    std::cout << "*** press_to_pdf(" << the_in_path << "): font_dir empty; using mock font" << std::endl;
    mock_font.family[0] = 0;
    mock_font.family[1] = 'M';
    mock_font.family[2] = 'O';
    mock_font.family[3] = 'C';
    mock_font.family[4] = 'K';
    mock_font.size = 10; // points
    mock_font.face = NormalFace;
    return &mock_font;
}

void generate_prologue(const std::string& alto_file_name,
                       const DocumentDirectory& doc_dir,
                       const PartDirectory& part_dir,
                       const FontDirectory& font_dir,
                       std::ostream& os) {
    os << "%!PS-Adobe-2.0\n";
    os << "%%Creator: press2ps\n";
    os << "%%Title: " << alto_file_name << "\n";
    os << "%%Pages: " << doc_dir.part_count - 1 << "\n"; // ***** Skipping 1 for font_dir -- is this safe ?????
    // os << "%%BoundingBox: llx lly urx ury\n"; ***** Compute join of all entity bounding boxes ?????
    // os << "%%DocumentPaperSizes: Letter\n"; ***** Is this in Press file ?????
    // os << "%%Orientation: Portrait\n"; ***** Is this in Press file?
    os << "%%EndComments\n";
    // ***** Include press_to_pdf-specific stuff ?????
    os << "%%EndProlog\n";
}

void read_entity_trailer(std::istream& is, EntityTrailer& entity_trailer) {
    entity_trailer.entity_type = is.get();
    entity_trailer.font_set = is.get();
    entity_trailer.begin_byte = read_UDWord(is);
    entity_trailer.byte_length = read_UDWord(is);
    entity_trailer.Xe = read_UWord(is);
    entity_trailer.Ye = read_UWord(is);
    entity_trailer.x_left = read_UWord(is);
    entity_trailer.y_bottom = read_UWord(is);
    entity_trailer.width = read_UWord(is);
    entity_trailer.height = read_UWord(is);
    entity_trailer.length = read_UWord(is);
}

static const char hex_digit[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void bytes_to_hex(std::ostream& os,
                  int dots, int lines, int bits_per_sample,
                  std::function<uint8_t(void)> read_byte) {
    // ***** Press manual says bits_per_sample could be as large as 16, and doesn't restrict to power of 2.
    assert(bits_per_sample <= 8 && 8 % bits_per_sample == 0);
    for (int l = 0; l < lines; ++l) {
        for (int d = 0; d < dots; d += (8 / bits_per_sample)) {
            uint8_t b = read_byte();
            os << hex_digit[(b >> 4) & 017] << hex_digit[b & 017];
            if (d % 64 == 63) os << "\n";
        };
    }
}

void read_entity(std::istream& is, const FontDirectory font_dir, std::size_t entity_list_limit,
                 const std::vector<char>& data_list, const EntityTrailer& entity_trailer,
                 std::ostream& os) {
    
    auto set_font = [&](UByte font) {
        const FontDirectoryEntry* fde = get_font(font_dir, entity_trailer.font_set, font);
        std::string family((char*)&fde->family[1], fde->family[0]);
        std::string font_key;
        // ***** Must take into consideration face and rotation *****
        if (family == "TIMESROMAN") {
            font_key = "/Times";
            switch (fde->face) {
                case ItalicFace: font_key += "-Italic"; break;
                case BoldFace: font_key += "-Bold"; break;
                case BoldItalicFace: font_key += "-BoldItalic"; break;
                default: font_key += "-Roman"; break;
            }
        } else if (family == "HELVETICA") {
            font_key = "/Helvetica";
            switch (fde->face) {
                case ItalicFace: font_key += "-Oblique"; break;
                case BoldFace: font_key += "-Bold"; break;
                case BoldItalicFace: font_key += "-BoldOblique"; break;
                default: break;
            }
        } else if (family == "GACHA") {
            font_key = "/Courier";
            switch (fde->face) {
                case ItalicFace: font_key += "-Oblique"; break;
                case BoldFace: font_key += "-Bold"; break;
                case BoldItalicFace: font_key += "-BoldOblique"; break;
                default: break;
            }
        } else /* ... */
            font_key = "/Courier"; // ***** What is best default to catch problems ?????
        os << font_key << " findfont ";
        if (fde->size > 0)
            os << fde->size * 2540 / 72;    // size is points
        else
            os << -fde->size;               // size is -micas
        os << " scalefont setfont\n";
    };
    
    auto f_data = data_list.cbegin();
    auto l_data = data_list.cend();
    
    UDWord n_data; // must be set before read_byte, read_word, or read_float is called
    auto read_byte = [&]()->u_int8_t {
        assert(n_data != 0); u_int8_t b = *f_data; ++f_data; --n_data; return b;
    };
    auto read_word = [&]()->UWord {
        u_int8_t b = read_byte(); return (b << 8) | read_byte();
    };
    auto read_float = [&]()->float {
        SWord w_hi = read_word();
        UWord w_lo = read_word();
        SDWord s = (w_hi << 16) | w_lo;
         /*
         structure FP: [
             sign  bit 1     //1 if negative.
             expon bit 8    //excess 128 format (complemented if number <0)
             mantissa1 bit 7 //High order 7 bits of mantissa
             mantissa2 bit 16 //Low order 16 bits of mantissa
         ]
         */
        bool negative = s < 0;
        if (negative)
            s = -s;
        int exp = (s >> 23) & 0377;
        if (s != 0) exp -= 128; // ****** was: if (exp != 0)
        int fraction = s & 037777777; // High-order bit explicitly represented?
        float f = (fraction * 1.0f) / 0040000000;
        assert( f == 0.0f || 0.5f <= f && f < 1.0f);
        while (exp < 0) {f = f / 2.0f; ++exp;}
        while (exp > 0) {f = f * 2.0f; --exp;}
        if (negative) f = -f;
        return f;
    };
    
    auto show = [&] (int n) {
        os << "(";
        while (n != 0 && f_data != l_data) {
            char c = *f_data;
            if (Debug) std::cout << c;
             // Escape backslash and parentheses.
            if (c == '\\' || c == '(' || c == ')') os << '\\';
            // ***** Need to reflect the character encoding of the current font.
            os << c;
            ++f_data;
            --n;
        }
        os << ") show\n";
    };

    auto skip = [&](int n) {
        while (n != 0 && f_data != l_data) {
            if (Debug) std::cout << *f_data;
            ++f_data;
            --n;
        }
    };
    
    // Set entity defaults (Press File Format page 3)
    // brightness, saturation, and hue affect characters, objects, and rects, but not dots (Press File Format page 3)
    UByte brightness = 0;
    UByte saturation = 0377;
    UByte hue = 0; // ***** is this black ?????
    UByte font = 0; set_font(font);
    // reset_space
    UWord x, y;
    UByte only_on_copy = 0;
    
    while (is.tellg() < entity_list_limit) {
//        if (Debug) std::cout << "<" << std::oct << std::showbase << is.tellg() << ": ";
        int cmd = is.get();
//        if (Debug) std::cout << cmd << std::noshowbase << std::dec << ">";
        int short_cmd = cmd & 0340;
        if (short_cmd < 0200) {
            // Short-form command
            /*
             EShowShort = 000,     // 000nnnnn
             ESkipShort = 040,     // 001nnnnn
             EShowSkip = 0100,     // 010nnnnn
             ESpaceXShort = 0140,  // 01100xxx xxxxxxxx
             ESpaceYShort = 0150,  // 01101yyy yyyyyyyy
             EFont = 0160,         // 0111ffff
             */
            switch (short_cmd) {
                case EShowShort: {
                    int n = (cmd & 037) + 1;
                    if (Debug) std::cout << "<showshort> " << n << " (";
                    show(n);
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case ESkipShort: {
                    int n = (cmd & 037) + 1;
                    if (Debug) std::cout << "<skipshort> " << n << " (";
                    skip(n);
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case EShowSkip: {
                    int n = (cmd & 037) + 1;
                    if (Debug) std::cout << "<showskipshort> (";
                    show(n);
                    skip(1);
                    if (Debug) std::cout << " " << *f_data << ")\n";
                    break;
                }
                case ESpaceXShort: {
                    UWord high_bits;
                    UWord low_bits;
                    if ((cmd & 020) != 0) { // EFont
                        font = cmd & 017;
                        if (Debug) std::cout << "<font> " << (int)font << "\n";
                        set_font(font);
                    } else {
                        high_bits = cmd & 07;
                        low_bits = is.get();
                        if ((cmd & 010) != 0) { // ESpaceYShort
                            y = entity_trailer.Ye + (high_bits << 8) + low_bits;
                            if (Debug) std::cout << "<setspaceyshort> " << y << "\n";
                        } else { // ESpaceXShort
                            x = entity_trailer.Xe + (high_bits << 8) + low_bits;
                            if (Debug) std::cout << "<setspacexshort> " << x << "\n";
                        }
                    }
                    break;
                }
                default: {
                    throw raf_exception("Short-form command not yet implemented");
                }
            }
        } else if (0200 <= cmd && cmd < 0240) {
            // Available for application use
            if (Debug) std::cout << "<avail> " << cmd << "\n";
        } else if (0240 <= cmd && cmd < 0353) {
            // Spare - reserved for future Press use
            if (Debug) std::cout << "<spare> " << cmd << "\n";
        } else {
            switch (cmd) {
                case ESkipControlIm: {
                    int n = is.get();
                    if (Debug) std::cout << "<skipcontrolimmediate> " << n << " (";
                    // skip entity list bytes
                    while (n != 0) {
                        char c = is.get();
                        if (Debug) std::cout << c;
                        --n;
                    }
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case EOnlyOnCopy: throw raf_exception("Unimplemented entity command: onlyoncopy");
                case EAlternate: {
                    UWord EL_types = read_UWord(is);
                    UDWord EL_bytes = read_UDWord(is);
                    UDWord DL_bytes = read_UDWord(is);
                    std::size_t n = l_data - f_data;
                    if (DL_bytes > n) {
                        std::cout << "*** press_to_pdf(" << the_in_path
                                  << "): <alternate> command requires "
                                  << std::to_string(DL_bytes - n)
                                  << " bytes too much data; truncating\n";
                        DL_bytes = n;
                    }
                    is.seekg(EL_bytes, is.cur); // skip entity list bytes for now
                    f_data += DL_bytes; // skip data list bytes for now
                    if (Debug) std::cout << "<alternate> " << EL_types << " " << (int)EL_bytes << " " << (int)DL_bytes << "\n";
                    break;
                }
                case ESetX: {
                    SWord delta_x = read_UWord(is);
                    x = entity_trailer.Xe + delta_x;
                    if (Debug) std::cout << "<setx> " << x << "\n";
                    os << "currentpoint exch pop " << x << " exch moveto\n";
                    break;
                }
                case ESetY: {
                    SWord delta_y = read_UWord(is);
                    y = entity_trailer.Ye + delta_y;
                    if (Debug) std::cout << "<sety> " << y << "\n";
                    os << "currentpoint pop " << y << " moveto\n";
                    break;
                }
                case EShowChars: {
                    int n = is.get();
                    if (n > l_data - f_data)
                        throw raf_exception(("<show> command consumed " + std::to_string(n - (l_data - f_data)) + " bytes too much data").c_str());
                    if (Debug) std::cout << "<show> " << n << " (";
                    show(n);
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case ESkipChars: {
                    int n = is.get();
                    if (n > l_data - f_data)
                        throw raf_exception(("<skip> command consumed " + std::to_string(n - (l_data - f_data)) + " bytes too much data").c_str());
                    if (Debug) std::cout << "<skip> " << n << " (";
                    skip(n);
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case ESkipControlBytes: {
                    int n = read_UWord(is);
                    if (n > l_data - f_data) {
                        std::cout << "*** press_to_pdf(" << the_in_path
                                  << "): <skipcontrolbytes> command requires "
                                  << n - (l_data - f_data) << " bytes too much data; truncating\n";
                        n = l_data - f_data;
                    }
                    int type = is.get();
                    if (Debug) std::cout << "<skipcontrolbytes> " << n << " " << type << " (";
                    skip(n);
                    if (Debug) std::cout << ")\n";
                    break;
                }
                case EShowCharIm: {
                    char c = is.get();
                    if (Debug) std::cout << "<showcharacter immediate> " << c << "\n";
                    if (c == '\\' || c == '(' || c == ')') os << '\\';
                    os << c;
                    break;
                }
                case ESetSpaceX: {
                    UWord s = read_UWord(is);
                    if (Debug) std::cout << "<setspacex> " << s << "\n";
                    break;
                }
                case ESetSpaceY: {
                    UWord s = read_UWord(is);
                    if (Debug) std::cout << "<setspacey> " << s << "\n";
                    break;
                }
                case EResetSpace: {
                    if (Debug) std::cout << "<resetspace>\n";
                    break;
                }
                case ESpace: throw raf_exception("Unimplemented entity command: <space>");
                case ESetBright: {
                    UByte b = is.get();
                    if (Debug) std::cout << "<setbrightness> " << (int)b << "\n";
                    break;
                }
                case ESetHue: {
                    UByte h = is.get();
                    if (Debug) std::cout << "<sethue> " << (int)h << "\n";
                    break;
                }
                case ESetSat: {
                    UByte s = is.get();
                    if (Debug) std::cout << "<setsaturation> " << (int)s << "\n";
                    break;
                }
                case EShowObject: {
                    n_data = read_UWord(is) * sizeof(UWord);
                    assert(f_data + n_data <= l_data);
                    if (Debug) std::cout << "<showobject> " << n_data / sizeof(UWord) << "\n";
                    // ***** Set brightness, hue, and saturation
                    os << "gsave\n";
                    while (n_data != 0) {
                        int object_cmd = read_word();
                        switch (object_cmd) {
                            case EMoveTo: {
                                SWord delta_x = (SWord)read_word();
                                int x = entity_trailer.Xe + delta_x;
                                SWord delta_y = (SWord)read_word();
                                int y = entity_trailer.Ye + delta_y;
                                os << x << " " << y << " moveto\n";
                                if (Debug) std::cout << "  <<moveto>> " << x << " " << y << "\n";
                                break;
                            }
                            case EDrawTo: {
                                SWord delta_x = (SWord)read_word();
                                int x = entity_trailer.Xe + delta_x;
                                SWord delta_y = (SWord)read_word();
                                int y = entity_trailer.Ye + delta_y;
                                os << x << " " << y << " lineto\n";
                                if (Debug) std::cout << "  <<drawto>> " << x << " " << y << "\n";
                                break;
                            }
                            case EDrawCurve: {
                                float Cx = (SDWord)read_float();
                                float Cy = (SDWord)read_float();
                                float Bx = (SDWord)read_float();
                                float By = (SDWord)read_float();
                                float Ax = (SDWord)read_float();
                                float Ay = (SDWord)read_float();
                                /*
                                 currentpoint  % x0 y0
                                 dup           % x0 y0 y0
                                 Cy/3 add      % x0 y0 y1           y1 := y0 + Cy/3
                                 dup           % x0 y0 y1 y1
                                 (Cy+By)/3 add % x0 y0 y1 y2        y2 := y1 + (Cy+By)/3
                                 3 2 roll      % x0 y1 y2 y0
                                 Cy+By+Ay add  % x0 y1 y2 y3        y3 := y0 + Cy+By+Ay
                                 4 3 roll      % y1 y2 y3 x0
                                 dup           % y1 y2 y3 x0 x0
                                 Cx/3 add      % y1 y2 y3 x0 x1     x1 := x0 + Cx/3
                                 dup           % y1 y2 y3 x0 x1 x1
                                 (Cx+Bx)/3 add % y1 y2 y3 x0 x1 x2  x2 := x1 + (Cx+Bx)/3
                                 3 2 roll      % y1 y2 y3 x1 x2 x0
                                 Cx+Bx+Ax add  % y1 y2 y3 x1 x2 x3  x3 := x0 + Cx+Bx+Ax
                                 6 3 roll      % x1 x2 x3 y1 y2 y3
                                 5 3 roll      % x1 y1 y2 y3 x2 x3
                                 4 2 roll      % x1 y1 x2 x3 y2 y3
                                 3 2 roll      % x1 y1 x2 y2 y3 x3
                                 exch          % x1 y1 x2 y2 x3 y3
                                 curveto       % -
                                 */
                                os << "currentpoint "
                                      "dup " << Cy/3 << " add dup " << (Cy+By)/3 << " add "
                                      "3 2 roll " << Cy+By+Ay << " add 4 3 roll "
                                      "dup " << Cx/3 << " add dup " << (Cx+Bx)/3 << " add "
                                      "3 2 roll " << Cx+Bx+Ax << " add 6 3 roll "
                                      "5 3 roll 4 2 roll 3 2 roll exch "
                                      "curveto\n";
                                if (Debug) std::cout << "  <<drawcurve>> "
                                    << Cx << " " << Cy << " "
                                    << Bx << " " << By << " "
                                    << Ax << " " << Ay << "\n";
                                break;
                            }
                            default: throw raf_exception(
                                        ("Unknown object command" + std::to_string(object_cmd)).c_str());
                        }
                    }
                    os << brightness / 255.0 << " setgray fill grestore\n";
                    break;
                }
                case EShowDots:
                case EShowDotsOpaque: {
                    n_data = read_UDWord(is) * sizeof(UWord);
                    assert(f_data + n_data <= l_data);
                    if (Debug)
                        std::cout << "<" << (cmd == EShowDots ? "showdots" : "showdotsopaque")
                                  << "> " << n_data / sizeof(UWord) << "\n";
                    int code, bits_per_sample,
                        dots, lines, pd = -1, dd = -1, pl = -1, dl = -1,
                        dot_direction, line_direction,
                        width = -1, height = -1,
                        AIS_scale_factor = 16; // ***** hack
                    while (n_data != 0) {
                        int dot_cmd = read_byte();
                        switch (dot_cmd) {
                            case EDotSetCoding: {
                                code = read_byte();
                                if (code == 0) bits_per_sample = 1;
                                else           bits_per_sample = code;
                                dots = read_word();
                                lines = read_word();
                                if (Debug) std::cout << "  <<setcoding>> " << code << " " << dots << " " << lines << "\n";
                                break;
                            }
                            case EDotSetMode: {
                                int direction = read_byte();
                                dot_direction = (direction >> 4) & 017;
                                line_direction = direction & 017;
                                if (Debug) std::cout << "  <<setmode>> " << direction << "\n";
                                break;
                            }
                            case EDotTwoByteCommand: {// two-byte command
                                dot_cmd = (dot_cmd<<8) | read_byte();
                                switch (dot_cmd) {
                                    case EDotSetWindow:
                                        pd = read_word(); dd = read_word();
                                        pl = read_word(); dl = read_word();
                                        if (Debug) std::cout << "  <<set window>> "
                                                             << pd << " " << dd << "  "
                                                             << pl << " " << dl << "\n";
                                        break;
                                    case EDotSetSize:
                                        width = read_word(); height = read_word();
                                        if (Debug) std::cout << "  <<setsize>> " << width << " " << height << "\n";
                                        break;
                                    case EDotDotsFollow: {
                                        // ***** Must account for EDotSetWindow *****
                                        os << "gsave\n";
                                        os << "{0.5 exp} settransfer\n"; // gamma correction
                                        os << "currentpoint translate\n";
                                        if (code == 0)
                                            os << width << " " << height << " scale\n";
                                        else {
                                            os << AIS_scale_factor << " " << dots << " mul "
                                               << AIS_scale_factor << " " << lines << " mul scale\n";
                                        }
                                        os << dots << " " << lines << " ";
                                        if (bits_per_sample == 1)
                                            os << (cmd == EShowDotsOpaque ? "1" : "true");
                                        else {
                                            os << bits_per_sample;
                                        }
                                        os << " [";
                                        assert(dot_direction == 0);
                                        switch (line_direction) {
                                            //case 0:
                                            //case 1:
                                            //case 2: os << dots << " 0 0 " << lines << " 0 0"; break;
                                            case 3: os << dots << " 0 0 " << -lines << " 0 " << lines; break;
                                            default: throw raf_exception(("ShowDots: unknown direction: " + std::to_string(line_direction)).c_str());
                                        }
                                        os << "]\n";
                                        os << "currentfile\n/ASCIIHexDecode filter\n";
                                        // If bits_per_sample == 1 && code == 0, then:
                                        //      1 => black pixel, 0 => white pixel (DotsOpaque) or transparent (Dots)
                                        // but bits_per_sample == 1 && code == 1, then:
                                        //      1 => white pixel, 0 => black pixel (Dots Opaque) or transparent (Dots)
                                        os << ((cmd == EShowDotsOpaque || bits_per_sample != 1) ?
                                                "image" : "imagemask") << "\n";
                                        bytes_to_hex(os, dots, lines, bits_per_sample, read_byte);
                                        os << ">\ngrestore\n";
                                        if (Debug) std::cout << "  <<dotsfollow>> ...\n";
                                        break;
                                    }
                                    case EDotGetDotsFromFile:
                                        raf_exception("Unimplemented dot command: <<get dots from file>>");
                                    case EDotGetDotsFromPressFile:
                                        raf_exception("Unimplemented dot command: <<get dots from press file>>");
                                    case EDotSetSamplingProperties:
                                        raf_exception("Unimplemented dot command: <<set sampling properties>>");
                                }
                            }
                            default: raf_exception(("Unknown dot command" + std::to_string(dot_cmd)).c_str());
                        }
                    }
                    break;
                }
                case EShowRect: {
                    UWord w = read_UWord(is);
                    UWord h = read_UWord(is);
                    if (Debug) std::cout << "<rect> " << x << " " << y << " " << w << " " << h << "\n";
                    // ***** Set brightness, hue, and saturation
                    os << "currentpoint gsave newpath\n"
                       << x     << " " << y     << " moveto\n"
                       << x + w << " " << y     << " lineto\n"
                       << x + w << " " << y + h << " lineto\n"
                       << x     << " " << y + h << " lineto\n"
                       << "closepath fill grestore moveto\n";
                    break;
                }
                case ENop: {
                    if (Debug) std::cout << "<nop>" << std::endl;
                    break;
                }
                default: {
                    throw raf_exception("Unimplemented entity command");
                }
            }
        }
    }
    if ((f_data > l_data) || (l_data - f_data > 3))
        std::cout << "*** press_to_pdf(" + the_in_path << ") Entity left "
                  << l_data - f_data << " data bytes unused\n";
}

void read_page(std::istream& is, const FontDirectory& font_dir, const PartDirectoryEntry& part_dir_ent,
               std::ostream& os) {
    std::size_t offset = records_to_bytes(part_dir_ent.part_first); // origin for all DL and EL file positions
    std::size_t size = records_to_bytes(part_dir_ent.part_count);
    
    // Last entity is followed by entity list padding
    std::size_t entity_limit = size - part_dir_ent.EL_padding_size * sizeof(UWord);
    
    // Length field is last field of EntityTrailer, which comes at the end of the entity
    std::size_t entity_trailer_length_offset = entity_limit - sizeof(UWord);
    is.seekg(offset + entity_trailer_length_offset, is.beg);
    UWord length = read_UWord(is);
    if (length == 0) throw raf_exception("*** Expected printed page to contain at least one entity");
    
    // Generate page prolog
    os <<
        "save\n"
        "72 2540 div dup scale\n" // Change units from points to micas
        "0 0 moveto\n"; // establish current point
    
    // Chain back to first entity, flagged by a word of zero where the length would be.
    // We must save the lengths in order to iterate forward through the entities.
    std::vector<UWord> entity_lengths;
    do {
        entity_lengths.push_back(length);
        entity_trailer_length_offset -= length * sizeof(UWord);
        is.seekg(offset + entity_trailer_length_offset, is.beg);
        length = read_UWord(is);
    } while (length != 0);
    
    // First entity begins just after zero word separating the data list and entity list.
    std::size_t entity_offset = entity_trailer_length_offset + sizeof(UWord);
    
    // Iterate forward through the entities.
    EntityTrailer entity_trailer;
    std::vector<char> data_list;
    do {
        length = entity_lengths.back();
        entity_lengths.pop_back();
        entity_limit = entity_offset + length * sizeof(UWord);
        std::size_t entity_trailer_offset = entity_limit - sizeof(EntityTrailer) + 4; // ***** 4 is kludge fudge factor *****
        is.seekg(offset + entity_trailer_offset, is.beg);
        read_entity_trailer(is, entity_trailer);
        if (Debug) {
            std::cout << "{entity fontset: " << (int)entity_trailer.font_set
                      << " origin XY: " << entity_trailer.Xe << " " << entity_trailer.Ye
                      << " bbox LBWH: " << entity_trailer.x_left << " " << entity_trailer.y_bottom
                      << " " << entity_trailer.width << " " << entity_trailer.height;
            // Show the parameters for this entity
            std::cout << std::endl << std::oct << std::showbase
                      << "  file [" << offset << ", " << offset + size << ")\n"
                      << "  DL [" << entity_trailer.begin_byte << ", " << entity_trailer.byte_length << ")\n"
                      << "  EL [" << entity_offset << ", " << entity_trailer_offset << ")\n"
                      << "  length " << length << std::noshowbase << std::dec;
            std::cout << "}" << std::endl;
        }
        
        data_list.resize(entity_trailer.byte_length);
        is.seekg(offset + entity_trailer.begin_byte, is.beg);
        is.read(&data_list[0], entity_trailer.byte_length);
        is.seekg(offset + entity_offset, is.beg);
        // Entity list is [entity_offset, entity_trailer_offset)
        read_entity(is, font_dir, offset + entity_trailer_offset, data_list, entity_trailer, os);
        entity_offset += entity_trailer.length * sizeof(UWord);
    } while (!entity_lengths.empty());
    if (Debug) std::cout << "{show page}" << std::endl;
    
    // Generate page trailer
    os <<
        "showpage\n"
        "restore\n";
    os.flush(); // for debugging
}

void read_pages(std::istream& is, const PartDirectory& part_dir, const FontDirectory& font_dir,
                std::ostream& os) {
    auto f = part_dir.cbegin();
    auto l = part_dir.cend();
    while (f != l) {
        if (f->part_type == PartTypePrintedPage) read_page(is, font_dir, *f, os);
        ++f;
    }
}

bool is_press_file(const std::string& in_path) {
    std::ifstream is(in_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** Could not open " << in_path << std::endl;
        std::exit(1);
    }
    DocumentDirectory doc_dir;
    // Get length of file.
    is.seekg(0, is.end);
    std::fstream::streamoff length = is.tellg();
    // Press file length must be multiple of 512 bytes.
    if (length % 512 != 0) return false;
    // Read document directory
    is.seekg(-sizeof(DocumentDirectory), is.end);
    doc_dir.password = read_UWord(is);
    // Press file document directory begins with password.
    if (doc_dir.password != Presspassword) return false;
    return true;
}

void press_to_pdf(const std::string& alto_file_name,
                 const std::string& in_path,
                 const std::string& out_path, bool ps) {
    the_in_path = in_path;
    std::ifstream is(in_path, std::ifstream::in | std::ifstream::binary);
    if (!is) {
        std::cout << "*** press_to_pdf: Could not open " << in_path << std::endl;
        std::exit(1);
    }
    redi::opstream os_pdf;
                           ("/usr/local/bin/ps2pdf - " + out_path); // ***** DELETE THIS *****
    std::ofstream os_ps;
    if (ps) {
        os_ps.open(out_path, std::fstream::trunc);
        if (!os_ps) {
            std::cout << "*** press_to_ps: Could not open " << out_path << std::endl;
            std::exit(1);
        }
    } else {
        os_pdf.open("/usr/local/bin/ps2pdf - " + out_path);
        if (!os_pdf) {
            std::cout << "*** press_to_pdf: Could not open " << out_path << std::endl;
            std::exit(1);
        }
    }
    if (Debug) std::cout << "*** press_to_pdf(" << in_path << ")\n";
    
    // Read directories
    DocumentDirectory doc_dir;
    PartDirectory part_dir;
    FontDirectory font_dir;
    
    read_doc_dir(is, doc_dir);
    read_part_dir(is, doc_dir, part_dir);
    read_font_dir(is, part_dir, font_dir); if (Debug) print_font_dir(font_dir);
    
    // Output PostScript prologue
    if (ps) generate_prologue(alto_file_name, doc_dir, part_dir, font_dir, os_ps);
    else    generate_prologue(alto_file_name, doc_dir, part_dir, font_dir, os_pdf);
    
    // Read printed page parts
    if (ps) read_pages(is, part_dir, font_dir, os_ps);
    else    read_pages(is, part_dir, font_dir, os_pdf);
    
    // ***** Output PostScript trailer ?????
}


