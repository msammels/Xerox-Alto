//
//  press2ps.h
//  restore_alto_files
//
//  Created by Paul McJones on 1/15/14.
//  Copyright (c) 2014 Paul McJones. All rights reserved.
//

#ifndef restore_alto_files_press2ps_h
#define restore_alto_files_press2ps_h

#include <string>

bool is_press_file(const std::string& in_path);

void press_to_pdf(const std::string& alto_file_name, // only for PostScript prologue
                  const std::string& in_path,
                  const std::string& out_path,
                  bool ps = false);

#endif
