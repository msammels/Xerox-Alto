//
//  Press.h
//  restore_alto_files
//
//  Created by Paul McJones on 10/21/13.
//  Copyright (c) 2013 Paul McJones. All rights reserved.
//
// Based on:
// 1. [Indigo]<PrintingDocs>PRESSFORMAT.PRESS!1 (of December 1979).
// 2. press.dfs in [Indigo]<AltoSource>PRESSEDITSOURCES.DM!4, with these comments:

    // Copyright Xerox Corporation 1979, 1980

    // Standard PRESS definitions
    // William Newman 4-24-75
    // last edited June 14, 1978  11:48 AM
    // added date1, date2 to DDV

#ifndef restore_alto_files_Press_h
#define restore_alto_files_Press_h

// The file format is based on the Alto's architecture, with sixteen-bit big-endian word addresses.

typedef uint8_t     UByte;
typedef uint16_t    UWord;
typedef int16_t     SWord;
typedef uint32_t    UDWord;
typedef int32_t     SDWord;

// A Press file consists of:
//   a sequence of printed page parts,
//   its font directory part,
//   its part directory,
//   and finally its document directory.
// Each part, part directory, and document directory must occupy an integral number of 256-word (512-byte) records.
// A printed page part consists of:
//   a data list, followed by
//   an entity list.
// An entity list consists of:
//   a Word of zero,
//   a sequence of entities (typically a one-byte code followed by 0 or more bytes of arguments.

const UWord Presspassword = 27183; // first word of document directory

const std::size_t WordsPerRecord = 256; // words are two bytes, so record is 512 bytes

// Structures

// Document directory (DDV in press.dfs)

inline
std::size_t records_to_bytes(UWord records) {
    return records * sizeof(UWord) * WordsPerRecord;
}

typedef struct {
    UWord   password;
    UWord   rec_count;  // total number of records in this file
    UWord   part_count; // total number of parts, including the font directory part
    UWord   pd_first;   // record number of start of part directory
    UWord   pd_count;   // number of records in the part directory
    UWord   back_pointer;   // back pointer to obsolete document directory -- record number ?????
    UDWord  date;       // not mentioned in 1979 "Press File Format" manual -- obsolete?
    UWord   first_copy; // starting copy number to print
    UWord   last_copy;  // ending copy number to print
    UByte   unused1[236];
    UByte   file_name[52];
    UByte   creator_name[32];
    UByte   creation_date[40];
    UByte   unused2[132];
} DocumentDirectory; // sizeof(DocumentDirectory) = WordsPerRecord*sizeof(UWord)

typedef struct {
    SWord   part_type;  // 0 is printed page; 1 is font directory part
    UWord   part_first; // record number of start of this part
    UWord   part_count; // number of records in this part
    UWord   EL_padding_size;    // length in Words of entity list padding at end of last record of printed page
} PartDirectoryEntry;

const SWord PartTypePrintedPage = 0;
const SWord PartTypeFontDir     = 1;

typedef struct {
    UWord   length;     // length in words of this entry
    UByte   font_set;
    UByte   font;
    UByte   m;          // first character being defined
    UByte   n;          // last character being defined
    UByte   family[20]; // file format: upper-case Bcpl string of family (e.g., METEOR)
    UByte   face;       // as returned by EncodeFace
    UByte   source;     // normally equal to m
    SWord   size;       // points if positive, micas if negative (72 points = 2540 micas)
    UWord   rotation;   // minutes of arc, anticlockwise (landscape is 90*60)
} FontDirectoryEntry;

const UByte NormalFace = 0;
const UByte ItalicFace = 1;
const UByte BoldFace = 2;
const UByte BoldItalicFace = 3;
const UByte LightFace = 4;
const UByte LightItalicFace = 5;
const UByte CondensedFace = 6;
const UByte CondensedItalicFace = 7;
// ...

typedef struct {
    UByte   entity_type;
    UByte   font_set;
    UDWord  begin_byte; // byte address of data (big-endian)
    UDWord  byte_length;// byte length of data (big-endian)
    SWord   Xe;         // entity origin X (micas relative to lower-left page corner)
    SWord   Ye;         // entity origin Y (micas relative to lower-left page corner)
                        // Press File Format (page 3) says Xe and Ye are signed
    UWord   x_left;     // entity bounding box left edge (relative to Xe)
    UWord   y_bottom;   // entity bounding box bottom edge (relative to Ye)
    UWord   width;      // entity bounding box width
    UWord   height;     // entity bounding box height
    UWord   length;     // length in Words of this entity, including the trailer
} EntityTrailer;

// Entity command codes

typedef enum {
    EShowShort = 000,     // 000nnnnn
    ESkipShort = 040,     // 001nnnnn
    EShowSkip = 0100,     // 010nnnnn
    ESpaceXShort = 0140,  // 01100xxx xxxxxxxx
    ESpaceYShort = 0150,  // 01101yyy yyyyyyyy
    EFont = 0160,         // 0111ffff
    
    EAvail = 0200,        // 10000000
    ESpare = 0240,        // 10100000
    
    ESkipControlIm = 0353,// 11101011
    EAlternate = 0354,
    EOnlyOnCopy=0355,
    ESetX=0356,
    ESetY=0357,
    EShowChars=0360,
    ESkipChars=0361,
    ESkipControlBytes=0362,
    EShowCharIm=0363,
    ESetSpaceX=0364,
    ESetSpaceY=0365,
    EResetSpace=0366,
    ESpace=0367,
    ESetBright=0370,
    ESetHue=0371,
    ESetSat=0372,
    EShowObject=0373,
    EShowDots=0374,
    EShowDotsOpaque=0375,
    EShowRect=0376,
    ENop=0377,
} EC;

typedef enum {
    EMoveTo = 0,
    EDrawTo = 1,
    EDrawCurve = 2,
} EObjectC;

typedef struct {
    uint16_t mantissa2: 16;
    uint8_t mantissa1: 7;
    uint8_t expon: 8;
    uint8_t sign: 1;
} Float;

/* See:
   William M. Newman and Robert F. Sproull. Pico Manual. Internal Memo, Xerox Palo Alto Research Center, July 1974.
   http://www.bitsavers.org/pdf/xerox/parc/memos/Pico_Manual_Jul74.pdf
*/
/*
structure FP: [
               sign  bit 1     //1 if negative.
               expon bit 8    //excess 128 format (complemented if number <0)
               mantissa1 bit 7 //High order 7 bits of mantissa
               mantissa2 bit 16 //Low order 16 bits of mantissa
               ]
*/

typedef enum {
    // One-byte commands
    EDotTwoByteCommand = 0,
    EDotSetCoding = 1,
    EDotSetMode = 2,
    // Two-byte commands
    EDotSetWindow = 1,
    EDotSetSize = 2,
    EDotDotsFollow = 3,
    EDotGetDotsFromFile = 4,
    EDotGetDotsFromPressFile = 5,
    EDotSetSamplingProperties = 6
} EDotC;

#endif
