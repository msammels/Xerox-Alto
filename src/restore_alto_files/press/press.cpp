
// press.cpp
// restore_alto_files
//
// Created by Paul McJones on 1/15/14.
// Copyright (c) 2014 Paul McJones. All rights reserved.


#include "press2pdf.h"
#include "Press.h"
#include <assert.h>
#include <iostream>

void usage() {
    std::cout << "Usage: press2pdf --in=<path to Press file> --out=<path for converted PDF file>" << std::endl;
    std::exit(1);
}

int main(int argc, const char * argv[])
{
    std::cout << "press2pdf of 2017/07/24" << std::endl;
    assert(sizeof(DocumentDirectory) == WordsPerRecord * sizeof(UWord));

    std::string alto_file_path("<alto_file_name>");
    std::string in_path;
    std::string out_path;
    bool ps = false;

    for (std::size_t i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        if (arg.compare(0, 5, "--in=") == 0) {
            in_path = arg.substr(5, std::string::npos);
        } else if (arg.compare(0, 6, "--out=") == 0) {
            out_path = arg.substr(6, std::string::npos);
        } else {
            std::cout << "Unknown option: " << arg << std::endl;
            usage();
        }
    }

    if (in_path.empty() || out_path.empty()) {
        std::cout << "*** Must specify in_path and out_path" << std::endl;
        usage();
    }

    press_to_pdf(alto_file_path, in_path, out_path, ps);
    return 0;
}
