#!/usr/bin/env python

# Press2Ps -- convert Press files (limited to NS2 usage) to Postscript
# press2ps.py d   -- converts chapter d to chapter d.ps

# Postscript file will use micas as units, so "72 2540 div 72 2540 div scale"

# When debugging Postscript, it's easiest to use Ghostscript:
# /usr/local/bin/gs <foo.ps>
# If in encounters an error, it will give the character position in the file where it occurs

import array,math
import sys

# print with AltoFormatter to solve Py2/Py3 issues
import HeraldFormatter
fm = HeraldFormatter.HeraldFormatter(False)

impose_widths = True

def array_make(len, typecode='H'):
    """Convenience routine for making array.array's.

    Useful for holding 16-bit quantities because a store into the array will
    check to be sure the value being stored is only 16 bits long, and raise
    an exception if not.
    """
    a = array.array(typecode)
    for i in range(len):
        a.append(0)
    return a

def get_byte(arry, wd_index, bt_index):
    """ Return byte from array, indexed by 2*wd_index + bt_index """
    idx = wd_index + bt_index//2
    if bt_index % 2 == 0:
        return arry[idx] >> 8
    else:
        return arry[idx] & 0o377

def get_press_byte(idx):
    w = press_file[idx//2]
    if (idx & 1) == 0:
        return w >> 8
    else:
        return w & 0o377

def get_press_word(idx):
    # return a word, from a byte index
    if (idx & 1) != 0: raise Exception("get_press_word called with odd index")
    return press_file[idx//2]


def get_bcpl_string(arry, idx):
    """ Assemble string starting at arry index idx """
    s = ""
    len = arry[idx] >> 8
    for i in range(len):
        s += chr(get_byte(arry,idx,i+1))
    return s

def signed_add(a, b=0):
    """ Sign extend a and b, add them, and return """
    
    # Can't sign extend Python ints by just or'ing in high bits,
    # because the int will grow to a long to keep it positive
    # Check to be sure there are no high bits in either operand
    if (a & 0xffff0000) != 0 or (b & 0xffff0000) != 0:
        raise Exception("Signed_add operand out of range.")
    if (a & 0o100000) != 0: a = a - 0o200000
    if (b & 0o100000) != 0: b = b - 0o200000
    return a+b

def escape_PS_string(s, recode=False):
    """ Escape ( ) \ in a string to send to PS

    If "recode," recode for ligatures, left quote, ...
    """
    re_code = fonts[(fsf*256)+font]['re-code']
    if re_code['type'] == 'math':
        fm.format("MATH recode {0} |{1}|", fonts[(fsf*256)+font]['name'], s)
    re_code_needed = False
    for c in s:
        if c in re_code: re_code_needed = True
    if re_code_needed:
        # Need to be sure we don't re-code the re-codes
        snew = ""
        for c in s:
            snew += c if c not in re_code else re_code[c]
        s = snew
    elif re_code['type'] == 'math':
        fm.format("No recoding for |{0}| {1}", s, re_code)
        sys.stdin.readline()
    # Following are to achieve necessary PS escapes
    if '(' in s or ')' in s or '\\' in s:
        s = s.replace('\\', '\\\\')
        s = s.replace('(', '\\(')
        s = s.replace(')', '\\)')
    return s


def read_binary_file(fn, swap_binary_bytes=False):
    """ Read entire binary file, return array containing it."""

    try:
        with open(fn,"rb") as f:
            s = bytearray(f.read())
            f.close()
        if len(s) % 2 == 1:
            raise Exception("Binary file requires even number of bytes")
        if swap_binary_bytes:
            # loop to swap bytes
            t = s
            s = ""
            for i in range(0, len(t), 2):
                s += t[i+1]
                s += t[i]
        wc = len(s) //2    # word count
        w = array_make(wc)
        for i in range(wc):
            ww = (s[2*i] << 8) + s[2*i+1]
            w[i] = ww
        return w
    except Exception, e:
        fm.format("Exception reading file {0} {1}", fn, e)
        sys.exit(1)  # if invoked with python -i, interpreter will catch this
    return None


press_file = None
widths_file = None

def read_font_widths():
    """ Read entire fonts.widths file into array widths_file (16-bit words) """
    global widths_file
    fn = "ns2/fonts.widths"
    widths_file = read_binary_file(fn)

def read_press_file(fn = "ns2/chapter01.press", swap_binary_bytes=False):
    """ Read entire press file into array press_file """
    global press_file
    press_file = read_binary_file(fn, swap_binary_bytes)

def get_part_directory():
    """ Return [index, # parts] to address press_file[] to find part directory """
    pl = len(press_file)
    if pl % 256 != 0: raise Exception("Press file not multiple of 256 words long")
    if press_file[pl-256] != 27183: raise Exception("Press file password wrong")
    return [ press_file[pl-256+3]*256, press_file[pl-256+2] ]

def get_font_directory():
    """ Return [index, len] to address press_file to find font directory """
    fd = get_part_directory()
    for i in range(fd[1]):
        idx = fd[0]+4*i
        w0 = press_file[idx]
        if w0 == 1:  # font directory
            return [ press_file[idx+1]*256, press_file[idx+2]*256 ]
    raise Exception("Cannot find font directory")

def get_page(n):
    """ Return [ index, len, EL_padding, entity_count ] for page n (1...) of Press file, or None

    The 'entity_count' value lets you find the nth entity in forward sequence by
    counting backwards.
    """
    fd = get_part_directory()
    for i in range(fd[1]):  # for all parts
        pi = fd[0]+i*4   # index of first word of part entry
        if press_file[pi] == 0:   # page part
            n -= 1
            if n == 0:   # found the page
                part_start = press_file[pi+1]*256
                part_len = press_file[pi+2]*256
                el_padding = press_file[pi+3]
                entity_count = 0
                e_trailer = part_start + part_len - el_padding - 1
                while press_file[e_trailer] != 0:
                    entity_count += 1
                    e_trailer -= press_file[e_trailer]
                return [ part_start, part_len, el_padding, entity_count ]
    return None

def get_font_widths(fam_name, face, siz):
    """ Find widths for a given font family name and face code.
    Returns dictionary with key=char code, val=width as specified in the file.
    if width == 0o100000, the character is missing
        else the width is in micas for a font of "siz" micas

    Format defined in ix.dfs, fontwidths.bcpl, listings with PrePress sources.
    There is also information in the "Press File Format" memo
    Face codes:
        1  italic
        2  bold
        4  light
        6  condensed
        12 expanded
    """
    idx = 0
    fam_name = fam_name.upper() # all names in fonts.widths are upper case
    fam_code = -1
    while True:
        ixh = widths_file[idx]
        typ = ixh >> 12
        ln = ixh & 0o7777
        if typ == 0: break   # end
        if typ == 1:         # name
            nam = get_bcpl_string(widths_file, idx+2)
            if nam == fam_name:
                fam_code = widths_file[idx+1]
            #print "Name: ",nam,widths_file[idx+1]
        if typ == 2:
            fm.format("fonts.widths: splines -- ignored")
        if typ == 3:
            fm.format("fonts.widths: chars -- ignored")
        if typ == 4:  # Widths
            e_fam = widths_file[idx+1] >> 8
            e_face = widths_file[idx+1] & 0o377
            e_siz = widths_file[idx+3]
            e_rot = widths_file[idx+4]
            if e_fam == fam_code and e_face == face and e_siz == 0 and e_rot == 0:

                bc = widths_file[idx+2] >> 8
                ec = widths_file[idx+2] & 0o377
                wsa = (widths_file[idx+5] << 16) + widths_file[idx+6]
                wlen = (widths_file[idx+7] << 16) + widths_file[idx+8]

                # Width data begins with WTB: 4 words of bounding box, 1 of fixed width indicators
                x_width_fixed = (widths_file[wsa+4] & 0o100000) != 0
                y_width_fixed = (widths_file[wsa+4] & 0o040000) != 0
                w = {}
                cidx = wsa+5
                for cc in range(bc, ec+1):
                    wd = widths_file[cidx]
                    if wd != 0o100000:
                        wd = (wd * siz) / 1000
                    if not x_width_fixed:
                        cidx += 1
                    w[cc] = wd
                if not y_width_fixed or widths_file[cidx] != 0:
                    raise Exception("Expect y widths to be zero")
                return w
        idx += ln
    return None

# key is fsf = (font_set * 256) + font
# value is { psfn:, name:, face:, siz:, widths: array of widths, scaled to micas }
# psfn is a Postscript font number, one for all Press fsf's that are the same

fonts = {}

def font_add(fsf, nam, face, siz):
    global fonts
    for ff in fonts:
        f = fonts[ff]
        if f['name'] == nam and f['face'] == face and f['siz'] == siz:
            # already there, just index it under the new fsf
            fonts[fsf] = f
            return
    # not in fonts yet, so add it
    # find max psfn used so far
    psfn_max = -1
    for ff in fonts:
        psfn_max = max(psfn_max, fonts[ff]['psfn'])
    wid = get_font_widths(nam, face, siz)
    fonts[fsf] = { 'psfn':psfn_max+1, 'name':nam, 'face':face, 'siz':siz, 'widths':wid }

def font_setup():
    """ Read font directory and obtain widths for each font being used """
    global fonts
    fd = get_font_directory()
    idx = fd[0]
    while True:
        el = press_file[idx]
        if el == 0: break
        fsf = press_file[idx+1]  # font set ,, font
        m = get_byte(press_file, idx+2, 0)
        n = get_byte(press_file, idx+2, 1)
        if n == 0o377 and press_file[idx+3] == 0:
            raise Exception("Fonts shapes in Press file not supported.")
        nam = get_bcpl_string(press_file, idx+3)
        face = get_byte(press_file, idx+13, 0)
        src = get_byte(press_file, idx+13, 1)
        siz = press_file[idx+14]
        rot = press_file[idx+15]
        if rot != 0:
            raise Exception("Font rotation non-zero")
        if siz > 0:
            # measured in points
            siz = 2540*siz/72  # convert to micas
        else:
            # measured in micas
            siz = -siz
        # Add this font to fonts if it's not already there
        font_add(fsf, nam, face, siz)
        idx += el

# The Symbol font requires considerabelr re-coding; the others only minor
font_name_translate = { 'TIMESROMAN':'Times-Roman', 'TIMESROMAN-BOLD':'Times-Bold', 'TIMESROMAN-ITALIC':'Times-Italic', 'TIMESROMAN-BOLD-ITALIC':'Times-BoldItalic',
                        'HELVETICA':'Helvetica', 'HELVETICA-BOLD':'Helvetica-Bold', 'HELVETICA-ITALIC':'Helvetica-Oblique', 'HELVETICA-BOLD-ITALIC':'Helvetica-BoldOblique',
                        'MATH':'Symbol', 'HIPPO':'Symbol', 'SIGMA':'Symbol' }

# ^Q is ffi, ^R is ffl, ^T is fi, ^U is fl, ^F is ff, ^G is left quote, ^K is acute, ^Y is en space, ^N is en-dash, ^S is em-dash
re_code_standard = { 'type':'standard', '\021':'f\256', '\022':'f\257', '\024':'\256', '\025':'\257', '\006':'ff', '\007':'\140', '\013':'\302', '\031':' ', '\016':'\261', '\023':'\320' }
# X is multiply, ^D is thin space, ^B is <, ^C is >, < is <=, > is >=, + is +, ( is prime, o is degree, # is infinity,
#         = +-, 4 is circleplus, V is or, & is and, n is logical not. g is =>, _ is ->, 9 is bullet, { is 1/4, | is 1/2
re_code_math = { 'type':'math', 'X':'\264', '\004':' ', '\002':'<', '\003':'>', '"':'\260', '<':'\243', '>':'\263', '+':'+', '(':'\242', 'o':'\260', '#':'\245', '=':'\261',
                 '4':'\305', 'V':'\332', '&':'\331', 'n':'\330', 'g':'\336', '_':'\256', '9':'\267', '{':"1/4", '|':"1/2" }
# D is delta
re_code_hippo = { 'type':'math', ' ':' ', 'D':'D', 'a':'a', 'b':'b', 'd':'d', 'e':'e', 'f':'f', 'g':'g', 'p':'p', 'q':'q', 'S':'S' }
# MD is sigma
re_code_sigma = { 'type':'math', 'M':'\345', '(':'(', ')':')' }


def PS_font_define(fs):
    """ Write out Postscript on file f to define all needed fonts """
    # find max psfn
    psfn_max = -1
    ff = None
    for f in fonts:
        psfn_max = max(psfn_max, fonts[f]['psfn'])
    # Now make a PS array:  Press_fonts, indexed by "psfn"
    fs.write("/PressFonts "+str(psfn_max+1)+" array def\n")
    for i in range(psfn_max+1):
        for f in fonts:
            ff = fonts[f]
            if ff['psfn'] == i: break
        # ff is a font dictionary corresponding to psfn
        # make up a unique name based on name[-bold]-[italic]
        un = ff['name']
        fac = ff['face']
        if (fac & 2) != 0: un += "-BOLD"
        if (fac & 1) != 0: un += "-ITALIC"
        if not un in font_name_translate:
            fm.format("Press font {0} has no PS equivalent", un)
            continue
        ps_font_name = font_name_translate[un]
        fs.write("PressFonts "+str(i)+" ")
        fs.write("/"+ps_font_name+" findfont "+str(ff['siz'])+" scalefont ")
        fs.write("put\n")
        # add re-code property to font record
        ff['re-code'] = re_code_standard
        if un == 'MATH': ff['re-code'] = re_code_math
        if un == 'HIPPO': ff['re-code'] = re_code_hippo
        if un == 'SIGMA': ff['re-code'] = re_code_sigma
        # now write a widths array for this postscript font
        if False:   # no longer put widths in PS file
            fs.write("PressWidths "+str(i)+" [ ")
            w = ff['widths']
            for ci in range(256):
                if ci in w:
                    wv = w[ci]
                    if wv & 0o100000 != 0:
                        fs.write("0 ")
                    else:
                        fs.write(str(wv)+" ")
                else:
                    fs.write("0 ")
            fs.write(" ] put\n")

###########################################################################
# Below here, stuff for dealing within a page

PS_f = None

def PS_start(fn):
    global PS_f
    PS_f = open(fn, "w")
    PS_f.write("%!PS-Adobe-1.0\n")  # a lie for now

def PS_end():
    PS_f.close()

# write out page n to postscript

# a whole bunch of global state
global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
el_idx = 0
el_end = 0
dl_idx = 0
x,y = 0,0
Xe,Ye = 0,0
x_space, y_space = 0,0
set_pos_needed = 0
font = 0
fsf = 0
set_font_needed = True


def get_el_byte():
    global el_idx
    b = get_press_byte(el_idx)
    el_idx += 1
    return b

def get_el_word():
    w = get_el_byte()
    w = (w << 8) + get_el_byte()
    return w

def get_el_double_word():
    w1 = get_el_word()
    w2 = get_el_word()
    return (w1 << 16) + w2

def get_dl_byte():
    global dl_idx
    b = get_press_byte(dl_idx)
    dl_idx += 1
    return b

def get_dl_word():
    w = get_dl_byte()
    return (w << 8) + get_dl_byte()

def get_dl_float():
    w1 = get_dl_word()
    w2 = get_dl_word()
    w = (w1 << 16) | w2
    if w == 0: return 0.0
    sign = 1
    # Can't test sign of w, because "long" logic of Python keeps the quantity
    # positive when shifting
    if (w1 & 0o100000) != 0:
        sign = -1
        w = w ^ 0xffffffff  # never mind adding 1
    w_exp = ((w >> 23) & 0xff) - 128
    base = float(math.pow(2, w_exp))
    accum = 0.0
    for i in range(23):
        # get ith bit of mantissa
        mbit = (w >> 22-i) & 1
        base = base/2
        if mbit == 1: accum += base
    accum = sign*accum
    #print "DL float ",accum
    return accum

def get_dl_string(n):
    global dl_idx
    s = ""
    for i in range(n):
        s += chr(get_press_byte(dl_idx))
        dl_idx += 1
    return s

def PS_set_pos():
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    if set_pos_needed == 0: return
    if set_pos_needed == 3:
        PS_f.write(str(x)+" "+str(y)+" moveto ")
    elif (set_pos_needed & 1) != 0:   # set x only
        PS_f.write("currentpoint exch pop "+str(x)+" exch moveto ")
    elif (set_pos_needed & 2) != 0:   # set y only
        PS_f.write("currentpoint pop "+str(y)+" moveto ")
    set_pos_needed = 0

def PS_show_chars(n, char_immediate=-1):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    fn = (fsf * 256) + font
    if set_font_needed:
        ps_font = fonts[fn]['psfn']
        PS_f.write("PressFonts "+str(ps_font)+" get setfont ")
        set_font_needed = False
    widths = fonts[fn]['widths']
    PS_set_pos()
    # hack for now
    if char_immediate == -1:
        s = get_dl_string(n)
    else:
        s = char_immediate
    fm.format("Show chars |{0}|", s)
    if not impose_widths:  # use PS widths for the font
        s = escape_PS_string(s, True)
        if x_space != -1:
            PS_f.write(str(x_space)+" "+str(y_space)+" 32 ("+s+" ) widthshow\n")
        else:
            PS_f.write("("+s+") show\n")
    else: # use PARC widths for the font
        for c in s:
            wid = widths[ord(c)]  # width of char
            if c == ' ' and x_space != -1: wid = x_space
            cs = escape_PS_string(c, True)  # ligature will print several chars
            PS_f.write("currentpoint ("+cs+") show exch "+str(wid)+" add exch moveto\n") 


def PS_set_font(f):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    font = f
    set_font_needed = True

def PS_space(what, amt=-1):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    if what == 'set-x':
        x_space = amt
    elif what == 'set-y':
        y_space = amt
    elif what == 'reset':
        x_space = -1
    elif what == 'space':
        PS_set_pos()   # if needed
        PS_f.write(str(x_space)+" "+str(y_space)+" 32 ( ) widthshow ")

def PS_rectangle(w, h):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    PS_set_pos()
    #print "Rectangle ",w,h
    PS_f.write("0 "+str(h)+" rlineto "+str(w)+" 0 rlineto 0 "+str(-h)+" rlineto "+str(-w)+" 0 rlineto closepath fill\n")

def PS_show_object(n):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    x, y = 0,0    # current position
    in_path = False
    while n != 0:
        cmd = get_dl_word()
        if cmd == 0:    # moveto
            if in_path:
                PS_f.write("closepath fill\n")
            in_path = True
            # do not get a signed number; signed_add will deal with it
            x = get_dl_word()
            y = get_dl_word()
            n -= 3
            x = signed_add(x, Xe)
            y = signed_add(y, Ye)
            PS_f.write("newpath "+str(x)+" "+str(y)+" moveto ")
        elif cmd == 1:  # drawto
            # do not get a signed number; signed_add will deal with it
            x = get_dl_word()
            y = get_dl_word()
            n -= 3
            x = signed_add(x, Xe)
            y = signed_add(y, Ye)
            PS_f.write(str(x)+" "+str(y)+" lineto ")
        elif cmd == 2:  # drawcurve
            Cx = get_dl_float()
            Cy = get_dl_float()
            Bx = get_dl_float()
            By = get_dl_float()
            Ax = get_dl_float()
            Ay = get_dl_float()
            n -= 13
            # now compute Bezier control points (Postscript ref man p 150)
            X1 = float(x) + Cx/3.0
            Y1 = float(y) + Cy/3.0
            X2 = X1 + (Cx + Bx)/3.0
            Y2 = Y1 + (Cy + By)/3.0
            X3 = float(x) + Cx + Bx + Ax
            Y3 = float(y) + Cy + By + Ay
            # now put out PS command
            PS_f.write(str(int(X1))+" "+str(int(Y1))+" "+str(int(X2))+" "+str(int(Y2))+" "+
                       str(int(X3))+" "+str(int(Y3))+" curveto ")
            x = int(X3)
            y = int(Y3)
        else: raise Exception("DL for object mal formed")
    PS_f.write("closepath fill\n")

def PS_show_dots(n):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed
    sav_idx = dl_idx
    PS_set_pos()
    while True:
        b = get_dl_byte()
        # first check one-byte commands
        if b == 1:   # set-coding
            code = get_dl_byte()
            dots = get_dl_word()
            lines = get_dl_word()
        elif b == 2:  # mode
            mode = get_dl_byte()
        elif b == 0:  # look for 2-byte commands
            w = get_dl_byte()
            if w == 1:   # set window
                pd = get_dl_word()
                dd = get_dl_word()
                pl = get_dl_word()
                dl = get_dl_word()
                if pd != 0 or pl != 0: fm.format("Ignoring show-dots set-size")
            elif w == 2:  # set size
                width = get_dl_word()
                height = get_dl_word()
            elif w == 3:  # dots follow
                break
            else: raise Exception("Show dots command NYI")
        else: raise Exception("Show dots command NYI")
    fm.format("Show dots: code {0} {1} {2} mode {3} size {4} {5}", code, dots, lines, mode, width, height)
    if dots % 16 != 0: raise Exception("Dots must be multiple of 16")
    PS_f.write("/picstr "+str(dots//16)+" string def\n")
    PS_f.write("currentpoint translate "+str(width)+" "+str(height)+" scale\n")
    PS_f.write(str(dots)+" "+str(lines)+" 1 [ "+str(dots)+" 0 0 "+str(-lines)+" 0 "+str(lines)+" ] ")
    PS_f.write("{ currentfile picstr readhexstring pop } image\n")
    for i in range(lines):
        for j in range(dots//16):
            w = get_dl_word()
            w ^= 0xffff  # make black on white
            PS_f.write(format(w, "04x"))
        PS_f.write("\n")
    dl_idx = sav_idx + 2 * n

def PS_page_show(n):
    global el_idx, el_end, dl_idx, x, y, Xe, Ye, x_space, y_space, set_pos_needed, font, fsf, set_font_needed

    p = get_page(n)
    # p is [ part_begin, part_len, EL_padding, # entities ]
    if p is None: return   # no such page

    # set up micas coordinate system
    PS_f.write("72 2540 div 72 2540 div scale\n")
    for eidx in range(p[3]):   # loop through entity count
        backup = p[3]-eidx-1   # number of entities to back up
        e_trailer = p[0]+p[1]-p[2]-1  # points to "entity-length" word
        for bu in range(backup):
            e_trailer -= press_file[e_trailer]
        # now e_trailer points to a trailer to execute
        fsf = press_file[e_trailer-11] & 0o377
        font = 0
        set_font_needed = True
        Xe = press_file[e_trailer-6]
        Ye = press_file[e_trailer-5]
        PS_space('reset')
        set_pos_needed = 3
        alt_state = 0  # alternative tracking
        dl_idx = (p[0] * 2)+((press_file[e_trailer-10] << 16) + press_file[e_trailer-9])  # byte count where DL begins
        el_idx = (e_trailer - press_file[e_trailer] + 1) * 2  # byte index
        el_end = (e_trailer - 11) * 2
        while el_idx != el_end:
            b = get_el_byte()
            #print "EL byte",b
            if b < 0o40:       # show characters short
                PS_show_chars(b+1)
            elif b < 0o100:    # skip characters short
                dl_idx += b-0o40+1
            elif b < 0o140:    # show characters and skip
                PS_show_chars(b-0o100+1)
                dl_idx += 1
            elif b < 0o150:    # set space x short
                b2 = get_el_byte()
                PS_space('set-x', ((b & 3) << 8) + b2)
            elif b < 0o160:    # set space y short
                b2 = get_el_byte()
                PS_space('set-y', ((b & 3) << 8) + b2)                
            elif b < 0o200:    # font
                PS_set_font(b-0o160)
            elif b == 0o353:   # skip control bytes immediate
                el_idx += get_el_byte()
            elif b == 0o354:   # alternative
                el_types = get_el_word()
                el_bytes = get_el_double_word()
                dl_bytes = get_el_double_word()
                # assume can handle the first alternative
                if alt_state == 0:
                    alt_state = 1      # say we're parsing first alternative
                else:                  # are coming upon another alternative, pass up
                    el_idx += el_bytes
                    dl_idx += dl_bytes
                    if el_types == 0 and el_bytes == 0 and dl_bytes == 0:
                        alt_state = 0  # no longer processing alternatives
            elif b == 0o356:   # set x
                x = signed_add(Xe, get_el_word())
                set_pos_needed |= 1 
            elif b == 0o357:   # set y
                y = signed_add(Ye, get_el_word())
                set_pos_needed |= 2
            elif b == 0o360:   # show characters
                PS_show_chars(get_el_byte())
            elif b == 0o361:   # skip characters
                dl_idx += get_el_byte()
            elif b == 0o362:   # skip control bytes
                n = get_el_word()
                get_el_byte()
                dl_idx += n
            elif b == 0o363:   # show character immediate
                PS_show_chars(1, get_el_byte())
            elif b == 0o364:   # set space x
                PS_space('set-x', get_el_word())
            elif b == 0o365:   # set space y
                PS_space('set-y', get_el_word())
            elif b == 0o366:   # reset space
                PS_space('reset')
            elif b == 0o367:   # space
                PS_space('space')
            elif b == 0o370:   # set brightness
                get_el_byte()  # ignore
            elif b == 0o371:   # set hue
                get_el_byte()  # ignore
            elif b == 0o372:   # set saturation
                get_el_byte()
            elif b == 0o373:   # show object
                PS_show_object(get_el_word())
            elif b == 0o374:   # show dots
                PS_show_dots(get_el_double_word())
            elif b == 0o375:   # show dots opaque
                PS_show_dots((get_el_word() << 16) + get_el_word(), opqaue=True)
            elif b == 0o376:   # show recetangle
                w = get_el_word()
                h = get_el_word()
                PS_rectangle(w, h)
            elif b == 0o377:   # nop
                pass
    PS_f.write("showpage\n")

def write_PS(fn='tst.ps'):
    """ Write PostScript for a single Press file.  Call after reading in press file """
    font_setup()
    PS_start(fn)
    PS_font_define(PS_f)
    PS_f.write("%%EndProlog\n%%Pages: (atend)\n")
    PS_f.write("%%Page ? ?\n")
    page = 1
    while get_page(page) is not None:
        PS_f.write("%%Page ? ?\n")
        PS_page_show(page)
        page += 1
    PS_f.write("%%Trailer\n%%Pages: "+str(page-1)+"\n")
    PS_end()


file_dir = "ns2/"
read_font_widths()

if False:   # do, write to tst.ps -- just to be sure all the code works
    for i in range(1,29):
        fn = "chapter" + format(i,"02d")+".press"
        fm.format("Reading {0}", fn)
        read_press_file(file_dir + fn)
        write_PS()

if False:   # If doing just one file
    #read_press_file("fig17-4.press", True)
    read_press_file(file_dir + "chapter21.press")
    write_PS()

def chapter(c):
    fn = "chapter{0:02d}".format(c)
    read_press_file(file_dir + fn + ".press")
    write_PS(fn + ".ps")

if __name__ == "__main__":
    # if you say "python -i press2ps.py" there will be only one argument (press2ps.py)
    # takes a single number as argument and converts that chapter
    if len(sys.argv) == 2:
        chapter(int(sys.argv[1]))

                
