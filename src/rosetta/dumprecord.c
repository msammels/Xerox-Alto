/* dumprecord.c */
/* cc -g -o dumprecord -D_POSIX_SOURCE dumprecord.c */
/* /usr/5bin/lint -u -n -lposix -D_POSIX_SOURCE dumprecord.c */
/* given the name of a file, open it and try to read it as if */
/* it were a MaxcTapeArchiveMedia.TapeRecord */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <malloc.h>
#include <time.h>

/* 3090 = 515*6 */
typedef enum {tapeblocksize = 3090} tapeblock;  /* 3090 = 515*6 */
typedef enum {brief, verbose, dumpcontents} argoption;

extern void exit();
extern void bcopy();

typedef enum {
	volume=0,
	directory=1,
	data=2,
	status=3,
	anon=255
	} RecordType;
	
typedef enum {
	primary=0
	} FunctionType;

typedef struct Volume_self {
	unsigned short fileNumber;	
	unsigned short blockNumber;	
	unsigned short sequenceNumber;	
	unsigned wastedSpace:8;	
	RecordType recordType:8;
	unsigned short volIDLLow;
	unsigned short volIDLHigh;
	FunctionType function:16;	
	unsigned short nOvLow;
	unsigned short nOvHigh;
	unsigned short nBytesLow;
	unsigned short nBytesHigh;
	unsigned short inchesOfDataLow;
	unsigned short inchesOfDataHigh;
	unsigned short length;
	unsigned short varData;
	/* varData computed */
	} VolumeRep;
typedef VolumeRep *Volume;

typedef struct Directory_self {
	unsigned short fileNumber;	
	unsigned short blockNumber;	
	unsigned short sequenceNumber;	
	unsigned wastedSpace:8;	
 	RecordType recordType:8;
	unsigned short fileNameStartLow;
	unsigned short fileNameStartHigh;
	unsigned short fileNameLengthLow;
	unsigned short fileNameLengthHigh;
	unsigned short propertyListStartLow;
	unsigned short propertyListStartHigh;
	unsigned short propertyListLengthLow;
	unsigned short propertyListLengthHigh;
	unsigned short gmtLow;
	unsigned short gmtHigh;
	unsigned short fileSizeLow;
	unsigned short fileSizeHigh;
	unsigned short length;
	unsigned short varData;
	/* varData computed */
	} DirectoryRep;
typedef DirectoryRep *Directory;

typedef struct Data_self {
	unsigned short fileNumber;	
	unsigned short blockNumber;	
	unsigned short sequenceNumber;	
	unsigned wastedSpace:8;	
	RecordType recordType:8;
	unsigned short length;
	unsigned short data;
	/* varData computed */
	} DataRep;
typedef DataRep *Data;

typedef struct Status_self {
	unsigned short fileNumber;	
	unsigned short blockNumber;	
	unsigned short sequenceNumber;	
	unsigned wastedSpace:8;	
	RecordType recordType:8;
	unsigned short numDataInsLeftLow;
	unsigned short numDataInsLeftHigh;
	unsigned short lastFilePositionUsedLow;
	unsigned short lastFilePositionUsedHigh;
	} StatusRep;
typedef StatusRep *Status;

typedef enum {less = 0, equal = 1, greater = 2} Comparison;

typedef struct MaxcWord_self {
	unsigned short high:16;
	unsigned short middle:16;
	unsigned short low:4;
	unsigned short junk:12;	
	} MaxcWord;

typedef unsigned MaxcByte;

static unsigned fixint(high, low)
unsigned short high, low;
{
	return((high << 16) | (low & 0xffff));
}

static void
error(s)
char *s;
{
	(void)fprintf(stderr, "dumprecord error: %s\n", s);
	exit(-1);
}

static void
error2(s, t)
char *s, *t;
{
	(void)fprintf(stderr, "dumprecord error: %s%s\n", s, t);
	exit(-1);
}

static void
usage()
{
	(void)fprintf(stderr, "usage: dumprecord [-v|d] fileName\n");
	exit(-1);
}

static void
dumpvol(vp, option)
Volume vp;
argoption option;
{
	unsigned volIDLength = fixint(vp->volIDLHigh, vp->volIDLLow);
	unsigned numberOfOverheadBytesPerRecord = 
	  fixint(vp->nOvHigh, vp->nOvLow);
	unsigned bytesPerInch = fixint(vp->nBytesHigh, vp->nBytesLow);
	unsigned inchesOfData = 
	  fixint(vp->inchesOfDataHigh, vp->inchesOfDataLow);
	char *varData = (char *)malloc((size_t)(volIDLength + 1));

	bcopy((char *)&vp->varData, varData, volIDLength);
	
	if (option == verbose) {
		(void)printf("Record type: Archivist volume\n");
		(void)printf("fileNumber: %d\n", vp->fileNumber);
		(void)printf("blockNumber: %d\n", vp->blockNumber);
		(void)printf("sequenceNumber: %d\n", vp->sequenceNumber);
		(void)printf("volIDLength: %d\n", volIDLength);
		if (vp->function == primary) 
	     	(void)printf("function: primary\n");
		else (void)printf("function: %d\n", (unsigned)vp->function);
		(void)printf("numberOfOverheadBytesPerRecord: %d\n", 
		  numberOfOverheadBytesPerRecord);
		(void)printf("bytesPerInch: %d\n", bytesPerInch);
		(void)printf("inchesOfData: %d\n", inchesOfData);
		(void)printf("varData: %s\n", varData);
	}
	else {
		(void)printf("Archivist volume %s\n", varData);
	}
	free((void *)varData);
}

static void
dumpdir(ld, option)
Directory ld;
argoption option;
{
	unsigned gby = 1968; /* GMT Base Year */
	unsigned uby = 1970; /* Unix Base Year */
	/* 1968 was a leap year: */
	unsigned unixCorrection = (((uby-gby)*365+1) * 24*60*60); 
	unsigned propertyListStart = 
	  fixint(ld->propertyListStartHigh, ld->propertyListStartLow);
	unsigned propertyListLength = 
	  fixint(ld->propertyListLengthHigh, ld->propertyListLengthLow);
	char *propertyList = (char *)malloc((size_t)(propertyListLength + 1));
	char *base = (char *)&ld->varData;
	char *props = &base[propertyListStart];
	long fileCreateTime = fixint(ld->gmtHigh, ld->gmtLow) - unixCorrection;
	unsigned fileSize = fixint(ld->fileSizeHigh, ld->fileSizeLow);
	unsigned fileNameStart = 
	  fixint(ld->fileNameStartHigh, ld->fileNameStartLow);
	unsigned fileNameLength = 
	  fixint(ld->fileNameLengthHigh, ld->fileNameLengthLow);
	char *fileName = (char *)malloc((size_t)(fileNameLength + 1));
	char *filename = &base[fileNameStart];
	
	bcopy(filename, fileName, fileNameLength);
	fileName[fileNameLength] = '\0';
	bcopy(props, propertyList, propertyListLength);
	propertyList[propertyListLength] = '\0';
	
	if (option == verbose) {
		(void)printf("Record type: Archivist directory\n");
		(void)printf("fileNumber: %d\n", ld->fileNumber);
		(void)printf("blockNumber: %d\n", ld->blockNumber);
		(void)printf("sequenceNumber: %d\n", ld->sequenceNumber);
		(void)printf("fileCreateTime: %s", 
			ctime((struct tm *)&fileCreateTime));
		(void)printf("fileSize: %d\n", fileSize);
		(void)printf("fileName: %s\n", fileName);
		(void)printf("propertyList: \"%s\"\n", propertyList);
		}
	else {
		char *r = strchr(fileName, '[');
		if (r != NULL) r[0] = '(';
		r = strchr(fileName, ']');
		if (r != NULL) r[0] = ')';
		(void)printf("Archivist directory %s\n", fileName);
	}
	free((void *)fileName);
	free((void *)propertyList);
}

static void
dumpdata(ld, file, option)
Data ld;
FILE *file;
argoption option;
{
	long startofvardata = 10;
	switch (option) {
	  case brief:
		(void)printf("Archivist data fileNumber: %d\n",ld->fileNumber);
		break;
	  case verbose:
		(void)printf("Record type: Archivist data\n");
		(void)printf("fileNumber: %d\n", ld->fileNumber);
		(void)printf("blockNumber: %d\n", ld->blockNumber);
		(void)printf("sequenceNumber: %d\n", ld->sequenceNumber);
		(void)printf("length: %d\n", ld->length);
		break;
	  case dumpcontents:
	  	if (fseek(file, startofvardata, 0) < 0) error("fseek failed");
	  	while (!feof(file)) {
	  	  (void)putchar(getc(file));
	  	  }
		break;
	  default: error("unknown argoption value");
	  }
}

static void
dumpstatus(ls, option)
Status ls;
argoption option;
{

	unsigned numDataInsLeft = 
	  fixint(ls->numDataInsLeftHigh, ls->numDataInsLeftLow);
	unsigned lastFilePositionUsed = 
	  fixint(ls->lastFilePositionUsedHigh, ls->lastFilePositionUsedLow);
	
	if (option == verbose) {
		(void)printf("Record type: Archivist status\n");
		(void)printf("fileNumber: %d\n", ls->fileNumber);
		(void)printf("blockNumber: %d\n", ls->blockNumber);
		(void)printf("sequenceNumber: %d\n", ls->sequenceNumber);
		(void)printf("numDataInsLeft: %d\n", numDataInsLeft);
		(void)printf("lastFilePositionUsed: %d\n", 
			lastFilePositionUsed);
	}
	else {
		(void)printf("Archivist status\n");
	}
}

static Comparison CompareWords(a, b)
MaxcWord a, b;
{
	if ((a.high == b.high) && (a.middle == b.middle) && (a.low == b.low))
	  return(equal);
	if (a.high < b.high) return(less);
	if (a.high > b.high) return(greater);
	if (a.middle < b.middle) return(less);
	if (a.middle > b.middle) return(greater);
	if (a.low < b.low) return(less);
	if (a.low > b.low) return(greater);
	error("can't happen");
	return(less);
}

static MaxcByte HighHalf(w)
MaxcWord w;
{
	return((w.high << 2) | ((w.middle >> 14) & 0x3));
}

static MaxcByte LowHalf(w)
MaxcWord w;
{
	MaxcByte b;
	unsigned low11bits = 0x3fff;
	
	b = ((w.middle & low11bits) << 4) + w.low;
	return(b);
}

static unsigned NarrowToLCardinal(w)
MaxcWord w;
{
	if ((w.high >> 14) != 0) error("Maxcword to large");
	return((w.high << 20) + (w.middle << 4) + w.low);
}

static MaxcWord TapeToMaxcWord(s, wordNumber)
char *s;
unsigned wordNumber;
{
	MaxcWord out;
	unsigned short word = wordNumber * 6;
	unsigned char *in = (unsigned char *)s;
	out.high = in[word] * 256 + in[word + 1];
	out.middle = in[word + 2] * 256 + in[word + 3];
	out.low = in[word + 5] >> 4;
	return(out);
}

static unsigned TapeTo7BitBytes(s, word, txt, j)
char *s; 
unsigned word;
char *txt;
unsigned j;
{
	unsigned short a;
	unsigned low7bits = 0x7f;
	unsigned char *in = (unsigned char *)s;
	
	a = in[word+0];
	txt[j+0] = (a >> 1) & low7bits;
	
	a = (a << 8) | in[word+1];
	txt[j+1] = (a >> 2) & low7bits;
	
	a = (a << 8) | in[word+2];
	txt[j+2] = (a >> 3) & low7bits;
	
	a = (a << 8) | in[word+3];
	txt[j+3] = (a >> 4) & low7bits;
	
	a = (a << 8) | in[word+5];
	txt[j+4] = (a >> 5) & low7bits;
	
	return((txt[j+0] == '\0') || (txt[j+1] == '\0') || 
	       (txt[j+2] == '\0') || (txt[j+3] == '\0') || (txt[j+4] == '\0'));
}

static unsigned TapeTo8BitBytes(s, word, txt, j)
char *s; 
unsigned word;
char *txt;
unsigned j;
{
	unsigned char *in = (unsigned char *)s;
	txt[j+0] = in[word+0];
	txt[j+1] = in[word+1];
	txt[j+2] = in[word+2];
	txt[j+3] = in[word+3];	
	return((txt[j+0] == '\0') || (txt[j+1] == '\0') || 
	       (txt[j+2] == '\0') || (txt[j+3] == '\0') );
}

static char *TapeToString(in, start)
char *in; 
unsigned start; 
{
	unsigned word = start * 6;
	unsigned i = 0;
	MaxcByte wordCnt = LowHalf(TapeToMaxcWord(in, start));
	char *txt = (char *)calloc((size_t)(wordCnt * 6), sizeof(char));

	for (i = 1; i < wordCnt; i++) {
		unsigned j = (i - 1) * 5;
		word = (start + i) * 6;
		if(TapeTo7BitBytes(in, word, txt, j)) return(txt);
		}
	return(txt);
}

static void PrintDate(ld, word)
char *ld;
unsigned word;
{
	MaxcByte mb = HighHalf(TapeToMaxcWord(ld, word));
	MaxcByte mb1 = LowHalf(TapeToMaxcWord(ld, word));
	
	if ( (mb == 0) && (mb1 == 0) ) (void)printf("null\n");
	else {
	  unsigned gmtOrigin = 0;
	  unsigned gmtBaseYear = 1968;
	  unsigned alto1968 = 
	    ((gmtBaseYear-1901)*365+(gmtBaseYear-1901)/4)*24*60*60;
	  unsigned unixBaseYear = 1970;
	  unsigned unixCorrection = ((unixBaseYear-gmtBaseYear)*365+1)*24*60*60;
	  long date;
	  char *timestring;
	  unsigned p = ((mb-15385)*86400)+mb1;
	  
	  if (p < alto1968) error("OutOfRange");
	  date = (p-alto1968)+gmtOrigin-unixCorrection;
	  timestring = ctime((struct tm *)&date);
	  (void)printf(timestring);
	  free((void *)timestring);
	  }
}

static void
dumpmaxcdata(file, baseSequenceNumber, baseFileNumber, byteSize)
FILE *file;
unsigned baseSequenceNumber, baseFileNumber, byteSize;
{
	char buf[(int)tapeblocksize+1];
	char txt[(int)tapeblocksize+1];
	int nitems;
	unsigned i, fileNumber, sequenceNumber, pageNumber;
	unsigned lastSequenceNumber = baseSequenceNumber;
	int lastPageNumber = -1;
	
	if ((byteSize != 7) && (byteSize != 8)) return;
	while (1) { /* exit by return */
	  nitems = fread((void *)buf, 1, (int)tapeblocksize, file);
	  if (nitems == 0) {
	    (void)printf("\n");
	    return;
	    }
	  if (nitems != (int)tapeblocksize) error("Partial block");
	  sequenceNumber = NarrowToLCardinal(TapeToMaxcWord(buf, 0));
	  if (sequenceNumber != lastSequenceNumber + 1) 
	     error("sequence number out of order");
	  else
	     lastSequenceNumber = sequenceNumber;
	  fileNumber = HighHalf(TapeToMaxcWord(buf, 1));
	  if (fileNumber != baseFileNumber)
	     error("file number changed");
	  pageNumber = LowHalf(TapeToMaxcWord(buf, 1));
	  if ((int)pageNumber <= lastPageNumber)
	     error("pageNumber out of order");
	  else 
	     lastPageNumber = pageNumber;
	  /* now we've got the data field to dump */
	  bzero(txt, (int)tapeblocksize+1);
	  for (i = 1; i <= 512; i++) {
		unsigned j = (i - 1) * 5;
		unsigned word = (2 + i) * 6;
		if (byteSize == 7) {
		   if (TapeTo7BitBytes(buf, word, txt, j)) break; 
		   }
		else { /* byteSize == 8 */
		   if (TapeTo8BitBytes(buf, word, txt, j)) break;
		   }
		}
	  (void)printf("%s", txt);
	  }
}

static void
dumpmaxcdir(ld, file, option)
char *ld;
FILE *file;
argoption option;
{
	unsigned sequenceNumber = NarrowToLCardinal(TapeToMaxcWord(ld, 0));
	unsigned pageNumber = LowHalf(TapeToMaxcWord(ld, 1));
	unsigned fileNumber = HighHalf(TapeToMaxcWord(ld, 1));
	char *dirname = TapeToString(ld, 66);
	unsigned c = LowHalf(TapeToMaxcWord(ld, 3));
	char *name = TapeToString(ld, c + 2);
	unsigned d = HighHalf(TapeToMaxcWord(ld, 4));
	char *ext = TapeToString(ld, d + 2);
	unsigned versionNumber = HighHalf(TapeToMaxcWord(ld, 9));
	unsigned byteSize = HighHalf(TapeToMaxcWord(ld, 11)) / 0x40;
	unsigned fileSize = NarrowToLCardinal(TapeToMaxcWord(ld, 12));

	switch (option) {
	  case brief :
		(void)printf("Maxc file <%s>%s", dirname, name);
		if (strlen(ext) != 0) (void)printf(".%s", ext);
		(void)printf(";%d\n", versionNumber);
		break;
	  case verbose :
		(void)printf("Record type: Maxc file\n");
		(void)printf("fullFName: <%s>%s", dirname, name);
		if (strlen(ext) != 0) (void)printf(".%s", ext);
		(void)printf(";%d\n", versionNumber);
		(void)printf("createDate: "); PrintDate(ld, 13); 
		(void)printf("writeDate: "); PrintDate(ld, 14); 
		(void)printf("readDate: "); PrintDate(ld, 15); 
		(void)printf("fileSize: %d\n", fileSize);
		(void)printf("sequenceNumber: %d\n", sequenceNumber);
		(void)printf("pageNumber: %d\n", pageNumber);
		(void)printf("fileNumber: %d\n", fileNumber);
		(void)printf("byteSize: %d\n", byteSize);
		free((void *)dirname); free((void *)name); free((void *)ext);
		break;
	  case dumpcontents :
		dumpmaxcdata(file, sequenceNumber, fileNumber, byteSize);
		break;
	  default :
		error("unknown argoption value");
	  }
}

static void
dumpmaxcvol(dp, option)
char *dp;
argoption option;
{
	unsigned sequenceNumber = NarrowToLCardinal(TapeToMaxcWord(dp, 0));
	unsigned tapeNumber = NarrowToLCardinal(TapeToMaxcWord(dp, 1));
	unsigned lastBlockNumber = NarrowToLCardinal(TapeToMaxcWord(dp, 2));
	unsigned lastFileNumber = NarrowToLCardinal(TapeToMaxcWord(dp, 5));

	if (option == verbose) {
		(void)printf("Record type: Maxc volume\n");
		(void)printf("sequenceNumber: %d\n", sequenceNumber);
		(void)printf("tapeNumber: %d\n", tapeNumber);
		(void)printf("lastBlockNumber: %d\n", lastBlockNumber);
		(void)printf("lastFileNumber: %d\n", lastFileNumber);
	}
	else {
		(void)printf("Maxc volume %d\n", tapeNumber);
	}
}

main(argc, argv)
int argc; 
char **argv;
{
	unsigned i, firstFile = 0;
	argoption option = brief;
	
	/* Do switch parsing */
	switch (argc) {
	  case 0 :
	  case 1 :
	  	usage();
	  	break;
	  case 2 : 
	  	firstFile = 1;
	  	break;
	  default : 
	  	if ((argv[1][0] == '-') && (argv[1][1] == 'v')) {
	  	    option = verbose;
	  	    firstFile = 2;
	  	    }
	  	else {
	  	    if ((argv[1][0] == '-') && (argv[1][1] == 'd')) {
	  	      option = dumpcontents;
	  	      firstFile = 2;
	  	      }
	  	    else firstFile = 1;
	  	    }
	  	break;
	  }
	
	for (i = firstFile; i < argc; i++) {
	  char *filename = argv[i];
	  char bp[(int)tapeblocksize];
	  int fd, nitems;
	  FILE *file;
	  struct stat statbuf;
	  
	  if (access(filename, R_OK) != 0) usage();
	  fd = open(filename, O_RDONLY);
	  if (fd == -1) error2("Error opening input file ", filename);
	  if ((file = fdopen(fd, "r")) == NULL)
		error2("Error opening input stream for file ", filename);
	  nitems = fread((void *)bp, 1, (int)tapeblocksize, file);
	  if (nitems == 0) error2("couldn't read file ", filename);
	  if (option != dumpcontents) (void)printf("%s: ", filename);
	  /* Decide what kind of a file it is */
	  /* There doesn't seem to be a better way to figure out if a */
	  /* file is a maxc volume record - the size of the file is the */
	  /* best heuristic I've seen.  It also appears that the only maxc */
	  /* file that's allowed to have */
	  /* statbuf.st_size % tapeblocksize !=0 is the volume record */
	  if (fstat(fd, &statbuf) == -1) error2("fstat failed for ", filename);
	  if (statbuf.st_size == 618384) dumpmaxcvol(bp, option);
	  else {
	    MaxcByte lowhalf = LowHalf(TapeToMaxcWord(bp, 1));
	    switch (lowhalf) {
	      case 0x3fffd : 
	      	dumpmaxcdir(bp, file, option); 
	      	break;
	      case 0x3ffff :
	        (void)printf("Maxc end of tape marker\n");
	        break;
	      default :
	        switch (((Directory)bp)->recordType) {
	        case volume:
	          dumpvol((Volume)bp, option);
	          break;
	        case directory:
	          dumpdir((Directory)bp, option);
	          break;
	        case data:
	          dumpdata((Data)bp, file, option);
	          break;
	        case status:
	          dumpstatus((Status)bp, option);
	          break;
	        default: 
	          error("Not an archive or bsys data structure");
		  break;
		}
	      }
	    }
	  (void)fclose(file);
	  (void)close(fd);
	  }
	return(0);
}