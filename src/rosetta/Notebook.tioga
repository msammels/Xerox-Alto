
CSL Notebook Entry: CSL Sun Based Archives

XEROX
To	From
CSL-NotebookReaders	James K. Foote
	2116



Subject	Date
CSL Sun Based Archives	June 25, 1991 10:59:37 am PDT


FOR XEROX INTERNAL USE ONLY

Abstract	CSL uses a 9 track tape drive on a Sun server to read tapes created with the Maxc bsys program and the Cedar based Archivist program. This document describes how to use the archive retrieval software, and how to use this software to transfer 9 track tape images to 8mm tapes.
Attributes	CSL Sun Unix
Other attributes	informal, draft, technical
References	ArchivesDoc.tioga
Motivation and Philosophy
We wanted to recover floorspace in the Maxc room.  There were two prime candidates - the alto tapeserver and the 9 track tape cabinets which stored the tapes created by the tapeserver.  To eliminate the need for the alto tapeserver we had to be able to preserve the promise that the original archivist and bsys programs made - that a file listed in the archives database could be recovered from tape.  To eliminate the need for the 9 track tape cabinets we needed to also transfer the 9 track tape images to 8mm tapes.
A non-goal is to reimplement or port the Archivist.
Who should be reading this (and other caveats)
These tools are designed for SoftwareSupport members to manage 9 track tapes, they're not intended for public consumption.  The 9 track tapes represent millions of dollars of intellectual investment - we want to do everything we can to safeguard them from accidental damage.  One of the big fears is that the tape media itself will disintegrate from repeated passes through the tape heads - so the fewer passes the better. This is particularly a concern for the Maxc tapes since some of the older bsys (maxc) tapes are known to be near end of life.  Each trip through the heads expends one of the few remaining reads of the tape - so a read with dirty heads that fails expends a read without extracting the data. Better to clean the heads first then attempt the read. 
Both the Maxc and Archivist tapes were created in pairs of primary and secondary tapes.  The Archivist primary and backup tapes have the same tape number with different prefixes.  The Maxc tapes are numbered sequentially, tapes 1 and 2 being paired, 3 and 4 paired etc.  But because the secondary Maxc tapes can be shorter than the primaries (ie if files were deleted between the two runs) the pair numbers diverge.  So 411 and 417 may be paired, etc.  This isn't terribly relevant to anything - just a warning so you won't be surprised.    
How to recover a file from 9 track tapes
Figure out which tape you need.
Archivist tape.
See ArchivesDoc.tioga.  For example, in a PCedar world you might type:

% ListArchives [Ivy]<CHauser>Modula-2>CursorMouse.DEF*
Enumerating ...
[Ivy]<CHauser>Modula-2>
CursorMouse.DEF!1	25 May 85 16:40:54 PDT	 T10439P T10439B
complete
Done
%

This tells you that the 9 track tape you want is the one named "T10439P" or "T10439B". Find the tape (try the cabinets in the Maxc room, or the cabinets in the TIC archives in the warehouse, or ask SoftwareSupport).

BSYS (Maxc) tape.
Most Maxc users retained a copy of their archive directory listing, so most requests for Maxc tapes come from users with the name of the tape.  For example:
<BECKER>CSTARGRAMMAR830.TEXT:1;P775200 1430 31-AUG-84 14:40:24   1431 31-AUG-84 15:03:55
Which says that the desired file (<BECKER>CSTARGRAMMAR830.TEXT:1) can be found on tape 1430 or 1431.
If you don't know which BSYS tape contains the desired file the only recourse is to use the steps below to trasfer all the BSYS tapes to 8mm tape - a biproduct of which are the log files which summarize the contents of the BSYS tapes. You can then grep through the logs to find the names of the BSYS tape immages that contain the files you're looking for.

Mount the tape
Make sure there is no write ring inserted and mount the tape on the Sun 9 track tape drive (currently  "lyrane" in the Purple Lab). One of the big fears is that the tape media itself will disintegrate from repeated passes through the tape heads - so the fewer passes the better. This is particularly a concern for the Maxc tapes since some of the older bsys (maxc) tapes are known to be near end of life.  Each trip through the heads expends one of the few remaining reads of the tape - so a read with dirty heads that fails expends a read without extracting the data. Better to clean the heads first then attempt the read. 
To mount a tape, pull the door to the tape drive open (pull hard), then thread the tape following the diagram on the drive.  Close the door.  Press "Density Select" until the light next to "Host" lights up, then press "Start."  The drive will then load the tape and indicate what density the tape seems to be.  To unload the tape, press "Reset" and then "Rewind."  You can also force the density to 1600 or 6250 by using "Density Select."

Login and create a working directory
Create a working directory in a place with plenty of free space.  40 Mb is probably enough. For example, from a shell:

pandora% rlogin lyrane
lyrane% cd /lyrane/tmp; mkdir restore; cd restore

Extract the tape contents
Add the tape extraction software to your path, then extract the contents of the tape to the working directory.  For example, in a cshell:

lyrane% source /project/archivist/top/enable; rehash
lyrane% dumptape
Extracting tape TapeNumber112
........(each dot represents a file being extracted)....

Find the file you're looking for

lyrane% makelisting TapeNumber112
lyrane% grep CursorMouse.DEF TapeNumber112.listing
TapeNumber112/file0015: <CHAUSER>CursorMouse.DEF!1
lyrane% dumprecord -d TapeNumber112/file0015 > CursorMouse.DEF
lyrane% cp CursorMouse.DEF /tilde/fred/CursorMouse.DEF

It is possible that the "grep" step above will result in two different files which both claim to have the same name and even version number.  These CAN be two totally different files created at different times and for different purposes.  For example, if a file was archived then deleted, and the file name was subsequently reused.  Bottom line: be careful to find the file you really want.  

Cleanup after yourself.
Unmount the tape:
lyrane% mt -f /dev/rmt0 unload 
Then unload the tape from the tape drive and return it to whereever you found it.  Then delete the temporary directory:
lyrane% cd /lyrane/tmp; rm -r -f restore.
How to transfer 9 track tapes to 8mm tapes
Add the archivist to the search path
lyrane%  source /project/archivist/top/enable; rehash

Pick a name for the Archive
For this example we'll assume that the 8mm tape we want to build will be named "Archivist1986". Insert a blank tape into the 8mm tape drive.  Make sure the little red tab on the 8mm tape is pulled back (ie the tape is NOT write protected).  Create a working directory on the Sun tapeserver (lyrane) in a place with plenty of free space.  40 Mb is probably enough. For example, from a shell:

lyrane% cd /tmp/restore; mkdir Archivist1986; cd Archivist1986

Initialize the 8mm tape
Insert a blank tape into the 8mm tape drive.  Make sure the little red tab on the 8mm tape is pulled back (ie the tape is NOT write protected).  Initialize the tape:
lyrane% initarchive  
Initializing 8mm tape as Archivist1986
mt -f /dev/rsmt0 rew
dd if=/project/archivist/src/rosetta.tar of=/dev/nrsmt0
echo 'Summary of 9 track tape images on Archivist1986:' > Archivist1986.summary
echo 'Files on Archivist1986:' > Archivist1986.log

For each 9 track tape to be transfered
Load the 9 track tape in the tape drive - make sure that NO write ring is present. Then:

lyrane% transferimage  
extracting tape T10439P .....(each dot represents a file being extracted)...
dumprecord T10439P/* >> Archivist1986.log
tar cf - T10439P > T10439P.tar
rm -r -f T10439P
compress T10439P.tar
dd if=T10439P.tar.Z of=/dev/nrsmt0
rm -r -f T10439P.tar.Z 
mt -f /dev/rmt0 offline
echo T10439P.tar.Z >> Archivist1986.summary

Unload the 9 track tape from the tape drive and repeat.
If you get stopped in the middle of these steps
If you get stopped in the middle of these steps and have to give up the tape server:

lyrane% mt -f /dev/rmt0 rew
lyrane% mt -f /dev/rsmt0 rew
Unload the 8mm and/or the 9 track drive.

Later, when the time comes to startup again:
Mount the 8mm and/or the 9 track drive. To mount a 9 track tape, pull the door to the tape drive open (pull hard), then thread the tape following the diagram on the drive.  Close the door.  Press "Density Select" until the light next to "Host" lights up, then press "Start."  The drive will then load the tape and indicate what density the tape seems to be.  To unload the tape, press "Reset" and then "Rewind."  You can also force the density to 1600 or 6250 by using "Density Select."
To load an 8mm tape push the button the tape drive, wait for the door to pop open, insert the tape and close the door. Then:
lyrane% mt -f /dev/rmt0 rew
lyrane% mt -f /dev/rsmt0 fsf N
(where "N" is the number of 9 track tapes already copied to this 8mm tape - if you've forgotten N, go back and count the number of entries in Archivist1986.summary)
Then pick up at "For each 9 track tape..." above.
To finish an 8mm tape
When you're done with a particular 8mm tape, finish it off with:

lyrane% finisharchive 
Finishing Archivist1986 8mm tape
dd if=Archivist1986.log of=/dev/nrsmt0
echo Archivist1986.log >> Archivist1986.summary
echo Archivist1986.summary >> Archivist1986.summary
dd if=Archivist1986.summary of=/dev/nrsmt0
mt -f /dev/rsmt0 rew
lpr Archivist1986.summary
cp Archivist1986.summary /project/archivist/logs/
cp Archivist1986.log /project/archivist/logs/
rm -r -f Archivist1986.summary Archivist1986.log

Unload the 8mm tape, attach the Archivist1986.summary printout to the tape box and flip the red tab on the 8mm tape case to the write protect (red tab visisble) position.
How to retrieve a file from an 8mm tape
Use the ListArchives command in Cedar (see ArchivesDoc.tioga) to find the name of the 9 track tape that has the file you want to retrieve. For example:
% ListArchives [Ivy]<CHauser>Modula-2>CursorMouse.DEF*
Enumerating ... 
 [Ivy]<CHauser>Modula-2>
  CursorMouse.DEF!1	25 May 85 16:40:54 PDT	 T10439P T10439B	complete
Done
%
Then the name of the 9 track tape that has the file you want is "T10439P".
Find the name of the 8mm tape that holds the 9 track tape image listed in the BTree.  In this example:
lyrane% grep T10439P /project/archivist/logs/*.summary
...Archivist1986.summary: T10439P.tar.Z
lyrane%

Examine Archivist1986.summary and count the number of files to the tape you're interested in.  In this case:

lyrane% cat Archivist1986.summary
Archivist1986 tape contents:
rosettastone.tar
T10437P.tar.Z
T10438P.tar.Z
T10439P.tar.Z
T10440P.tar.Z
...
lyrane%

Then T10439P.tar.Z is the 4th file on the 8mm tape. Mount the Archivist1986 8mm tape and extract the 9 track tape image:

lyrane% mt -f /dev/rsmt0 rew
lyrane% mt -f /dev/rsmt0 fsf 4 
Where "4" is the index found above
lyrane% dd if=/dev/rsmt0 of=T10439P.tar.Z
lyrane% mt -f /dev/rsmt0 rew
lyrane% uncompress T10439P.tar.Z
lyrane% tar xf T10439P.tar.Z

Then follow the steps from "Find the file you're looking for" above.

Don't forget to unmount the 8mm tape when you're done.   And don't forget to delete the restored files that you aren't interested in.
The Rosetta Stone
I wanted to leave enough info on each 8mm tape so that an archeologist might be able to figure out what I'd been up to.  So we created a tar file which includes the sources to everything that seemed releated and all the existing documentation.  This tar file is always the first image on an 8mm tape. The hope is that someone finding a tape might try to run tar on it and would find enough info to figure out how to read the rest of the tape.
The Programs
Programs
dumprecord
Given an file which is a copy of a record from a 9 track tape, dumprecord will print details of the file header and body to the standard output.  dumprecord can read both bsys and archivist record images. 
Scripts
dumptape
Used to transfer the entire contents of a 9 track tape to disk.
makelisting
Used to build a record number to filename mapping.
initarchive 
Used to initialize a new 8mm tape to receive 9 track tape images.
finisharchive
Used to finish an 8mm tape.
transferimage
Used to transfer the entire contents of a 9 track tape image to an 8mm tape.
   D  Copyright c 1991 by Xerox Corporation.  All rights reserved.
 Κ
Κ  StyleDef±
BeginStyle(Cedar) AttachStyle(abstract) "for abstract nodes" {  head4  AlternateFontFamily  14 en tabStops  } StyleRule  (root) "format for root nodes" {  cedarRoot  } ScreenRule  (root) "format for root nodes" {  0 firstHeaders  0 lastDropFolio  FontPrefix  docStandard  36 pt topMargin  36 pt headerMargin  .5 in footerMargin  .5 in bottomMargin  1.75 in leftMargin  1.5 in rightMargin  5.25 in lineLength  24 pt topIndent  24 pt topLeading  0 leftIndent  0 rightIndent  } PrintRule  (pagenumber) "format for folio on each page" {  unleaded  AlternateFontFamily  } StyleRule(positionInternalMemoLogo) "Xerox logo: screen" {  docStandard  1 pt leading  1 pt topLeading  1 pt bottomLeading  } ScreenRule(positionInternalMemoLogo) "for Xerox logo" {  docStandard  1 pt leading  1 pt topLeading  -36 pt bottomLeading  -1.5 in leftIndent  } PrintRule(internalMemoLogo) "Xerox logo: screen" {  "Logo" family  18 bp size  20 pt topLeading  20 pt bottomLeading  } ScreenRule(internalMemoLogo) "for Xerox logo" {  "Logo" family  18 pt size  12 pt leading  -36 pt topLeading  36 pt bottomLeading  -1.5 in leftIndent  } PrintRule(memoHead) "for the To, From, Subject nodes at front of memos" {  docStandard  AlternateFontFamily  240 pt tabStops  } StyleRuleEndStyle"LocalStyle-3" styleNewlineDelimiter
 IunleadedMarkinsideHeaderΠbo**Ipositioninternalmemologo IinternalmemologomemoheadΟsNΟt"NN N N N44N N Iabstract
Οm1<OΡboxO OO
O+O
headIbodyQ33.QQ(	Οb	’	FF	 J’56JJ99JJJ’J ΧΧJ 	’	QXQdQγQ 	’Qππ	ΆJ 	’$$Qvv	 JJ11Q 	’	J J44JJ88J 	’   J!!22J22J>>66J JΟiJ 	’	Q	wQ))*	’$$Q55Q 	’	J J>>J 	’	₯₯J&&JJ77JOOJ22Q 	’&&	X JLLJ))JJJJ""JJJ++J J77	£//	T JJ(J ,Qζ	||JJQ€€Q11	’	@ J  J&&J//J33J**JJJ11J--00J Jͺ'	J’46JJ]]JJ’QJJ	ffJ66J''JJ Jl J!!JJJJJJJJJ Jxx JJ"J))JJ  JJ DJ JQΊΊ	’	

QΝΝ	’	Q?	Q2	QA	Q	QL     00  ;>  