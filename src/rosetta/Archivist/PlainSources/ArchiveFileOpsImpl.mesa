-- ArchiveFileOpsImpl.mesa
	-- Copyright � 1984, 1985, 1986, 1987 by Xerox Corp.  All rights reserved.
	-- Last edited by Tim Diebert: November 15, 1987 11:24:26 am PST

DIRECTORY
	ArchiveFileOps,
	BasicTime USING [GMT, nullGMT],
	Convert USING [IntFromRope, Error, TimeFromRope],
	FS USING [ComponentPositions, Error, ExpandName],
	STPRemoteFile USING [GetServerPupName],
	IO USING [Close, EndOfStream, GetChar, RIS, STREAM],
	Process USING [Pause, SecondsToTicks],
	RefText USING [AppendChar, Append, AppendRope, New, ObtainScratch, TrustTextAsRope],
	Rope USING [Cat, Equal, FromRefText, IsEmpty, ROPE, Substr],
	STP USING [Close, ConnectionErrors, ConfirmProcType, Continue, Create, Enumerate, Error,
		ErrorCode, Handle, IsOpen, Login, NoteFileProcType, Open, Retrieve, ValidProperties],
	STPPrivate USING [Handle, PListArray, PList],
	STPBackdoor USING [BadProperty, DoFiles, ResetPList, SetPListItem, UserStateToPList],
	UserCredentials USING [Get];

ArchiveFileOpsImpl: CEDAR PROGRAM
	IMPORTS Convert, IO, FS, Process, RefText, Rope, STP, STPBackdoor, STPRemoteFile, UserCredentials
	EXPORTS ArchiveFileOps = BEGIN OPEN ArchiveFileOps;
	
	ROPE: TYPE ~ Rope.ROPE;
	STREAM: TYPE ~ IO.STREAM;
	TIME: TYPE ~ BasicTime.GMT;
	nulGMT: TIME ~ BasicTime.nullGMT;
	
	propertyStrings: STPPrivate.PListArray = [
		userName: "User-Name", userPassword: "User-Password",
		connectName: "Connect-Name", connectPassword: "Connect-Password",
		byteSize: "Byte-Size", type: "Type", size: "Size",
		directory: "Directory", nameBody: "Name-Body", version: "Version",
		createDate: "Creation-Date", readDate: "Read-Date", writeDate: "Write-Date",
		author: "Author",
		eolConversion: "End-of-Line-Convention",
		account: "Account", userAccount: "User-Account",
		device: "Device", serverName: "Server-Filename"];
		
	OpenConnection: PUBLIC PROC [server: ROPE] RETURNS [Connection] = BEGIN
		ENABLE UNWIND => NULL;
		c: Connection _ NEW [ConnectionRecord];
		name, password: ROPE _ NIL;
		[name, password] _ UserCredentials.Get[];
		c.stpHandle _ STP.Create[];
		STP.Login[c.stpHandle, name, password];
		c.rt _ RefText.New[4096];
		OpenNew[c, server];
		RETURN [c];
		END;
		
	CloseConnection: PUBLIC PROC [c: Connection] = BEGIN
		ENABLE UNWIND => NULL;
		IF c # NIL AND c.open THEN {c.stpHandle.Close[ ! STP.Error => CONTINUE]; c.open _ FALSE};
		END;
		
	GetPropertyList: PUBLIC PROC  [c: Connection, fullFName: ROPE]  RETURNS [propertyList: ROPE, createTime: BasicTime.GMT, size: INT] = BEGIN
		ENABLE UNWIND => NULL;
		NoteProps: STP.NoteFileProcType = BEGIN
			-- PROC [file: Rope.ROPE] RETURNS [continue: Continue]
			handle: STPPrivate.Handle;
			pList: STPPrivate.PList;
			c.rt.length_ 0;
			c.rt _ RefText.AppendChar[c.rt, '(];
			TRUSTED {handle _ LOOPHOLE[c.stpHandle]};
			pList _ handle.plist;
			FOR prop: STP.ValidProperties IN [directory .. serverName] DO
				IF pList[prop] # NIL THEN BEGIN
					c.rt _ RefText.AppendChar[c.rt, '(];
					c.rt _ RefText.Append[c.rt, propertyStrings[prop]];
					c.rt _ RefText.AppendChar[c.rt, ' ];
					c.rt _ RefText.Append[c.rt, pList[prop]];
					c.rt _ RefText.AppendChar[c.rt, ')];
					END;
				ENDLOOP;
			c.rt _ RefText.AppendChar[c.rt, ')];
			IF pList[createDate] = NIL THEN ERROR FileOpsError["Create date missing.", FALSE];
			IF pList[createDate].length = 0 THEN ERROR FileOpsError["Create date missing.", FALSE];
			createTime _ Convert.TimeFromRope[Rope.FromRefText[pList[createDate]]
				! Convert.Error => ERROR FileOpsError["Create date invalid.", FALSE]];
			IF pList[size] = NIL OR pList[size].length = 0
				THEN size _ 0
				ELSE size _ Convert.IntFromRope[Rope.FromRefText[pList[size]]
					! Convert.Error => ERROR FileOpsError["File size invalid.", FALSE]];
			RETURN[yes];
			END;
		
		cp: FS.ComponentPositions;
		server: ROPE _ NIL;
		file: ROPE _ NIL;
		name: ROPE _ NIL;
		password: ROPE _ NIL;
		retryCnt: CARDINAL_ 0;
		
		[fullFName: fullFName, cp: cp] _ FS.ExpandName[fullFName];
		server _ fullFName.Substr[cp.server.start, cp.server.length];
		file _ fullFName.Substr[cp.dir.start-1];
		IF NOT server.Equal[c.serverName, FALSE] OR NOT c.open THEN OpenNew[c, server];
		c.stpHandle.Enumerate[fullFName, NoteProps
			! STP.Error => IF code = connectionRejected AND retryCnt < 100
				THEN {retryCnt_ retryCnt + 1; Process.Pause[Process.SecondsToTicks[1]]; RETRY}
				ELSE ERROR FileOpsError[error, (code IN STP.ConnectionErrors)]];
		propertyList _ Rope.FromRefText[c.rt];
		END;
	
	OpenNew: PROC [c: Connection, server: ROPE] = BEGIN
		retryCnt: CARDINAL _ 0;
		pupServer: ROPE;
		IF c.open AND c.stpHandle # NIL AND STP.IsOpen[c.stpHandle] THEN c.stpHandle.Close[];
		c.open _ FALSE;
		pupServer _ STPRemoteFile.GetServerPupName[server
			! FS.Error => ERROR FileOpsError["FS Error while looking up server name.", TRUE]];
		[ ] _ c.stpHandle.Open[pupServer
			! STP.Error => IF code = connectionRejected AND retryCnt < 100
				THEN {retryCnt_ retryCnt + 1; Process.Pause[Process.SecondsToTicks[1]]; RETRY}
				ELSE ERROR FileOpsError[error, (code IN STP.ConnectionErrors)]];
		c.serverName _ server;
		c.open _ TRUE;
		END;
	
	Retrieve: PUBLIC PROC [c: Connection, fullFName: ROPE, outStream: STREAM] = BEGIN
		ENABLE UNWIND => NULL;
		Confirm: STP.ConfirmProcType = BEGIN
			-- PROC[file: Rope.ROPE] RETURNS [answer: Confirmation, localStream: IO.STREAM]
			RETURN[do, outStream];
			END;
		user: ROPE _ NIL;
		name, password: ROPE _ NIL;
		cp: FS.ComponentPositions;
		server: ROPE _ NIL;
		file: ROPE _ NIL;
		retryCnt: CARDINAL_ 0;
		[fullFName: fullFName, cp: cp] _ FS.ExpandName[fullFName];
		server _ fullFName.Substr[cp.server.start, cp.server.length];
		file _ fullFName.Substr[cp.dir.start-1];
		IF NOT server.Equal[c.serverName, FALSE] OR NOT c.open THEN OpenNew[c, server];
		c.stpHandle.Retrieve[file, Confirm
			! STP.Error => IF code = connectionRejected AND retryCnt < 100
				THEN {retryCnt_ retryCnt + 1; Process.Pause[Process.SecondsToTicks[1]]; RETRY}
				ELSE {outStream.Close[];
					ERROR FileOpsError[error, (code IN STP.ConnectionErrors)]}];
		outStream.Close[];
		END;
	
	Store: PUBLIC PROC
		[c: Connection, fullFName: ROPE, propertyList: ROPE, inStream: STREAM] = BEGIN
		OPEN STPPrivate;
		ENABLE UNWIND => NULL;
		Confirm: STP.ConfirmProcType = BEGIN
			-- PROC[file: Rope.ROPE] RETURNS [answer: Confirmation, localStream: IO.STREAM]
			RETURN[do, inStream];
			END;
		Append: PROC [c: CHAR] = {
			SELECT which FROM
				property => property _ RefText.AppendChar[property, c];
				value => value _ RefText.AppendChar[value, c];
				ENDCASE;
			};
		property: REF TEXT _ RefText.ObtainScratch[512];
		value: REF TEXT _ RefText.ObtainScratch[512];
		parens: INT _ 0;
		char: CHAR;
		which: {property, value} _ property;
		cp: FS.ComponentPositions;
		server, directory, subDirectory, nameBody: ROPE _ NIL;
		stp: STPPrivate.Handle _ NIL;
		closeDone: BOOL _ FALSE;
		SetProp: PROC [prop: STP.ValidProperties, value: REF TEXT] = BEGIN
			stp.plist[prop].length _ 0;
			stp.plist[prop] _ RefText.Append[stp.plist[prop], value];
			END;
		SetPropRope: PROC [prop: STP.ValidProperties, value: ROPE] = BEGIN
			stp.plist[prop].length _ 0;
			stp.plist[prop] _ RefText.AppendRope[stp.plist[prop], value];
			END;
		GetPropertyList: PROC = BEGIN
			str: STREAM _ IO.RIS[propertyList];
			DO
				char _ str.GetChar[ ! IO.EndOfStream => GOTO BadPList];
				SELECT char FROM
					'' =>
						Append[str.GetChar[ ! IO.EndOfStream => GOTO BadPList]];
					'( => {
						parens _ parens + 1;
						which _ property;
						property.length _ 0; };
					'  =>
						IF which = property THEN { which _ value; value.length _ 0; }
						ELSE Append[char];
					') => {
						IF property.length # 0 AND value.length # 0 THEN {
							STPBackdoor.SetPListItem[
								myPList,
								RefText.TrustTextAsRope[property],
								RefText.TrustTextAsRope[value] ! STPBackdoor.BadProperty => CONTINUE];
							property.length _ 0;
							value.length _ 0;
							};
						IF (parens _ parens - 1) = 0 THEN EXIT;
						};
					ENDCASE => Append[char];
				ENDLOOP;
			IF (property.length # 0 AND value.length # 0) THEN GOTO BadPList;
			EXITS
				BadPList => BEGIN
					STPBackdoor.ResetPList[myPList];
					END;
			END;
			
		[fullFName: fullFName, cp: cp] _ FS.ExpandName[fullFName];
		server _ fullFName.Substr[cp.server.start, cp.server.length];
		directory _ fullFName.Substr[cp.dir.start, cp.dir.length];
		subDirectory _ fullFName.Substr[cp.subDirs.start, cp.subDirs.length];
		nameBody _ fullFName.Substr[cp.base.start, cp.base.length + (IF cp.ext.length = 0 THEN 0 ELSE (cp.ext.length + 1))];
		IF NOT subDirectory.IsEmpty THEN nameBody _ subDirectory.Cat[">", nameBody];
		IF c = NIL THEN {c _ OpenConnection[server]; closeDone _ TRUE;};
		IF NOT server.Equal[c.serverName, FALSE] OR NOT c.open THEN OpenNew[c, server];
		TRUSTED { stp _ LOOPHOLE[c.stpHandle] };
		
		STPBackdoor.ResetPList[stp.plist];
		STPBackdoor.UserStateToPList[stp];
		STPBackdoor.ResetPList[myPList];
		IF propertyList # NIL THEN GetPropertyList[];  -- Makes myPList
		
		SetPropRope[directory, directory];
		IF cp.ver.length # 0 THEN
			stp.plist[version] _ RefText.AppendRope[stp.plist[version], fullFName.Substr[cp.ver.start, cp.ver.length]];
		SetPropRope[nameBody, nameBody];
		IF myPList[type].length = 0
			THEN {SetProp[type, "Binary"]; SetProp[byteSize, "8"]}
			ELSE {SetProp[type, myPList[type]]; SetProp[byteSize, myPList[byteSize]]};
		SetProp[size, myPList[size]];
		SetProp[device, myPList[device]];
		SetProp[author, myPList[author]];
		SetProp[eolConversion, myPList[eolConversion]];
		SetProp[createDate, myPList[createDate]];
		SetProp[readDate, myPList[readDate]];
		SetProp[writeDate, myPList[writeDate]];
		STPBackdoor.DoFiles[stp, NIL, Confirm, NIL, store
			! STP.Error => BEGIN
				inStream.Close[];
				IF closeDone THEN CloseConnection[c];
				ERROR FileOpsError [error, (code IN STP.ConnectionErrors)]
				END];
		IF closeDone THEN CloseConnection[c];
		inStream.Close[];
		END;
	
	FileOpsError: PUBLIC ERROR [error: ROPE, serverTrouble: BOOL] = CODE;
	
	myPList: STPPrivate.PList;
	Init: PROC[] = BEGIN
		myPList _ NEW[STPPrivate.PListArray];
		FOR i: STP.ValidProperties IN STP.ValidProperties DO
			myPList[i] _ RefText.ObtainScratch[100]; myPList[i].length _ 0;
			ENDLOOP;
		END;
	
	Init[];
		
	END.