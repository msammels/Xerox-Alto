-- ArchiveMedia.mesa
	-- Copyright � 1984, 1985, 1987 by Xerox Corp.  All rights reserved.
	-- Last edited by Tim Diebert: March 20, 1987 10:35:42 am PST

DIRECTORY
	BasicTime USING [GMT, nullGMT],
	IO USING [STREAM],
	Rope USING [ROPE];

ArchiveMedia: CEDAR DEFINITIONS
	IMPORTS = BEGIN
	
	ROPE: TYPE ~ Rope.ROPE;
	STREAM: TYPE ~ IO.STREAM;
	GMT: TYPE ~ BasicTime.GMT;
	nulGMT: GMT ~ BasicTime.nullGMT;
	
	DirectoryEntry: TYPE ~ REF DirectoryRec;
	DirectoryRec: TYPE ~ RECORD
		[
		fileName: ROPE, -- This is the full path name of the file.  ie.  [Ivy]<Diebert>This.tioga!2
		fileCreateTime: GMT, -- From the file server.
		propertyList: ROPE, -- From the file server.
		fileSize: INT -- in bytes,  this is only a hint that is aquired from the file server.
		];
	
	VOLUME: TYPE ~ REF VOLUMERecord;
	VOLUMERecord: TYPE ~ RECORD
		[
		volID: ROPE, -- the media volume ID such as P5002.
		volFunction: FunctionType, -- primary, backup or personal
		mediaType: MediaType, -- tape, optical disk or disk
		accessOptions: AccessOptions _ read, -- or write
		mediaSpecific: REF ANY _ NIL
		];
	
	MediaType: TYPE ~ MACHINE DEPENDENT {tape, opticalDisk, disk, maxcTape, undefined(255)};
	FunctionType: TYPE ~ MACHINE DEPENDENT {
		Primary(0),
		Backup (1),
		Personal (2),
		(65535)};
	AccessOptions: TYPE ~ {read, write};
	
	
	Error: ERROR [error: ErrorDesc];
	
	ErrorDesc: TYPE ~ RECORD
		[media: MediaType, group: ErrorGroup, explanation: ROPE] _ [undefined, ok, NIL];
	
	ErrorGroup: TYPE ~ {
		ok, -- initial group
		bug, -- caused by an internal bug
		environment, -- something's wrong in the environment; human intervention required
		client, -- illegal operation, probably due to bug in client program
		user -- illegal operation, probably due to user action
		};
	
	FileLargerThanMedia: ERROR;
		-- This signal is raised when the media cannot take the file in progress because the media is too small.  This may happen when the fileSizeHint is incorrect by a large amount.
	
	EndOfMedia: ERROR;
		-- This ERROR is raised when a FindFileDirectoryEntry or a ReadFileDirectoryEntry has been requested and the end of media is reached before a directory entry is found.
	
	FindFileDirectoryEntry: PROC  [vol: VOLUME, fullFName: ROPE, createDateWanted: GMT _ nulGMT]  RETURNS [DirectoryEntry];
		-- This procedure finds the requested directory on the media.  The media is then positioned to provide access to read the file.  The idea here is to open a stream on the media and transfer the file.
	
	ReadFileDirectoryEntry: PROC [vol: VOLUME] RETURNS [DirectoryEntry];
		-- This proc reads the next available directroy entry.  The media is left positioned so that a stream may be opened to it.
		
	CreateFileDirectoryEntry: PROC  [fullFName: ROPE, propertyList: ROPE, createDate: GMT, sizeHint: INT]  RETURNS [DirectoryEntry];
		-- This procedure creates a Directory entry for the file specified.
	
	WriteFileDirectoryEntry: PROC [vol: VOLUME, dir: DirectoryEntry] RETURNS [BOOL];
		-- This procedure takes a DirectoryEntry and writes it onto the media VOLUME.  The media is then positioned such that a stream may be opend to it.
	
	OpenMediaStream: PROC [vol: VOLUME] RETURNS [STREAM];
		-- This procedure opens an IO Stream on the media at the position the media is in.  It should be used only after either a FindFileEntry or a WriteFileDirectoryEntry.
	
	DeleteLastEntry: PROC [vol: VOLUME, dir: DirectoryEntry];
		-- This procedure is a way to delete all remnants of a file that may have be aborted in the middle.  It is used to insure that the media is consitant.
		
	FileFitCheck: PROC [vol: VOLUME, sizeHint: INT] RETURNS [fit: BOOL, bytesLeft: INT];
		-- This proc is used to check whether a file of sizeHint size will fit onto the remaining space on the media.  If the file is to large a FALSE is returned.  The number of bytes left on the volume is returned as bytesLeft.
	
	RestoreMediaToBeginning: PROC [vol: VOLUME] RETURNS [BOOL];
		-- This proc repositions the media to the beginning.  On tape media this preforms a rewind, on disks it posiions the heads to the first directory entry.  This command cannot be issued to write access media.  If it is called it will return FALSE.
	
	END.