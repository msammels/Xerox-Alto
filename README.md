# Xerox Alto

[[_TOC_]]

## The roots of the modern personal computer

---

![](assets/xerox-alto-source-code-pet-apple-ii.jpg){width=25%}

*What was your first computer? Clockwise from top: Radio Shack TRS-80, Apple
II, and Commodore Pet.*

---

Depending on your age, your first computer might have been an Apple II, a
Radio Shack TRS-80, an IBM PC, an Apple Macintosh, or another of the early
personal computers. If you missed these early machines the first time around,
perhaps you have seen them in the Personal Computer section of the Revolution
exhibit at the Computer History Museum.

Some people are old enough that the first computer they used was a large
mainframe in a distant building that they never actually saw. Sseniors in high
school (1966-1967) would take a class that included FORTRAN IV
programming. They punched our programs into 80-column IBM cards, and several
weeks later we received their printouts. That was strong motivation to think
carefully and double-check everything!

---

![](assets/xerox-alto-source-code-ibm-026-key-punch.jpg){width=25%}

*Preparing programs and data for early computers often required punching it
into cards with a device like this IBM 026 Key Punch.*

---

In the early 1970s, hobbyists and other “early adopters” used the new
microprocessors to build homebrew computers. Some began selling kits and even
fully-assembled computers, such as the Altair 8800, IMSAI 8080, and Processor
Technology Sol-20. These computers had tiny memories and only a cassette tape
or floppy disk drive for storage. The user sometimes interacted with them via
front-panel lights and switches, or perhaps with a keyboard, and a TV as a
display. Each personal computer was isolated, with no way to network to other
computers. Despite their limitations, these early machines excited the
imagination of many. Len Shustek described some of this activity during The
Homebrew Computer Club 2013 Reunion.

---

![](assets/xerox-alto-source-code-sol-20.jpg){width=25%}

*Early computer kits included the Processor Technology Sol-20 with two floppy
disk drives and a video display.*

---

In 1970 the Xerox Corporation, which had become very successful in the 1960s
with its 914 copier and related products, established the Palo Alto Research
Center (PARC). The goals were to develop an “architecture of information” and
lay the groundwork for future electronic office products. A 1972 Rolling Stone
magazine article by Stewart Brand, with photos by Annie Leibovitz, provided an
early look at the computer research scene at PARC, the Stanford AI Lab, and
other Bay Area locations.

By the fall of 1976 PARC’s research was far enough along that a Xerox product
group started to design products based on their prototypes.

It’s hard to explain just how advanced the Alto seemed at the time. It had a
full-page graphics display with 606 by 808 black & white pixels, a keyboard, a
mouse, a fairly powerful processor with 128 KBytes of main memory, a hard
drive with a 2.5 MByte removable cartridge, and a 2.94 Mbit/sec Ethernet
interface. The Ethernet connected Altos together into a local network that
included a high-performance laser printer, an Alto-based file server with
hundreds of megabytes of capacity, and gateways to local networks at other
Xerox offices and to the ARPANET.

---

![](assets/xerox-alto-source-code-parc-alto.jpg){width=25%}

*PARC’s Alto computer was one of the first with a bit-mapped display and a
mouse.*

---

The Alto software included Bravo, the first WYSIWYG (What You See Is What You
Get) word processor, Markup, a pixel-oriented drawing editor, and Draw, a
vector-oriented drawing editor. That fall day I had my first taste of the
world that we now take for granted: the ability to create and modify digital
documents containing text and graphics, store them, and transmit them “at
blinding electronic speed”. I took the job, and for the next four years I used
an Alto to develop some of the software for Xerox’s Star office automation
system.

In 2023 we take the personal computer and the Internet for granted. Most homes
and offices have desktop or laptop computers, and almost all of them are
attached to the Internet via WiFi or Ethernet. Our mobile devices —
smartphones and tablets — let us continuously participate in the digital
world. The pioneering Alto project that began in 1972 invented or refined many
of the fundamental hardware and software ideas upon which our modern devices
are based, including raster displays, mouse pointing devices,
direct-manipulation user interfaces, windows and menus, and Ethernet.

---

![](assets/xerox-alto-source-code-paul-mcjones-office.jpg){width=25%}

*In 1980, Paul McJones used this Alto to develop portions of the Xerox Star
operating system.*

---

The first Altos were built as research prototypes. Ultimately about 1500 of
them were built and deployed throughout the Xerox Corporation, as well as at
universities and other sites. But this groundbreaking machine was never sold
as a product; its legacy is as inspiration for what followed.

## Alto software

The Alto software included four different programming environments: BCPL,
Mesa, Smalltalk, and Lisp. There was a single-user operating system
(compatibly reimplemented in each of the four programming environments), a set
of utility programs, application programs such as the Bravo word processor,
the Markup, Draw, and Sil drawing programs, and the Laurel email
program. Altos were also interfaced to specialized hardware such as large disk
drives and laser print engines; these Altos then typically ran server software
and allowed that specialized hardware to be shared by a community of users.

Ethernet was developed for the Alto system. The networking software, called
Pup (for PARC Universal Packet), anticipated the Internet by allowing multiple
Ethernet local area networks to be interconnected by leased phone lines, radio
links, and the ARPANET (which at this time connected a handful of computers at
ARPA research centers). The Alto had an indirect but large influence on the
mass-market personal computer hardware and software of the 1980s and beyond.


Here are snapshots of Alto source code, executables, documentation, font
files, and other files from 1975 to 1987. The files are organized by the
original server on which they resided at PARC (Filene, Ibis, Indigo, Io, Ivy,
and Pixel), plus two additional groupings (cd6 and cd8) that correspond to
files that were restored from other archive tapes. For each server, there are
one or more top-level directories containing subdirectories and files. Each
file is generally presented two ways: in a viewable format (e.g., HTML, PDF),
and in its original format (referred to as “raw” in the directory listings).

[Xerox PARC Alto/Xero Alto Filesystem Archive](src)
